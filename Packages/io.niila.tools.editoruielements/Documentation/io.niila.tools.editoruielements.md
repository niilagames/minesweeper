# Editor UI Elements Guide

Editor UI Elements is a set of tools to ease the creation of editor UI in Unity.
It comes with ready made static helpers for creating rows, columns, padded areas for content, header and text styles, which allows for easy setup of layout and text content.
It also comes with a few default windows that you can inherit from instead of the EditorWindow class, which comes with a default basic layout.

Finally there is a class called UiElement, which you can inherit from, if you want to make your own UiElements for reuse.
It implements a Draw method, which must be implemented, but besides that it can use its constructor or properties to set values that it needs to render.

## Working with UI Elements

### Using the default windows


### Using pages and page menues


### Using the UiElements class


### Using the UiStyles class


### Using the UiFonts class
To use fonts provided by the library use the following static call ```UiFonts.Get()```.
As a parameter to the static Get call indicate type of font (for instance normal, header or button).

### Using the UiIcons class
To get an icon, use the static call ```UiIcons.Get()```.
As a parameter to the static Get call indicate the type of icon (for instance trash, home, settings or add).

### Creating custom UiElements


## FAQ (or the 'something went wrong' section)

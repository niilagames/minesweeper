# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),

## [1.5.0] - 2020-10-06
### Added
- New icons!
- Added a way to set the equality function for lists to make a manual change check.

## [1.4.0] - 2020-03-17
### Added
- Added OnEnable and OnDisable methods to the UiElement class. They have empty implementations in the abstract classes that can be overridden.
- Added icons to make it faster to and more consise when using icons across different tools. They are available through UiIcons.
- Added fonts to make the editor tools stand out a bit more (and look nicer). They are available through UiFonts.
### Changed
- Changed all headers, texts, buttons and more to use the new fonts.

## [1.3.0] - 2019-12-10
### Added
- Added lists to UiElements. They add a unified way to draw a list for all types of elements.

## [1.2.0] - 2019-11-12
### Added
- Added a modal window with a text input.
- Added pages and page menu elements.
- Added interfaces for the page element types (for instance IPageElement).
- Added tickable pages and tickable page menu elements.
- Added Dynamic pages that allow name, ID and description to be set when created and later.
- Added rich text versions of text and header helpers (for instance UiElements.H1Rich(...) or UiElements.TextRich(...)).
- Added a new abstract editor window class with a default setup (a header row and an area with a page menu and pages area).
- Added interfaces for UiElement and UiElement<T>

## [1.1.0] - 2019-10-22
### Added
- Added a new abstract editor window class with a default setup (a header row and two columns beneath).
- Added a new abstract editor window class with a default setup (a header row and a content area beneath).
- Added an introduction to the documentation.

### Changed
- Changed the way rows, columns and padded areas (priorly know as content areas) are used. The new way reduces nesting in code.

## [1.0.0] - 2019-10-15
### Added
- Added UiElement abstract class, which can be extended to better group element code.
- Added UiElements helper class with static methods for layouts ets.
- Added UiStyles helper class with styles for different types of elements (for instance headers).
﻿using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

public class Debouncer
{

    private Action _action;
    private double _debounceTimeSeconds;

    private bool _isActionInvocationStarted;
    private double _invokeActionTime;


    public Debouncer(Action action, double debounceTimeSeconds = .5d)
    {
        _action = action;
        _debounceTimeSeconds = debounceTimeSeconds;
    }

    public void Update()
    {
        if (_isActionInvocationStarted)
        {
            var currentTime = EditorApplication.timeSinceStartup;
            if (currentTime >= _invokeActionTime)
            {
                _action.Invoke();
                _isActionInvocationStarted = false;
            }
        }
    }

    public void Invoke()
    {
        _invokeActionTime = EditorApplication.timeSinceStartup + _debounceTimeSeconds;
        _isActionInvocationStarted = true;
    }

}

﻿using System.Collections;

namespace Niila.Tools.EditorUiElements.Editor.Pages
{
    public abstract class TickablePageElement : PageElement, ITickablePageElement
    {
        public bool IsTicked { get; set; }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Niila.Tools.EditorUiElements.Editor.Pages
{
    public interface ITickablePageElement : IPageElement
    {
        bool IsTicked { get; set; }
    }
}
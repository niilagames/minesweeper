﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Niila.Tools.EditorUiElements.Editor.Pages
{
    public interface IPageElement : IUiElement
    {
        string PageId { get; }
        string PageName { get; }
        string PageDescription { get; }

        void OnEnabled();

        void OnDisabled();

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Niila.Tools.EditorUiElements.Editor.Pages
{
    /// <summary>
    /// Dynamic page element allows the page type to be reusable by setting the pageId, pageName and pageDescription on creation instead of as a constant.
    /// </summary>
    public abstract class DynamicPageElement : PageElement
    {
        public override string PageId { get; }
        public override string PageName { get; }
        public override string PageDescription { get; }

        public DynamicPageElement(string pageId, string pageName, string pageDescription)
        {
            PageId = pageId;
            PageName = pageName;
            PageDescription = pageDescription;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.EditorUiElements.Editor.Pages
{
    public class TickablePageMenuElement<T> : PageMenuElement<T> where T : class, ITickablePageElement
    {
        public event Action<ITickablePageElement> OnPageMenuTicket;

        protected override List<T> GetSortedPageList(List<T> pages)
        {
            var sortedList = pages.OrderBy(page => page.PageName).ThenByDescending(page => page.IsTicked).ToList();
            return sortedList;
        }

        protected override void DrawMenuItem(T tickablePage, bool isSelected)
        {
            var backgroundColour = isSelected ? (Color?)UiStyles.BackgroundColourSelected : null;
            var buttonRect = UiElements.BeginRow(20, true, new[] { UiElements.Border.Bottom }, null, backgroundColour);

            var tmpIsTicket = tickablePage.IsTicked;
            if (isSelected)
            {
                UiElements.BeginPaddedArea();
                EditorGUILayout.BeginHorizontal();
                tickablePage.IsTicked = EditorGUILayout.Toggle(tickablePage.IsTicked, GUILayout.Width(20));
                UiElements.TextBold(tickablePage.PageName);
                EditorGUILayout.EndHorizontal();
                UiElements.EndPaddedArea();
            }
            else
            {
                UiElements.BeginPaddedArea();
                EditorGUILayout.BeginHorizontal();
                tickablePage.IsTicked = EditorGUILayout.Toggle(tickablePage.IsTicked, GUILayout.Width(20));
                UiElements.Text(tickablePage.PageName);
                EditorGUILayout.EndHorizontal();
                UiElements.EndPaddedArea();
            }

            // If page was ticked or unticked, raise event.
            if (tmpIsTicket != tickablePage.IsTicked)
            {
                OnPageMenuTicket?.Invoke(tickablePage);
            }

            UiElements.EndRow();

            // Check if menu item was clicked.
            if (Event.current.type == EventType.MouseUp && Event.current.button == 0 && buttonRect.Contains(Event.current.mousePosition))
            {
                RaiseOnPageMenuItemClicked(tickablePage);
                // Use up the event, so it doesn't propagate.
                Event.current.Use();
            }
        }
    }
}
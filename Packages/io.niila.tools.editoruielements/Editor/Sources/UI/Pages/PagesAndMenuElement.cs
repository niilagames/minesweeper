﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Niila.Tools.EditorUiElements.Editor;
using Niila.Tools.EditorUiElements.Editor.Pages;
using UnityEditor;

namespace Niila.Tools.EditorUiElements.Editor
{
    public class PagesAndMenuElement<T> : UiElement where T : class, IPageElement
    {
        public int PageMenuWidth { get; set; }
        public List<T> Pages { get; private set; }

        private string _lastPageSaveKey = null;
        private PageMenuElement<T> _pageMenu = null;
        private T _selectedPage = default(T);

        private bool _isEnabled;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lastPageSaveKey">The key used to save which page the user had open last. This will be the one to be selected initially, if it exists. MUST BE UNIQUE ACROSS PROJECT!</param>
        /// <param name="pages"></param>
        /// <param name="pageMenuWidth"></param>
        public PagesAndMenuElement(string lastPageSaveKey, int pageMenuWidth)
        {
            PageMenuWidth = pageMenuWidth;
        }

        public void SetPages(List<T> pages)
        {
            Pages = pages;
            if (_pageMenu == null)
            {
                _pageMenu = new PageMenuElement<T>();
            }

            _pageMenu.Pages = pages;


        }

        private void SetSelectedPageFromSettings()
        {
            // Fetch last open page and set that as selected page (or first page if none found).
            var lastSelectedPageId =
                EditorPrefs.HasKey(_lastPageSaveKey) ? EditorPrefs.GetString(_lastPageSaveKey) : null;
            _selectedPage = Pages.FirstOrDefault(p => p.PageId == lastSelectedPageId) ??
                            (Pages.Count > 0 ? Pages[0] : null);

            SetSelectedPage(_selectedPage);
        }

        public void OnEnabled()
        {
            _isEnabled = true;

            SetSelectedPageFromSettings();

            foreach (var page in Pages)
            {
                page.OnEnabled();
            }

            _pageMenu.OnPageMenuItemClicked += SetSelectedPage;
        }

        public void OnDisabled()
        {
            _pageMenu.OnPageMenuItemClicked -= SetSelectedPage;

            foreach (var page in Pages)
            {
                page.OnDisabled();
            }

            _isEnabled = false;
        }

        protected virtual void SetSelectedPage(T page)
        {
            _selectedPage = page;
            _pageMenu.SelectedPage = page;
            EditorPrefs.SetString(_lastPageSaveKey, page.PageId);
        }

        public override void Draw()
        {
            UiElements.BeginColumns(true);

            UiElements.BeginColumn(PageMenuWidth, true, new[] {UiElements.Border.Right});

            _pageMenu?.Draw();

            UiElements.EndColumn();

            UiElements.BeginColumn(0, true);

            _selectedPage?.Draw();

            UiElements.EndColumn();

            UiElements.EndColumns();
        }

        public override void Update()
        {
            if (_isEnabled)
            {
                foreach (var page in Pages)
                {
                    page.Update();
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.EditorUiElements.Editor.Pages
{
    public class PageMenuElement<T> : UiElement where T : class, IPageElement
    {
        public event Action<T> OnPageMenuItemClicked;

        public List<T> Pages { get; set; }
        public T SelectedPage { get; set; }

        private Vector2 _scrollPos;

        public override void Draw()
        {
            if (Pages == null || Pages.Count == 0)
            {
                return;
            }

            var sortedPages = GetSortedPageList(Pages);

            _scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);
            UiElements.BeginRows(true);
            foreach (var page in sortedPages)
            {
                var isSelected = page == SelectedPage;
                DrawMenuItem(page, isSelected);
            }
            UiElements.EndRows();
            EditorGUILayout.EndScrollView();
        }

        public override void Update()
        {
            
        }

        protected virtual List<T> GetSortedPageList(List<T> pages)
        {
            return pages;
        }

        protected virtual void DrawMenuItem(T page, bool isSelected)
        {
            var backgroundColour = isSelected ? (Color?)UiStyles.BackgroundColourSelected : null;
            var buttonRect = UiElements.BeginRow(20, true, new []{UiElements.Border.Bottom}, null, backgroundColour);
            if (isSelected)
            {
                UiElements.BeginPaddedArea();
                UiElements.TextBold(page.PageName);
                UiElements.EndPaddedArea();
            }
            else
            {
                UiElements.BeginPaddedArea();
                UiElements.Text(page.PageName);
                UiElements.EndPaddedArea();
            }
            UiElements.EndRow();

            // Check if menu item was clicked.
            if (Event.current.type == EventType.MouseUp && Event.current.button == 0 && buttonRect.Contains(Event.current.mousePosition))
            {
                RaiseOnPageMenuItemClicked(page);
                // Use up the event, so it doesn't propagate.
                Event.current.Use();
            }
        }

        protected void RaiseOnPageMenuItemClicked(T page)
        {
            OnPageMenuItemClicked?.Invoke(page);
        }
    }
}
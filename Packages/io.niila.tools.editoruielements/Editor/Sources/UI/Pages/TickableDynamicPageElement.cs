﻿using System.Collections;

namespace Niila.Tools.EditorUiElements.Editor.Pages
{
    public abstract class TickableDynamicPageElement : 
        DynamicPageElement, ITickablePageElement
    {
        public bool IsTicked { get; set; }

        public TickableDynamicPageElement(string pageId, string pageName, string pageDescription) : base(pageId, pageName, pageDescription)
        {
        }
    }
}
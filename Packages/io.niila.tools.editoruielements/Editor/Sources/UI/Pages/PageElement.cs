﻿using System.Collections;

namespace Niila.Tools.EditorUiElements.Editor.Pages
{
    public abstract class PageElement : UiElement, IPageElement
    {
        public abstract string PageId { get; }
        public abstract string PageName { get; }
        public abstract string PageDescription { get; }

        public virtual void OnEnabled() { }

        public virtual void OnDisabled() { }
    }
}
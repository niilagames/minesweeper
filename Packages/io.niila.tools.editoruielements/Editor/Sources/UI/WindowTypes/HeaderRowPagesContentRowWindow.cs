﻿using System.Collections;
using System.Collections.Generic;
using Niila.Tools.EditorUiElements.Editor.Pages;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.EditorUiElements.Editor
{

    public abstract class HeaderRowPagesContentRowWindow<T> : EditorWindow where T : class, IPageElement
    {

        protected abstract int HeaderRowHeight { get; }
        protected abstract bool UsePaddedAreaInHeader { get; }
        protected abstract int PageMenuWidth { get; }
        /// <summary>
        /// The key used to save the which page was last opened on the current machine. Must be unique! Preferably use a prefix besides a name.
        /// </summary>
        protected abstract string PageMenuLastPageSaveKey { get; }

        protected PagesAndMenuElement<T> PagesAndMenu;

        protected abstract List<T> GetPageList();

        protected virtual void OnEnable()
        {
            if (PagesAndMenu == null)
            {
                PagesAndMenu = new PagesAndMenuElement<T>(PageMenuLastPageSaveKey, PageMenuWidth);
            }
            
            PagesAndMenu.SetPages(GetPageList());
            PagesAndMenu.OnEnabled();
        }

        protected virtual void OnDisable()
        {
            PagesAndMenu.OnDisabled();
        }

        private void OnGUI()
        {
            UiElements.BeginRows(true);
            UiElements.BeginRow(HeaderRowHeight, true, new[] { UiElements.Border.Bottom });
            if (UsePaddedAreaInHeader)
            {
                UiElements.BeginPaddedArea();
            }

            OnHeaderRow();

            if (UsePaddedAreaInHeader)
            {
                UiElements.EndPaddedArea();
            }
            UiElements.EndRow();
            UiElements.BeginRow(0, true);

            PagesAndMenu?.Draw();

            UiElements.EndRow();
            UiElements.EndRows();
        }

        protected abstract void OnHeaderRow();
    }
}
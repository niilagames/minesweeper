﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.EditorUiElements.Editor
{

    public abstract class HeaderRowDualColumnsWindow : EditorWindow
    {

        protected abstract int HeaderRowHeight { get; }
        protected abstract bool UsePaddedAreaInHeader { get; }
        protected abstract int LeftColumnWidth { get; }
        protected abstract bool UsePaddedAreaInLeftColumn { get; }
        protected abstract int RightColumnWidth { get; }
        protected abstract bool UsePaddedAreaInRightColumn { get; }


        private void OnGUI()
        {
            UiElements.BeginRows(true);
            UiElements.BeginRow(HeaderRowHeight, true, new[] { UiElements.Border.Bottom });
            if (UsePaddedAreaInHeader)
            {
                UiElements.BeginPaddedArea();
            }

            OnHeaderRow();

            if (UsePaddedAreaInHeader)
            {
                UiElements.EndPaddedArea();
            }

            UiElements.EndRow();
            UiElements.BeginRow(0, true);
            UiElements.BeginColumns(true);
            UiElements.BeginColumn(LeftColumnWidth, true, new[] { UiElements.Border.Right });
            if (UsePaddedAreaInLeftColumn)
            {
                UiElements.BeginPaddedArea();
            }

            UiElements.Text("Some header in column 1");

            if (UsePaddedAreaInLeftColumn)
            {
                UiElements.EndPaddedArea();
            }

            UiElements.EndColumn();
            UiElements.BeginColumn(RightColumnWidth, true);
            if (UsePaddedAreaInRightColumn)
            {
                UiElements.BeginPaddedArea();
            }

            UiElements.Text("Some header in column 2");

            if (UsePaddedAreaInRightColumn)
            {
                UiElements.EndPaddedArea();
            }

            UiElements.EndColumn();
            UiElements.EndColumns();
            UiElements.EndRow();
            UiElements.EndRows();
        }

        protected abstract void OnHeaderRow();

        protected abstract void OnLeftColumn();

        protected abstract void OnRightColumn();

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.EditorUiElements.Editor
{

    public abstract class HeaderRowContentRowWindow : EditorWindow
    {

        protected abstract int HeaderRowHeight { get; }
        protected abstract bool UsePaddedAreaInHeader { get; }
        protected abstract bool UsePaddedAreaInContentRow { get; }


        private void OnGUI()
        {
            UiElements.BeginRows(true);
            UiElements.BeginRow(HeaderRowHeight, true, new[] { UiElements.Border.Bottom });
            if (UsePaddedAreaInHeader)
            {
                UiElements.BeginPaddedArea();
            }

            OnHeaderRow();

            if (UsePaddedAreaInHeader)
            {
                UiElements.EndPaddedArea();
            }

            UiElements.EndRow();
            UiElements.BeginRow(0, true);
            if (UsePaddedAreaInContentRow)
            {
                UiElements.BeginPaddedArea();
            }

            OnContentRow();

            if (UsePaddedAreaInContentRow)
            {
                UiElements.EndPaddedArea();
            }
            UiElements.EndRow();
            UiElements.EndRows();
        }

        protected abstract void OnHeaderRow();

        protected abstract void OnContentRow();
    }
}
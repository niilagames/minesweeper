﻿
namespace Niila.Tools.EditorUiElements.Editor
{
    public interface IUiElement
    {
        void Draw();
        void Update();
        void OnEnable();
        void OnDisable();
    }

    public abstract class UiElement : IUiElement
    {
        public abstract void Draw();

        public abstract void Update();
        public virtual void OnEnable() { }
        public virtual void OnDisable() { }
    }

    public interface IUiElement<in T>
    {
        void Draw(T child);
        void Update();
        void OnEnable();
        void OnDisable();
    }

    public abstract class UiElement<T> : IUiElement<T>
    {
        public abstract void Draw(T child);
        public abstract void Update();
        public virtual void OnEnable() { }
        public virtual void OnDisable() { }
    }
}
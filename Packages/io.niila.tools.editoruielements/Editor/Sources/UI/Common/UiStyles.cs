﻿using UnityEditor;
using UnityEngine;

namespace Niila.Tools.EditorUiElements.Editor
{

    public static class UiStyles
    {
        public static readonly Color BorderColour = Color.gray;

        public static readonly Color BackgroundColourSelected = new Color(0.5f, 0.5f, 0.5f, 0.5f);

        public static readonly RectOffset DefaultPadding = new RectOffset(10, 10, 10, 10);

        #region Headers

        public static readonly GUIStyle H1 = new GUIStyle(GUI.skin.label) { name = "NiilaUiElementsH1Style", font = UiFonts.Get(FontType.Header), fontSize = 22, margin = new RectOffset(0, 0, 0, 15), padding = new RectOffset(0, 0, 0, -5)};

        public static readonly GUIStyle H2 = new GUIStyle(GUI.skin.label) { name = "NiilaUiElementsH2Style", font = UiFonts.Get(FontType.Header), fontSize = 18, margin = new RectOffset(0, 0, 0, 10), padding = new RectOffset(0, 0, 0, -5) };

        public static readonly GUIStyle H3 = new GUIStyle(GUI.skin.label) { name = "NiilaUiElementsH3Style", font = UiFonts.Get(FontType.Header), fontSize = 14, margin = new RectOffset(0, 0, 0, 10), padding = new RectOffset(0, 0, 0, -5) };

        #endregion

        #region Texts

        public static readonly GUIStyle TextStyle = new GUIStyle(GUI.skin.label) { name = "NiilaUiElementsTextStyle", font = UiFonts.Get(), fontSize = 14, wordWrap = true };

        public static readonly GUIStyle TextBoldStyle = new GUIStyle(GUI.skin.label) { name = "NiilaUiElementsTextBoldStyle", font = UiFonts.Get(FontType.NormalBold), fontSize = 14, wordWrap = true };

        #endregion

        #region Foldout

        public static GUIStyle FoldoutH1
        {
            get
            {
                var style = new GUIStyle(EditorStyles.foldout)
                {
                    font = UiFonts.Get(FontType.Header),
                    fontSize = 22,
                    fontStyle = FontStyle.Normal,
                    margin = new RectOffset(0, 0, 0, 15)
                };
                return style;
            }
        }

        public static GUIStyle FoldoutH2
        {
            get
            {
                var style = new GUIStyle(EditorStyles.foldout)
                {
                    font = UiFonts.Get(FontType.Header),
                    fontSize = 18,
                    fontStyle = FontStyle.Normal,
                    margin = new RectOffset(0, 0, 0, 10)
                };
                return style;
            }
        }

        public static GUIStyle FoldoutH3
        {
            get
            {
                var style = new GUIStyle(EditorStyles.foldout)
                {
                    font = UiFonts.Get(FontType.Header),
                    fontSize = 14,
                    fontStyle = FontStyle.Normal,
                    margin = new RectOffset(0, 0, 0, 10)
                };
                return style;
            }
        }

        public static GUIStyle Foldout
        {
            get
            {
                var style = new GUIStyle(EditorStyles.foldout)
                {
                    font = UiFonts.Get(),
                    fontSize = 12,
                    fontStyle = FontStyle.Normal,
                    margin = new RectOffset(0, 0, 0, 5)
                };
                return style;
            }
        }

        #endregion

        #region Buttons

        public static readonly GUIStyle ButtonStyle = new GUIStyle(GUI.skin.button) { font = UiFonts.Get(FontType.Button), fontSize = 14 };

        public static readonly GUIStyle ButtonWithIconStyle = new GUIStyle(GUI.skin.button) { font = UiFonts.Get(FontType.Button), fontSize = 14, padding = new RectOffset(3, 3, 3, 3) };

        public static readonly GUIStyle ButtonLargeStyle = new GUIStyle(GUI.skin.button) { font = UiFonts.Get(FontType.Button), fontSize = 18, padding = new RectOffset(3, 3, 3, 3) };

        public static readonly GUIStyle ButtonLargeWithIconStyle = new GUIStyle(GUI.skin.button) { font = UiFonts.Get(FontType.Button), fontSize = 18, padding = new RectOffset(5,5,5,5) };

        #endregion
    }
}
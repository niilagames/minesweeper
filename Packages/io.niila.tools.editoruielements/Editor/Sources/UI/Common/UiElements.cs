﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.EditorUiElements.Editor
{

    public static class UiElements
    {
        public static void BeginPaddedArea(RectOffset padding = null)
        {
            EditorGUILayout.BeginVertical(new GUIStyle {padding = padding ?? UiStyles.DefaultPadding});
        }

        public static void EndPaddedArea()
        {
            EditorGUILayout.EndVertical();
        }

        #region Columns

        public static void BeginColumns(bool expandHeight)
        {
            EditorGUILayout.BeginHorizontal(GUILayout.ExpandHeight(expandHeight));
        }

        public static void EndColumns()
        {
            EditorGUILayout.EndHorizontal();
        }

        public static Rect BeginColumn(int columnWidth, bool expandHeight, Border[] borders = null, Color? borderColour = null, Color? backgroundColour = null)
        {
            var tmpBackgroundColour = GUI.backgroundColor;
            var style = new GUIStyle
            {
                padding = new RectOffset(0, 0, 0, 0),
                margin = new RectOffset(0, 0, 0, 0),
                normal = { background = backgroundColour != null ? Texture2D.whiteTexture : Texture2D.blackTexture }
            };
            if (backgroundColour != null)
            {
                GUI.backgroundColor = backgroundColour.Value;
            }

            var col = EditorGUILayout.BeginVertical(style, GUILayout.Width(columnWidth), GUILayout.ExpandHeight(expandHeight));
            if (borders != null)
            {
                foreach (var border in borders)
                {
                    var selectedBorderColour = borderColour.HasValue ? borderColour.Value : UiStyles.BorderColour;
                    switch (border)
                    {
                        case Border.Top:
                            EditorGUI.DrawRect(new Rect(col.x, col.y, col.width, 1), selectedBorderColour);
                            break;
                        case Border.Right:
                            EditorGUI.DrawRect(new Rect(col.x + col.width, col.y, 1, col.height), selectedBorderColour);
                            break;
                        case Border.Bottom:
                            EditorGUI.DrawRect(new Rect(col.x, col.y + col.height, col.width, 1), selectedBorderColour);
                            break;
                        case Border.Left:
                            EditorGUI.DrawRect(new Rect(col.x, col.y, 1, col.height), selectedBorderColour);
                            break;
                    }
                }
            }

            GUI.backgroundColor = tmpBackgroundColour;

            return col;
        }

        public static void EndColumn()
        {
            EditorGUILayout.EndVertical();
        }

        #endregion

        #region Rows

        public static void BeginRows(bool expandWidth)
        {
            EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(expandWidth));
        }

        public static void EndRows()
        {
            EditorGUILayout.EndVertical();
        }

        public static Rect BeginRow(int rowHeight, bool expandWidth, Border[] borders = null, Color? borderColour = null, Color? backgroundColour = null)
        {
            var tmpBackgroundColour = GUI.backgroundColor;
            var style = new GUIStyle
            {
                padding = new RectOffset(0, 0, 0, 0),
                margin = new RectOffset(0, 0, 0, 0),
                normal = { background = backgroundColour != null ? Texture2D.whiteTexture : Texture2D.blackTexture }
            };
            if (backgroundColour != null)
            {
                GUI.backgroundColor = backgroundColour.Value;
            }

            var row = EditorGUILayout.BeginHorizontal(style, GUILayout.Height(rowHeight), GUILayout.ExpandWidth(expandWidth));
            if (borders != null)
            {
                foreach (var border in borders)
                {
                    var selectedBorderColour = borderColour.HasValue ? borderColour.Value : UiStyles.BorderColour;
                    switch (border)
                    {
                        case Border.Top:
                            EditorGUI.DrawRect(new Rect(row.x, row.y, row.width, 1), selectedBorderColour);
                            break;
                        case Border.Right:
                            EditorGUI.DrawRect(new Rect(row.x + row.width, row.y, 1, row.height), selectedBorderColour);
                            break;
                        case Border.Bottom:
                            EditorGUI.DrawRect(new Rect(row.x, row.y + row.height, row.width, 1), selectedBorderColour);
                            break;
                        case Border.Left:
                            EditorGUI.DrawRect(new Rect(row.x, row.y, 1, row.height), selectedBorderColour);
                            break;
                    }
                }
            }

            GUI.backgroundColor = tmpBackgroundColour;

            return row;
        }

        public static void EndRow()
        {
            EditorGUILayout.EndHorizontal();
        }

        #endregion

        #region Headers

        public static void H1(string text)
        {
            EditorGUILayout.LabelField(text, UiStyles.H1);
            GUILayout.Space(10);
        }

        public static void H1Rich(string text)
        {
            var style = UiStyles.H1;
            style.richText = true;
            EditorGUILayout.LabelField(text, style);
            GUILayout.Space(10);
        }

        public static void H2(string text)
        {
            EditorGUILayout.LabelField(text, UiStyles.H2);
            GUILayout.Space(5);
        }

        public static void H2Rich(string text)
        {
            var style = UiStyles.H2;
            style.richText = true;
            EditorGUILayout.LabelField(text, style);
            GUILayout.Space(5);
        }

        public static void H3(string text)
        {
            EditorGUILayout.LabelField(text, UiStyles.H3);
            GUILayout.Space(5);
        }

        #endregion

        #region Text

        public static void Text(string text)
        {
            EditorGUILayout.LabelField(text, UiStyles.TextStyle);
        }

        public static void Text(string text, TextAnchor alignment)
        {
            var style = new GUIStyle(UiStyles.TextStyle) {alignment = alignment, stretchHeight = true};
            EditorGUILayout.LabelField(text, style);
        }

        public static void TextRich(string text)
        {
            var style = new GUIStyle(UiStyles.TextStyle) {richText = true};
            EditorGUILayout.LabelField(text, style);
        }

        public static void TextBold(string text)
        {
            var style = new GUIStyle(UiStyles.TextStyle)
            {
                font = UiFonts.Get(FontType.NormalBold)
            };
            EditorGUILayout.LabelField(text, style);
        }

        public static void TextColoured(string text, Color color)
        {
            var style = new GUIStyle(UiStyles.TextStyle) {normal = new GUIStyleState {textColor = color}};
            EditorGUILayout.LabelField(text, style);
        }

        public static void TextSmallColoured(string text, Color color)
        {
            var style = new GUIStyle(UiStyles.TextStyle)
            {
                normal = new GUIStyleState {textColor = color}, fontSize = 10
            };
            EditorGUILayout.LabelField(text, style);
        }

        #endregion

        #region Buttons

        public static bool Button(string text, params GUILayoutOption[] layoutOptions)
        {
            var allLayoutOptions = (new[] {GUILayout.Height(22)}).Concat(layoutOptions).ToArray();

            return GUILayout.Button(text, UiStyles.ButtonStyle, allLayoutOptions);
        }

        public static bool Button(UiIcon iconType, params GUILayoutOption[] layoutOptions)
        {
            var allLayoutOptions = (new[] { GUILayout.Height(22) }).Concat(layoutOptions).ToArray();
            var buttonContent = new GUIContent(UiIcons.Get(iconType));
            return GUILayout.Button(buttonContent, UiStyles.ButtonWithIconStyle, allLayoutOptions);
        }

        public static bool Button(UiIcon iconType, string text, params GUILayoutOption[] layoutOptions)
        {
            var allLayoutOptions = (new[] { GUILayout.Height(22) }).Concat(layoutOptions).ToArray();
            var buttonContent = new GUIContent(UiIcons.Get(iconType)) {text = $" {text}"};
            return GUILayout.Button(buttonContent, UiStyles.ButtonWithIconStyle, allLayoutOptions);
        }

        public static bool ButtonTooltip(string text, string tooltip, params GUILayoutOption[] layoutOptions)
        {
            var allLayoutOptions = (new[] {GUILayout.Height(22)}).Concat(layoutOptions).ToArray();

            return GUILayout.Button(new GUIContent(text, tooltip), UiStyles.ButtonStyle, allLayoutOptions);
        }

        public static bool ButtonTooltip(UiIcon iconType, string tooltip, params GUILayoutOption[] layoutOptions)
        {
            var allLayoutOptions = (new[] {GUILayout.Height(22)}).Concat(layoutOptions).ToArray();
            var buttonContent = new GUIContent(UiIcons.Get(iconType)) {tooltip = tooltip};
            return GUILayout.Button(buttonContent, UiStyles.ButtonWithIconStyle, allLayoutOptions);
        }
        
        public static bool ButtonTooltip(UiIcon iconType, string text, string tooltip, params GUILayoutOption[] layoutOptions)
        {
            var allLayoutOptions = (new[] {GUILayout.Height(22)}).Concat(layoutOptions).ToArray();
            var buttonContent = new GUIContent(UiIcons.Get(iconType)) {text = $" {text}", tooltip = tooltip};
            return GUILayout.Button(buttonContent, UiStyles.ButtonWithIconStyle, allLayoutOptions);
        }
        
        public static bool ButtonLarge(string text, params GUILayoutOption[] layoutOptions)
        {
            var allLayoutOptions = (new[] { GUILayout.Height(40) }).Concat(layoutOptions).ToArray();
            return GUILayout.Button(text, UiStyles.ButtonLargeStyle, allLayoutOptions);
        }

        public static bool ButtonLarge(UiIcon iconType, params GUILayoutOption[] layoutOptions)
        {
            var allLayoutOptions = (new[] { GUILayout.Height(40) }).Concat(layoutOptions).ToArray();
            var buttonContent = new GUIContent(UiIcons.Get(iconType));
            return GUILayout.Button(buttonContent, UiStyles.ButtonLargeWithIconStyle, allLayoutOptions);
        }

        public static bool ButtonLarge(UiIcon iconType, string text, params GUILayoutOption[] layoutOptions)
        {
            var allLayoutOptions = (new[] { GUILayout.Height(40) }).Concat(layoutOptions).ToArray();
            var buttonContent = new GUIContent(UiIcons.Get(iconType)) { text = $" {text}" };
            return GUILayout.Button(buttonContent, UiStyles.ButtonLargeWithIconStyle, allLayoutOptions);
        }
        
        public static bool ButtonLargeTooltip(string text, string tooltip, params GUILayoutOption[] layoutOptions)
        {
            var allLayoutOptions = (new[] {GUILayout.Height(40)}).Concat(layoutOptions).ToArray();

            return GUILayout.Button(new GUIContent(text, tooltip), UiStyles.ButtonStyle, allLayoutOptions);
        }

        public static bool ButtonLargeTooltip(UiIcon iconType, string tooltip, params GUILayoutOption[] layoutOptions)
        {
            var allLayoutOptions = (new[] {GUILayout.Height(40)}).Concat(layoutOptions).ToArray();
            var buttonContent = new GUIContent(UiIcons.Get(iconType)) {tooltip = tooltip};
            return GUILayout.Button(buttonContent, UiStyles.ButtonWithIconStyle, allLayoutOptions);
        }
        
        public static bool ButtonLargeTooltip(UiIcon iconType, string text, string tooltip, params GUILayoutOption[] layoutOptions)
        {
            var allLayoutOptions = (new[] {GUILayout.Height(40)}).Concat(layoutOptions).ToArray();
            var buttonContent = new GUIContent(UiIcons.Get(iconType)) {text = $" {text}", tooltip = tooltip};
            return GUILayout.Button(buttonContent, UiStyles.ButtonWithIconStyle, allLayoutOptions);
        }

        #endregion

        #region Lists

        /// <summary>
        /// Displays a list of items and returns true, if the list was altered in any way.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="itemDrawer"></param>
        /// <returns></returns>
        public static bool List<T>(IList<T> list, Func<int, T, T> itemDrawer)
        {
            bool isOpen = true;
            return List(null, list, false, ref isOpen, itemDrawer);
        }

        /// <summary>
        /// Displays a list of items and returns true, if the list was altered in any way.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="title"></param>
        /// <param name="list"></param>
        /// <param name="itemDrawer"></param>
        /// <returns></returns>
        public static bool List<T>(string title, IList<T> list, Func<int, T, T> itemDrawer)
        {
            bool isOpen = true;
            return List(title, list, false, ref isOpen, itemDrawer);
        }

        /// <summary>
        /// Displays a list of items and returns true, if the list was altered in any way.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="title"></param>
        /// <param name="list"></param>
        /// <param name="closable"></param>
        /// <param name="isOpen"></param>
        /// <param name="itemDrawer">A function that receives the index and the item as arguments and should draw the field. It should return the result of the field.</param>
        /// <param name="itemsEqualFunc">A function that checks for equality of two items of type T. If null it uses the the types own equality check.</param>
        /// <returns></returns>
        public static bool List<T>(string title, IList<T> list, bool closable, ref bool isOpen, Func<int, T, T> itemDrawer, Func<T,T, bool> itemsEqualFunc = null)
        {
            if (itemsEqualFunc == null)
            {
                itemsEqualFunc = (item1, item2) => item1.Equals(item2);
            }
            
            // Indicates if the list has been altered in any way.
            bool dirty = false;

            if (!string.IsNullOrWhiteSpace(title) && !closable)
            {
                H3(title);
            }

            if (closable)
            {
                isOpen = EditorGUILayout.Foldout(isOpen, title, true, UiStyles.FoldoutH3);
            }

            if (!closable || isOpen)
            {
                BeginRows(true);
                for (int i = 0; i < list.Count; i++)
                {
                    BeginRow(0, true);
                    var item = list[i];
                    list[i] = itemDrawer(i, item);

                    // Update dirty to true, if item was changed.
                    if (!itemsEqualFunc(item, list[i]))
                    {
                        dirty = true;
                    }

                    if (Button(UiIcon.Remove, GUILayout.Width(23)))
                    {
                        RemoveItem(list, i);
                        dirty = true; // Altered is true, if a row is deleted.
                        break;
                    }

                    EndRow();
                }

                BeginRow(0, true);
                GUILayout.FlexibleSpace();
                if (Button(UiIcon.Add, GUILayout.Width(23)))
                {
                    AddEmptyItem(list);
                    dirty = true; // Altered is true, if a row is added.
                }

                EndRow();
                EndRows();
            }

            return dirty;
        }

        private static void RemoveItem<T>(IList<T> list, int index)
        {
            list.RemoveAt(index);
        }

        private static void AddEmptyItem<T>(IList<T> list)
        {
            list.Add(default);
        }

        #endregion

        public enum Border
        {
            Top, Right, Bottom, Left
        }
    }
}
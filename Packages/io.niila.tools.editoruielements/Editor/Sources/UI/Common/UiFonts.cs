﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.EditorUiElements.Editor
{

    public class UiFonts
    {

        private const string PackagePath = "Packages/io.niila.tools.editoruielements/";
        private const string EditorResourcePath = PackagePath + "Editor/Resources/";
        private const string EditorFontsPath = EditorResourcePath + "Fonts/";

        private static readonly Dictionary<FontType, string> FontTypeNameMap = new Dictionary<FontType, string>
        {
            { FontType.Normal, "Nunito-Light.ttf" }, { FontType.NormalBold, "Nunito-Bold.ttf" },
            { FontType.Header, "Nunito-SemiBold.ttf" },
            { FontType.Button, "Nunito-Light.ttf" }
        };

        public static Font Get(FontType type = FontType.Normal)
        {
            var fontPath = EditorFontsPath + FontTypeNameMap[type];
            var font = AssetDatabase.LoadAssetAtPath<Font>(fontPath);
            return font;
        }
    }

    public enum FontType
    {
        Normal, NormalBold,
        Header,
        Button,
    }

    public enum FontWeight
    {
        Normal,
        Bold,
        Thin
    }
}
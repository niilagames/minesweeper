﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.EditorUiElements.Editor
{

    public static class UiIcons
    {
        private const string PackagePath = "Packages/io.niila.tools.editoruielements/";
        private const string EditorResourcePath = PackagePath + "Editor/Resources/";
        private const string EditorIconsPath = EditorResourcePath + "Icons/";

        private static readonly Dictionary<UiIcon, string> IconTypeNameMap = new Dictionary<UiIcon, string>
        {
            { UiIcon.Add, "plusCircle.png" }, { UiIcon.Remove, "minusCircle.png" }, { UiIcon.Trash, "trash.png" },
            { UiIcon.MoveUp, "arrowUp.png" }, { UiIcon.MoveDown, "arrowDown.png" },
            { UiIcon.MoveToTop, "arrowUpDouble.png" }, { UiIcon.MoveToBottom, "arrowDownDouble.png" },
            { UiIcon.ArrowLeft, "arrowLeft.png" }, { UiIcon.ArrowRight, "arrowRight.png" }, { UiIcon.ArrowUp, "arrowUp.png" }, { UiIcon.ArrowDown, "arrowDown.png" },
            { UiIcon.Folder, "folder.png" }, { UiIcon.FolderOpen, "folderOpen.png" },
            { UiIcon.Home, "home.png" }, { UiIcon.Settings, "settings.png" }, { UiIcon.More, "more.png" },
            { UiIcon.Edit, "edit.png" }, { UiIcon.Save, "save.png" }, { UiIcon.Copy, "copy.png" }, { UiIcon.Paste, "paste.png" },
            { UiIcon.Filter, "filter.png" }, { UiIcon.Search, "search.png" }, { UiIcon.Eye, "eye.png" }, { UiIcon.EyeInvisible, "eyeInvisible.png" },
            { UiIcon.Info, "info.png" }, { UiIcon.Star, "star.png" }, { UiIcon.StarFilled, "starFilled.png" },
            { UiIcon.User, "user.png" }, { UiIcon.Group, "group.png" },
            { UiIcon.Book, "book.png" }, { UiIcon.BookOpen, "bookOpen.png" },
            { UiIcon.Bookmark, "bookmark.png" },
            { UiIcon.Forbidden, "forbidden.png" }, { UiIcon.Lock, "lock.png" }, { UiIcon.LockOpen, "lockOpen.png" },
            { UiIcon.Shuffle, "shuffle.png" }, { UiIcon.Refresh, "refresh.png" },
            { UiIcon.Okay, "okayCircle.png" }, { UiIcon.Cancel, "cancelCircle.png" },
            { UiIcon.Warning, "warning.png" }, { UiIcon.Error, "error.png" },
            { UiIcon.Flag, "flag.png" },
            { UiIcon.Dice, "dice.png" }, { UiIcon.Map, "map.png" },
            { UiIcon.Music, "Music.png" }, { UiIcon.Sound, "sound.png" },
        };

        public static Texture2D Get(UiIcon iconType)
        {
            var iconPath = EditorIconsPath + IconTypeNameMap[iconType];
            var icon = AssetDatabase.LoadAssetAtPath<Texture2D>(iconPath);
            return icon;
        }
    }

    public enum UiIcon
    {
        Add, Remove, Trash,
        MoveUp, MoveDown,
        MoveToTop, MoveToBottom,
        ArrowLeft, ArrowRight, ArrowUp, ArrowDown,
        Folder, FolderOpen,
        Home, Settings, More,
        Edit, Save, Copy, Paste,
        Filter, Search, Eye, EyeInvisible,
        Info, Star, StarFilled,
        User, Group,
        Book, BookOpen,
        Bookmark,
        Forbidden, Lock, LockOpen,
        Shuffle, Refresh,
        Okay, Cancel,
        Warning, Error,
        Flag,
        Dice, Map,
        Music, Sound
    }
}
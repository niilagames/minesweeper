﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.EditorUiElements.Editor.Modals
{
    public class ModalTextInput : EditorWindow
    {
        private string _title;
        private string _message;
        private string _ok;
        private string _textInput;
        private Func<string, bool> _inputValidator;
        private string _validationErrorMessage;
        private Action<string> _onOkAction;

        public static void Open(string title, string message, string ok, Action<string> onOkAction, string defaultInput = null)
        {
            Open(title, message, ok, null, null, onOkAction, defaultInput);
        }

        public static void Open(string title, string message, string ok, Func<string, bool> inputValidator, string validationErrorMessage,
            Action<string> onOkAction, string defaultInput = null)
        {
            ModalTextInput window = ScriptableObject.CreateInstance<ModalTextInput>();
            window.titleContent = new GUIContent(title);
            window._title = title;
            window._message = message;
            window._ok = ok;
            window._inputValidator = inputValidator;
            window._validationErrorMessage = validationErrorMessage;
            window._onOkAction = onOkAction;
            window._textInput = defaultInput;
            window.minSize = new Vector2(300, 150);
            window.maxSize = new Vector2(300, 150);
            window.ShowModalUtility();
        }

        private void CheckShortcuts()
        {
            if (Event.current != null && Event.current.isKey)
            {
                if (Event.current.keyCode == KeyCode.Return || Event.current.keyCode == KeyCode.KeypadEnter)
                {
                    Ok();
                }

                if (Event.current.keyCode == KeyCode.Escape)
                {
                    Cancel();
                }
            }
        }

        private void OnGUI()
        {
            UiElements.BeginPaddedArea();

            UiElements.H1(_title);
            UiElements.Text(_message);
            EditorGUILayout.Space();

            GUI.SetNextControlName("TextInput");
            _textInput = EditorGUILayout.TextField(_textInput);
            EditorGUI.FocusTextInControl("TextInput");
            if (_inputValidator != null)
            {
                if (!_inputValidator(_textInput))
                {
                    UiElements.TextSmallColoured(_validationErrorMessage, Color.red);
                }
                else
                {
                    UiElements.TextSmallColoured("Great!", new Color(0.3f, 1f, 0.3f, 1));
                }
            }

            GUILayout.FlexibleSpace();

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (UiElements.Button("Cancel"))
            {
                Cancel();
            }
            if (UiElements.Button(_ok))
            {
                Ok();
            }
            EditorGUILayout.EndHorizontal();

            UiElements.EndPaddedArea();

            // Check if any shortcuts were pressed.
            CheckShortcuts();
        }

        private void Cancel()
        {
            Close();
        }

        private void Ok()
        {
            if (_inputValidator != null)
            {
                if (_inputValidator(_textInput))
                {
                    _onOkAction(_textInput);
                    Close();
                }
            }
            else
            {
                _onOkAction(_textInput);
                Close();
            }
        }
    }
}
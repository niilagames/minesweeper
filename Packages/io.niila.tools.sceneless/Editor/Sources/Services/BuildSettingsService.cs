﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.Sceneless.Editor.Services
{
    public static class BuildSettingsService
    {
        public static bool IsSceneFirstInBuildSettings(string scenePath)
        {
            if (string.IsNullOrWhiteSpace(scenePath))
            {
                throw new ArgumentNullException("No scene path was given. Can't check if scene is first in build settings.");
            }

            return EditorBuildSettings.scenes.Length > 0 && EditorBuildSettings.scenes[0].path == scenePath;
        }

        public static bool IsSceneFirstInBuildSettings(SceneAsset sceneAsset)
        {
            if (sceneAsset == null)
            {
                throw new ArgumentNullException("No scene asset was given. Can't check if scene is first in build settings.");
            }

            return IsSceneFirstInBuildSettings(AssetDatabase.GetAssetOrScenePath(sceneAsset));
        }

        public static bool IsSceneEnabledInBuildSettings(string scenePath)
        {
            if (string.IsNullOrWhiteSpace(scenePath))
            {
                throw new ArgumentNullException("No scene path was given. Can't check if scene is enabled in build settings.");
            }

            var initEditorBuildScene = EditorBuildSettings.scenes.First(s => s.path == scenePath);
            return initEditorBuildScene.enabled;
        }

        public static bool IsSceneEnabledInBuildSettings(SceneAsset sceneAsset)
        {
            if (sceneAsset == null)
            {
                throw new ArgumentNullException("No scene asset was given. Can't check if scene is enabled in build settings.");
            }

            return IsSceneEnabledInBuildSettings(AssetDatabase.GetAssetOrScenePath(sceneAsset));
        }

        /// <summary>
        /// Makes sure that the scene is the first scene in build settings and that it is enabled.
        /// </summary>
        /// <param name="scenePath"></param>
        public static void EnsureSceneIsFirstInBuildSettings(string scenePath)
        {
            if (string.IsNullOrWhiteSpace(scenePath))
            {
                throw new ArgumentNullException("No scene path was given. Can't set scene as first scene in build settings.");
            }

            // If there are no scenes in build settings.
            if (EditorBuildSettings.scenes.Length == 0)
            {
                var newBuildSettingScenes = new List<EditorBuildSettingsScene>
                {
                    new EditorBuildSettingsScene(scenePath, true),
                };
                EditorBuildSettings.scenes = newBuildSettingScenes.ToArray();
            }
            // If it is already first in build settings, make sure to enable it.
            else if (EditorBuildSettings.scenes[0].path == scenePath)
            {
                EditorBuildSettings.scenes[0].enabled = true;
            }
            else // If it exists already, but is not first, remove it and add it again in front. If it doesn't exist, add it in front.
            {
                var newBuildSettingScenes = new List<EditorBuildSettingsScene>(EditorBuildSettings.scenes.Where(s => s.path != scenePath));
                newBuildSettingScenes.Prepend(new EditorBuildSettingsScene(scenePath, true));
                EditorBuildSettings.scenes = newBuildSettingScenes.ToArray();
            }

            if (EditorBuildSettings.scenes.Length == 0 || EditorBuildSettings.scenes[0].path != scenePath ||
                EditorBuildSettings.scenes[0].enabled != true)
            {
                Debug.LogError($"An error occurred, while trying to add the {scenePath} scene to build settings.\nTry adding it as the first scene in build settings and make sure it is enabled!");
            }
        }

        /// <summary>
        /// Makes sure that the scene is the first scene in build settings and that it is enabled.
        /// </summary>
        /// <param name="scenePath"></param>
        public static void EnsureSceneIsFirstInBuildSettings(SceneAsset sceneAsset)
        {
            if (sceneAsset == null)
            {
                throw new ArgumentNullException("No scene asset was given. Can't set scene as first scene in build settings.");
            }

            EnsureSceneIsFirstInBuildSettings(AssetDatabase.GetAssetOrScenePath(sceneAsset));
        }
    }
}
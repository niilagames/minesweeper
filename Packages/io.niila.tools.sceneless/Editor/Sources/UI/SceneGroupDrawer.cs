﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Niila.Tools.EditorUiElements.Editor;
using Niila.Tools.Sceneless.Editor.Sources;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.Sceneless.Editor
{

    public class SceneGroupDrawer : UiElement<SceneGroup>
    {
        private static readonly ReflectionCacher FieldCacher = new ReflectionCacher();

        private bool _mainSceneListOpen, _uiSceneListClosed, _environmentSceneListClosed, _otherSceneListClosed;

        private static readonly GUIContent ActiveSceneLabel = new GUIContent(
            "Active scene",
            "The scene that will be set as active after load. This influences things like lighting etc.");

        private static readonly GUIContent MainCameraSceneLabel = new GUIContent("Main camera scene",
            "The main camera scene should be the one containing the main camera. This scene will be loaded first to remove potential flicker between scene group loads.");

        public override void Draw(SceneGroup sceneGroup)
        {
            // Indicates if the list has been altered in any way.
            bool dirty = false;

            SetSceneGroupField("_name", sceneGroup, EditorGUILayout.TextField("Name", sceneGroup.Name));
            EditorGUILayout.LabelField("Description");
            SetSceneGroupField("_description", sceneGroup,
                EditorGUILayout.TextArea(sceneGroup.Description, GUILayout.Height(55)));

            GUILayout.Space(10);

            var mainScenes = GetSceneGroupField<List<SceneReference>>("_mainScenes", sceneGroup);
            dirty = dirty || UiElements.List($"Main scenes ({SceneCounter(mainScenes)})", mainScenes, true,
                        ref _mainSceneListOpen, DrawSceneItem, SceneReferenceEquality);
            var uiScenes = GetSceneGroupField<List<SceneReference>>("_uiScenes", sceneGroup);
            dirty = dirty || UiElements.List($"UI scenes ({SceneCounter(uiScenes)})", uiScenes, true,
                        ref _uiSceneListClosed, DrawSceneItem, SceneReferenceEquality);
            var environmentScenes = GetSceneGroupField<List<SceneReference>>("_environmentScenes", sceneGroup);
            dirty = dirty || UiElements.List($"Environment scenes ({SceneCounter(environmentScenes)})",
                        environmentScenes, true, ref _environmentSceneListClosed, DrawSceneItem, SceneReferenceEquality);
            var otherScenes = GetSceneGroupField<List<SceneReference>>("_otherScenes", sceneGroup);
            dirty = dirty || UiElements.List($"Other scenes ({SceneCounter(otherScenes)})", otherScenes, true,
                        ref _otherSceneListClosed, DrawSceneItem, SceneReferenceEquality);

            if (dirty)
            {
                Debug.Log("Scenes have changed!");
            }
            
            GUILayout.Space(10);
            // Don't draw the dropdowns until there are scenes that can be selected.
            var allScenes = sceneGroup.AllScenes.ToList();
            if (allScenes.Any())
            {
                // Active scene dropdown.
                var selectedActiveScene = sceneGroup.ActiveScene;
                if (selectedActiveScene == null || string.IsNullOrWhiteSpace(selectedActiveScene.ScenePath) ||
                    !allScenes.Contains(selectedActiveScene))
                {
                    selectedActiveScene = allScenes.First();
                }

                dirty = dirty || SetSceneGroupField("_activeScene", sceneGroup,
                            DrawSceneDropdown(ActiveSceneLabel, allScenes.ToArray(), selectedActiveScene));

                // Main camera scene dropdown.
                var selectedMainCameraScene = sceneGroup.MainCameraScene;
                if (selectedMainCameraScene == null || string.IsNullOrWhiteSpace(selectedMainCameraScene.ScenePath) ||
                    !allScenes.Contains(selectedMainCameraScene))
                {
                    selectedMainCameraScene = allScenes.First();
                }

                dirty = dirty || SetSceneGroupField("_mainCameraScene", sceneGroup,
                            DrawSceneDropdown(MainCameraSceneLabel, allScenes.ToArray(), selectedMainCameraScene));
            }
            else
            {
                EditorGUILayout.HelpBox(
                    "You need to add scenes to the scene group, before you can select things like active scene etc.",
                    MessageType.Warning);
            }

            // If the scene group was altered in any way, mark it as dirty.
            if (dirty)
            {
                EditorUtility.SetDirty(sceneGroup);
            }
        }

        public override void Update()
        {

        }

        /// <summary>
        /// Returns a string in the format: {valid scene reference count}/{total scene references}.
        /// This means that if some of the scene references in the list are null or not valid,
        /// they would not count towards the number before the slash.
        /// </summary>
        /// <param name="sceneReferences"></param>
        /// <returns></returns>
        private string SceneCounter(List<SceneReference> sceneReferences)
        {
            return
                $"{sceneReferences.Count(s => s != null && !string.IsNullOrWhiteSpace(s.ScenePath))}/{sceneReferences.Count}";
        }

        private SceneReference DrawSceneItem(int index, SceneReference sceneReference)
        {
            var sceneAsset = sceneReference != null && !string.IsNullOrWhiteSpace(sceneReference.ScenePath)
                ? FieldCacher.GetField<SceneAsset>("sceneAsset", sceneReference)
                : null;
            var selectedScene = EditorGUILayout.ObjectField(sceneAsset, typeof(SceneAsset), false);
            // If no scene was selected, return null.
            if (selectedScene == null)
            {
                return sceneReference;
            }

            // If scene reference was null, create new scene reference.
            if (sceneReference == null)
            {
                sceneReference = new SceneReference();
            }

            // Set scene asset field on scene reference before returning the scene reference.
            FieldCacher.SetField("sceneAsset", sceneReference, selectedScene);
            return sceneReference;
        }

        private bool SceneReferenceEquality(SceneReference sr1, SceneReference sr2)
        {
            // If both null, they are equal.
            if (sr1 == null && sr2 == null)
            {
                return true;
            }

            // If one of them is null, they are not equal.
            if ((sr1 == null && sr2 != null) || (sr1 != null && sr2 == null))
            {
                return false;
            }
            
            // If both are not null, they are equal if they refer to the same scene asset.
            return sr1.ScenePath == sr2.ScenePath;
        }

        private SceneReference DrawSceneDropdown(GUIContent label, SceneReference[] sceneReferences,
            SceneReference selectedSceneReference)
        {
            var validItems = sceneReferences.Where(sr => sr != null && !string.IsNullOrWhiteSpace(sr.ScenePath)).ToArray();
            var selectedIndex =
                Array.FindIndex(validItems, sr => sr.ScenePath == selectedSceneReference.ScenePath);
            var dropdownItems = validItems.Select(sr => FieldCacher.GetField<SceneAsset>("sceneAsset", sr).name).ToArray();
            int newSelectedIndex = EditorGUILayout.Popup(label, selectedIndex, dropdownItems);
            return sceneReferences[newSelectedIndex];
        }

        private bool SetSceneGroupField<T>(string fieldName, object obj, T value)
        {
            var prevValue = FieldCacher.GetField<T>(fieldName, obj);
            if (!value.Equals(prevValue))
            {
                FieldCacher.SetField(fieldName, obj, value);
                return true;
            }

            return false;
        }

        private T GetSceneGroupField<T>(string fieldName, object obj)
        {
            return FieldCacher.GetField<T>(fieldName, obj);
        }
    }
}
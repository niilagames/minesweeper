﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Niila.Tools.EditorUiElements.Editor;
using Niila.Tools.Sceneless;
using Niila.Tools.Sceneless.Settings;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.Sceneless.Editor
{
    public class SmartRenamerElement : UiElement
    {
        public ScenelessSetup SceneSetup { get; set; }

        private string _namePattern = "";

        private static Dictionary<Type, Dictionary<string, FieldInfo>> _objFields =
            new Dictionary<Type, Dictionary<string, FieldInfo>>();

        private bool _showAvailablePatterns = false;

        public SmartRenamerElement(ScenelessSetup sceneSetup)
        {
            SceneSetup = sceneSetup;
        }

        public override void OnEnable()
        {
            _namePattern = ScenelessEditorSettings.SmartRenamerPattern;
        }

        public override void OnDisable()
        {
            ScenelessEditorSettings.SmartRenamerPattern = _namePattern;
        }

        public override void Draw()
        {
            if (SceneSetup == null)
            {
                return;
            }

            EditorGUILayout.BeginVertical();

            UiElements.H2("Smart Renamer");
            UiElements.Text(
                "Rename all scene groups (or selected scene groups) based on a pattern.");

            EditorGUILayout.Space();

            _namePattern = EditorGUILayout.TextField("Name pattern", _namePattern);

            EditorGUILayout.Space();

            if (UiElements.Button(UiIcon.Edit, "Rename"))
            {
                ScenelessEditorUtils.EnsureFolderStructure(ScenelessEditorUtils.FullResourcePath);

                var useSelectedSceneGroups = ScenelessEditorUtils.SelectedSceneGroups.Count > 0;

                // The selected index indicates the current index iteration
                // compared to only the selected scene groups.
                // The index below will be for all scene groups.
                var selectedIndex = 0;

                for (var i = 0; i < SceneSetup.SceneGroups.Count; i++)
                {
                    // If we have no selected scene groups, then use i as the selected index, as it would be the same.
                    if (!useSelectedSceneGroups)
                    {
                        selectedIndex = i;
                    }
                    
                    var sceneGroup = SceneSetup.SceneGroups[i];
                    // Only rename, if no scene groups are selected
                    // or if this scene group is part of seleceted scene groups.
                    if (!useSelectedSceneGroups || ScenelessEditorUtils.SelectedSceneGroups.Contains(sceneGroup))
                    {
                        var sgName = _namePattern
                            .Replace("#index+1", (i + 1).ToString())
                            .Replace("#index", i.ToString())
                            .Replace("#selected-index+1", (selectedIndex + 1).ToString())
                            .Replace("#selected-index", selectedIndex.ToString());
                        SetField("_name", sceneGroup, sgName);
                        sceneGroup.name = sgName;
                        EditorUtility.SetDirty(sceneGroup);
                    }

                    // If we have selected scene groups, remember to update selected index
                    // after each time we've encountered one of the selected scene groups.
                    if (useSelectedSceneGroups)
                    {
                        if (ScenelessEditorUtils.SelectedSceneGroups.Contains(sceneGroup))
                        {
                            selectedIndex++;
                        }
                    }
                }

                EditorUtility.SetDirty(SceneSetup);
                AssetDatabase.SaveAssets();
            }

            _showAvailablePatterns = EditorGUILayout.Foldout(_showAvailablePatterns, "Available patterns", true);
            if (_showAvailablePatterns)
            {
                UiElements.TextRich(
                    "<size=11>The patterns below are replaced by calculated values. Normal text is also fine.</size>");
                UiElements.TextRich(
                    "<size=11>Patterns: '#index', '#index+1', '#selected-index', '#selected-index+1'.</size>");
                UiElements.TextRich("<size=11><i>Example: 'Level #index+1'.</i></size>");
            }

            EditorGUILayout.EndVertical();
        }

        public override void Update()
        {
        }

        private void SetField<T>(string fieldName, object obj, T value)
        {
            var field = GetCachedField(fieldName, obj.GetType());
            field.SetValue(obj, value);
        }

        private T GetField<T>(string fieldName, object obj)
        {
            var field = GetCachedField(fieldName, obj.GetType());
            return (T) field.GetValue(obj);
        }

        private FieldInfo GetCachedField(string fieldName, Type objType)
        {
            // Check if we have any fields for the given object type.
            if (_objFields.ContainsKey(objType))
            {
                // Check if we have the specific field for the given object type.
                if (_objFields[objType].ContainsKey(fieldName))
                {
                    return _objFields[objType][fieldName];
                }

                // Find field, cache it and return it.
                var field = objType.GetField(fieldName,
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                _objFields[objType].Add(fieldName, field);
                return field;
            }

            // Add object type, find field, cache it and return it.
            _objFields.Add(objType, new Dictionary<string, FieldInfo>());
            var fieldInfo = objType.GetField(fieldName,
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            _objFields[objType].Add(fieldName, fieldInfo);
            return fieldInfo;
        }
    }
}
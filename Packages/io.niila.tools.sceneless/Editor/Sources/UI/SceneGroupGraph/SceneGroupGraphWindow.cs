﻿using System;
using System.Collections.Generic;
using System.Linq;
using Editor.Sources.UI.SceneGroupGraph.GraphGenerators;
using Niila.Tools.Sceneless.Editor;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.Sources.UI.SceneGroupGraph
{
    public class SceneGroupGraphWindow : EditorWindow
    {

        public static List<SceneGroupGraphWindow> OpenWindows => _openWindows.ToList();
        
        private static List<SceneGroupGraphWindow> _openWindows = new List<SceneGroupGraphWindow>();
        
        private SceneGroupGraphView _graphView;
        private GraphSaveLoadUtil _saveLoadUtil;
        
        [MenuItem("Tools/Sceneless/Sceneless Graph")]
        public static void OpenWindow()
        {
            var window = GetWindow<SceneGroupGraphWindow>();
            window.titleContent = new GUIContent("Sceneless Graph");
        }

        [MenuItem("Tools/Sceneless/Sceneless Graph", true)]
        public static bool CanOpenScenelessGraph()
        {
            return ScenelessEditorUtils.IsSetupComplete();
        }

        public void ReloadGraphData()
        {
            // Load from file to refresh the graph.
            _saveLoadUtil?.LoadGraph();
        }
        
        private void ConstructGraphView(bool enableMiniMap)
        {
            _graphView = new SceneGroupGraphView(this)
            {
                name = "Sceneless Graph"
            };
            _saveLoadUtil = new GraphSaveLoadUtil(_graphView);
            
            // Load graph, if graph data exists on disk.
            if (_saveLoadUtil.GraphDataExists())
            {
                _saveLoadUtil.LoadGraph();
            }
            
            rootVisualElement.Add(_graphView);
            _graphView.StretchToParentSize();

            if (enableMiniMap)
            {
                var miniMapPos = new Vector2(10, 30);
                var miniMap = new MiniMap{anchored = true};
                miniMap.SetPosition(new Rect(miniMapPos, new Vector2(200, 140)));
                _graphView.Add(miniMap);
            }
        }

        private void ConstructToolbar()
        {
            var toolbar = new Toolbar();
            toolbar.Add(new Button( SaveSceneGraph ) { text = "Save"} );
            toolbar.Add(new Button( LoadSceneGraph ) { text = "Load"} );
            toolbar.Add(new Label("  "));
            toolbar.Add(new Button( GenerateLinearGraph ) { text = "Generate linear graph" });
            toolbar.Add(new Label("  "));
            toolbar.Add(new Button( ResetView ) { text = "Reset view" });
            
            rootVisualElement.Add(toolbar);
        }

        private void SaveSceneGraph()
        {
            _saveLoadUtil.SaveGraph();
        }

        private void LoadSceneGraph()
        {
            _saveLoadUtil.LoadGraph();
        }
        
        private void GenerateLinearGraph()
        {
            var sceneGroups = ScenelessEditorUtils.SceneSetup.SceneGroups;
            (new LinearGraphGenerator()).GenerateGraph(_graphView, sceneGroups);
        }
        
        private void ResetView()
        {
            _graphView.ResetViewTransform();
        }

        private void OnEnable()
        {
            _openWindows.Add(this);
            ConstructGraphView(true);
            ConstructToolbar();
        }

        private void OnDisable()
        {
            rootVisualElement.Remove(_graphView);
            _openWindows.Remove(this);
        }
    }
}
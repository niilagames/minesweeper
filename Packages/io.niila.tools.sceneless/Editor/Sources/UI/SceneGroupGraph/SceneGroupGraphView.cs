﻿using System;
using System.Collections.Generic;
using System.Linq;
using Editor.Sources.UI.SceneGroupGraph.Nodes;
using Niila.Tools.Sceneless;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.Sources.UI.SceneGroupGraph
{
    public class SceneGroupGraphView : GraphView
    {

        private EditorWindow _window;
        private NodeCreatorPopup _nodeCreatorPopup;

        public SceneGroupGraphView(EditorWindow window)
        {
            _window = window;
            
            // Add stylesheet.
            styleSheets.Add(Resources.Load<StyleSheet>("ScenelessGraph"));
            
            // Make zooming possible.
            SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);
            
            // Make dragging and rectable selection possible.
            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());
            
            // Style the background and grid.
            var grid = new GridBackground();
            Insert(0, grid);
            grid.StretchToParentSize();

            // Reset the graph view and the viewTransform.
            Reset();
            ResetViewTransform();
            
            // Add a node creator popup window.
            _nodeCreatorPopup = ScriptableObject.CreateInstance<NodeCreatorPopup>();
            _nodeCreatorPopup.Init(this);
            nodeCreationRequest = context =>
                SearchWindow.Open(new SearchWindowContext(context.screenMousePosition), _nodeCreatorPopup);
            
            // Setup change handler.
            graphViewChanged += OnGraphViewChanged;
        }
        
        /// <summary>
        /// Clears all elements in the graph and creates a new entry node.
        /// </summary>
        public void Reset()
        {
            // Clear all nodes and edges from the graph view.
            DeleteElements(nodes.ToList());
            DeleteElements(edges.ToList());
            
            // Create entry node.
            var entryNode = CreateEntryNode();
            AddElement(entryNode);
        }

        // Resets the view to the initial position.
        public void ResetViewTransform()
        {
            viewTransform.position = new Vector2(100, 250);
            viewTransform.scale = Vector3.one;
        }
        
        /// <summary>
        /// Enables us to control which ports can be connected and which cannot. 
        /// </summary>
        /// <param name="startPort"></param>
        /// <param name="nodeAdapter"></param>
        /// <returns></returns>
        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            var compatiblePorts = new List<Port>();
            
            ports.ForEach(port =>
            {
                // A port is compatible, if the port is not itself or on the same node.
                if (startPort != port && startPort.node != port.node)
                {
                    compatiblePorts.Add(port);
                }
            });

            return compatiblePorts;
        }

        private GraphViewChange OnGraphViewChanged(GraphViewChange change)
        {
            if (change.elementsToRemove != null)
            {
                // Create list to add other elements to remove.
                var extraElementsToRemove = new List<GraphElement>();
                
                // Get nodes that are getting removed.
                var nodesToRemove = change.elementsToRemove.OfType<BaseNode>().ToList();
                foreach (var nodeToRemove in nodesToRemove)
                {
                    switch (nodeToRemove)
                    {
                        case RedirectNode rn:
                            extraElementsToRemove.Add(rn.ReceiverNode);
                            extraElementsToRemove.AddRange(rn.ReceiverNode.GetEdges());
                            break;
                        case RedirectReceiverNode rrn:
                            extraElementsToRemove.Add(rrn.RedirectNode);
                            extraElementsToRemove.AddRange(rrn.RedirectNode.GetEdges());
                            break;
                    }
                }
                
                // Add all the new nodes that should be removed to change list.
                change.elementsToRemove = change.elementsToRemove.Concat(extraElementsToRemove).Distinct().ToList();
            }

            // Return the change.
            return change;
        }

        /// <summary>
        /// Uses the screenMousePosition and the editor window to find the local position inside the graph view.
        /// The screenMousePosition can for instance be found inside some context objects.
        /// </summary>
        /// <param name="screenMousePosition"></param>
        /// <returns></returns>
        public Vector2 GetMousePosition(Vector2 screenMousePosition)
        {
            // Get mouse position inside graph view.
            var worldPosition =
                _window.rootVisualElement.ChangeCoordinatesTo(_window.rootVisualElement.parent,
                    screenMousePosition - _window.position.position);
            var localPosition = contentViewContainer.WorldToLocal(worldPosition);
            // Return found position.
            return localPosition;
        }
        
        
        /// <summary>
        /// Creates the graph's entry node. The place where it all starts. The birth of the Universe!
        /// </summary>
        /// <returns></returns>
        private Node CreateEntryNode()
        {
            // Create node.
            var node = new SceneGroupEntryNode("ENTRY");
            
            // Set position.
            node.SetPosition(new Rect(new Vector2(0, 0), node.DefaultSize));
            
            // Return the node.
            return node;
        }
    }
}
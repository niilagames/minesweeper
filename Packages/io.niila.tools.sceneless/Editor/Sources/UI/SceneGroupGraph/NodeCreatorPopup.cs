﻿using System;
using System.Collections.Generic;
using System.Linq;
using Editor.Sources.UI.SceneGroupGraph.Nodes;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace Editor.Sources.UI.SceneGroupGraph
{
    public class NodeCreatorPopup : ScriptableObject, ISearchWindowProvider
    {
        private SceneGroupGraphView _graphView;

        public void Init(SceneGroupGraphView graphView)
        {
            _graphView = graphView;
        }

        public List<SearchTreeEntry> CreateSearchTree(SearchWindowContext context)
        {
            var nodeEntries = GetNodeEntries();
            
            // Save a set of existing paths used for checking if a group or entry has already been created.
            var existingPaths = new HashSet<string>();

            // Create search tree with initial header at level 0.
            var tree = new List<SearchTreeEntry>
            {
                new SearchTreeGroupEntry(new GUIContent("Create node"), 0)
            };
            
            foreach (var entry in nodeEntries)
            {
                // If the entry's full path already exsits, throw an exception. No two nodes can share a path.
                if (existingPaths.Contains(entry.Path))
                {
                    throw new ArgumentException($"Cannot add entry for node type {entry.NodeType} as another node already claimed that path.");
                }
                
                // Split the path into its segments.
                var pathSegments = entry.Path.Split('/');

                for (var i = 0; i < pathSegments.Length; i++)
                {
                    var pathSegment = pathSegments[i];
                    var currentPath = pathSegments.Take(i + 1).Aggregate((acc, ps) => $"{acc}/{ps}");
                    // If the current part of the path has already been created skip ahead.
                    if (existingPaths.Contains(currentPath))
                    {
                        continue;
                    }

                    // Tree levels start at 1, as 0 is reserved for the header.
                    var treeLevel = i + 1;
                    var isLastSegment = i >= pathSegments.Length - 1;
                    if (!isLastSegment)
                    {
                        // If this is not the last segment then it is a group. Create it and add it to the list.
                        var treeGroup = new SearchTreeGroupEntry(new GUIContent(pathSegment), treeLevel);
                        tree.Add(treeGroup);
                    }
                    else
                    {
                        // If this is the last entry it represents the node entry. Create the entry. 
                        var treeEntry = new SearchTreeEntry(new GUIContent(pathSegment))
                        {
                            userData = entry, level = treeLevel,
                        };
                        tree.Add(treeEntry);
                    }
                    
                    // Add current part of path to existing paths;
                    existingPaths.Add(currentPath);
                }
            }

            return tree;
        }

        public bool OnSelectEntry(SearchTreeEntry searchTreeEntry, SearchWindowContext context)
        {
            // Get node creator entry.
            var entry = (INodeCreatorPopupEntry) searchTreeEntry.userData;
            
            // Get mouse position inside graph view.
            var position = _graphView.GetMousePosition(context.screenMousePosition);
            
            // Create node(s).
            var nodeResult = entry.CreateNode();

            // Position and add nodes to graph view.
            nodeResult.AddToGraph(_graphView, position);

            return true;
        }

        private INodeCreatorPopupEntry[] GetNodeEntries()
        {
            // Find all classes that implement the INodeCreatorPopupEntry interface.
            var nodeEntryTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes()).Where(t =>
                t.IsClass && typeof(INodeCreatorPopupEntry).IsAssignableFrom(t));
            // Instantiate a version of each type.
            var nodeEntryObjs = nodeEntryTypes.Select(type => (INodeCreatorPopupEntry) Activator.CreateInstance(type));
            // Order the entries.
            var orderedNodeEntries = nodeEntryObjs.OrderBy(e => e.Order);
            // Return them as an array.
            return orderedNodeEntries.ToArray();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Editor.Sources.UI.SceneGroupGraph.Nodes;
using Niila.Tools.Sceneless.Editor;
using Niila.Tools.Sceneless.SceneGroupGraph;
using Niila.Tools.Sceneless.SceneGroupGraph.NodeData;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace Editor.Sources.UI.SceneGroupGraph
{
    public class GraphSaveLoadUtil
    {
        private const string SaveFileName = "SceneGraph";
        private SceneGroupGraphView _graphView;
        private NodeFactory _nodeFactory;
        
        public GraphSaveLoadUtil(SceneGroupGraphView graphView)
        {
            _graphView = graphView;
            _nodeFactory = new NodeFactory(graphView);
        }

        /// <summary>
        /// Saves the current data in the graph view.
        /// </summary>
        public void SaveGraph()
        {
            // Get nodes and edges.
            var entryNode = _graphView.nodes.ToList().OfType<SceneGroupEntryNode>().First();
            var nodes = _graphView.nodes.ToList().Where(node => !(node is SceneGroupEntryNode)).Cast<BaseNode>().ToList();
            var edges = _graphView.edges.ToList();

            // Create (or get) an instance of the graph data scriptable object.
            var graphData = ScenelessEditorUtils.GetOrCreateAsset<SceneGroupGraphData>(SaveFileName);
            
            // Clear it before saving again.
            graphData.Clear();
            
            // Save nodes.
            graphData.EntryNodeGUID = entryNode.GUID;
            foreach (var node in nodes)
            {
                graphData.AddNodeData(GetNodeDataFromNode(node));
            }
            
            // Save edges.
            var connectedPorts = edges.Where(edge => edge.input.node != null && edge.output.node != null).ToArray();
            foreach (var port in connectedPorts)
            {
                var baseNode = (BaseNode)port.output.node;
                var targetNode = (BaseNode)port.input.node;
                
                graphData.EdgeData.Add(new EdgeData(baseNode.GUID, targetNode.GUID));
            }
            
            // Save graph data.
            EditorUtility.SetDirty(graphData);
            AssetDatabase.SaveAssets();
        }

        /// <summary>
        /// Checks whether or not saved graph data exists.
        /// </summary>
        /// <returns></returns>
        public bool GraphDataExists()
        {
            return ScenelessEditorUtils.GetAsset<SceneGroupGraphData>(SaveFileName) != null;
        }
        
        /// <summary>
        /// Loads the graph data into the connected graph view.
        /// </summary>
        public void LoadGraph()
        {
            var graphData = ScenelessEditorUtils.GetAsset<SceneGroupGraphData>(SaveFileName);
            if (graphData == null)
            {
                throw new NullReferenceException("Cannot load saved graph as no save data exists.");
            }
            
            // Reset the current graph view.
            _graphView.Reset();
            
            // Get nodes and edges.
            var entryNode = _graphView.nodes.ToList().OfType<SceneGroupEntryNode>().First();
            var loadedNodes = graphData.NodeData.ToList();
            var loadedEdges = graphData.EdgeData;
            
            // Update the graph view.
            
            // Update entry node GUID.
            entryNode.GUID = graphData.EntryNodeGUID;

            // Create temp dictionary for nodes used for connecting them afterwards.
            Dictionary<string, BaseNode> nodeMap = new Dictionary<string, BaseNode>();
            
            // Add nodes to the graph view.
            foreach (var nodeData in loadedNodes)
            {
                CreateNodeFromNodeData(nodeData, ref loadedNodes, ref nodeMap);
            }
            
            // Add edges to the graph.
            foreach (var edgeData in loadedEdges)
            {
                // Find nodes.
                Node baseNode = entryNode.GUID == edgeData.BaseNodeGUID ? (Node) entryNode : nodeMap[edgeData.BaseNodeGUID];
                var targetNode = nodeMap[edgeData.TargetNodeGUID];

                // Find ports.
                var outputPort = baseNode.outputContainer[0] as Port;
                var inputPort = targetNode.inputContainer[0] as Port;
                
                // Create new edge.
                var edge = new Edge
                {
                    output = outputPort,
                    input = inputPort
                };
                
                // Connect the edge.
                edge.input.Connect(edge);
                edge.output.Connect(edge);
                
                // Add edge to graph view.
                _graphView.Add(edge);
                
            }
        }
        
        private BaseNodeData GetNodeDataFromNode(BaseNode node)
        {
            switch (node)
            {
                case SceneGroupNode sgn:
                    return new SceneGroupNodeData(sgn.GUID, sgn.SceneGroup, sgn.GetPosition().position);
                case RedirectNode rn:
                    return new RedirectNodeData(rn.GUID, rn.RedirectName, rn.ReceiverNode.GUID, rn.GetPosition().position);
                case RedirectReceiverNode rrn:
                    return new RedirectReceiverNodeData(rrn.GUID, rrn.RedirectName, rrn.RedirectNode.GUID, rrn.GetPosition().position);
                default:
                    throw new ArgumentOutOfRangeException($"Creating node data for the given node with type {node.GetType().Name} isn't implemented yet.");
            }
        }

        /// <summary>
        /// Creates a node (or more) based on the type of node data and adds it to the graph view.
        /// It also adds the node(s) to the given node map.
        /// </summary>
        /// <param name="nodeData"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        private void CreateNodeFromNodeData(BaseNodeData nodeData, ref List<BaseNodeData> allNodeData, ref Dictionary<string, BaseNode> createdNodes)
        {
            switch (nodeData)
            {
                case SceneGroupNodeData sgnData:
                    var sgn = _nodeFactory.CreateNode(typeof(SceneGroupNode)).AddToGraph(_graphView, sgnData.Position).GetNode<SceneGroupNode>();
                    sgn.GUID = sgnData.GUID;
                    sgn.SceneGroup = sgnData.SceneGroup;
                    createdNodes.Add(sgn.GUID, sgn);
                    break;
                case RedirectNodeData rnData:
                    // Create redirector node (which also creates a receiver node) and set GUID.
                    var rn = _nodeFactory.CreateNode(typeof(RedirectNode)).AddToGraph(_graphView, rnData.Position).GetNode<RedirectNode>();
                    rn.GUID = rnData.GUID;
                    // Get receiver node and receiver node data.
                    var rrnData = (RedirectReceiverNodeData)allNodeData.First(nd => nd.GUID == rnData.ReceiverNodeGUID);
                    var rrn = rn.ReceiverNode;
                    rrn.GUID = rrnData.GUID;
                    rrn.SetPosition(new Rect(rrnData.Position, rrn.DefaultSize));
                    
                    // Set redirect name for both.
                    rn.SetRedirectName(rnData.RedirectName);
                    rrn.SetRedirectName(rrnData.RedirectName);
                    
                    // Add both to the map of created nodes.
                    createdNodes.Add(rn.GUID, rn);
                    createdNodes.Add(rrn.GUID, rrn);
                    break;
                case RedirectReceiverNodeData rrnDataDummy:
                    // We handle this case in the redirect node data case, but this case is needed,
                    // so it doesn't throw an exception.
                    break;
                default:
                    throw new ArgumentOutOfRangeException($"Creating new node for the given node data with type {nodeData.GetType().Name} isn't implemented yet.");
            }
        }
    }
}
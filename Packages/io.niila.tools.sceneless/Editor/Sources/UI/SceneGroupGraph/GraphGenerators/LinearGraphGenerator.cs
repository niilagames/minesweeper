﻿using System.Collections.Generic;
using System.Linq;
using Editor.Sources.UI.SceneGroupGraph.Nodes;
using Niila.Tools.Sceneless;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace Editor.Sources.UI.SceneGroupGraph.GraphGenerators
{
    public class LinearGraphGenerator : IGraphGenerator
    {
        public void GenerateGraph(SceneGroupGraphView graphView, List<SceneGroup> sceneGroups,
            bool skipWarnings = false)
        {
            if (!skipWarnings)
            {
                // Ask if the user wants to clear the whole graph.
                var isOkay = EditorUtility.DisplayDialog("This will clear your current graph",
                    "Are you sure you want to continue?",
                    "Yes, continue", "No, don't clear my graph");
                // If they do not, return before clearing.
                if (!isOkay)
                {
                    return;
                }
            }

            // Reset the current graph view.
            graphView.Reset();

            // Create node factory.
            var nodeFactory = new NodeFactory(graphView);

            // Get entry node.
            var entryNode = graphView.nodes.ToList().OfType<SceneGroupEntryNode>().First();

            // Add nodes to the graph view.
            Node prevNode = null;
            for (var i = 0; i < sceneGroups.Count; i++)
            {
                // Get the scene group from the list.
                var sceneGroup = sceneGroups[i];

                // Create and add a new node for the scene group and add data.
                var position = new Vector2((i + 1) * 250, 200);
                var node = nodeFactory.CreateNode(typeof(SceneGroupNode)).AddToGraph(graphView, position).GetNode<SceneGroupNode>();
                node.SceneGroup = sceneGroup;

                // If there is no prev node, use the entry node as previous node.
                if (prevNode == null)
                {
                    prevNode = entryNode;
                }

                // Find ports on nodes.
                var outputPort = prevNode.outputContainer[0] as Port;
                var inputPort = node.inputContainer[0] as Port;

                // Create the edge.
                var edge = new Edge
                {
                    output = outputPort,
                    input = inputPort,
                };

                // Connect the edge.
                edge.input.Connect(edge);
                edge.input.Connect(edge);

                // Add edge to graph view.
                graphView.Add(edge);

                // Save current node as prev node.
                prevNode = node;
            }
        }
    }
}
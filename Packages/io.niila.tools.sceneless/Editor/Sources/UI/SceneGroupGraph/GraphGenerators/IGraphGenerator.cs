﻿using System.Collections.Generic;
using Niila.Tools.Sceneless;

namespace Editor.Sources.UI.SceneGroupGraph.GraphGenerators
{
    public interface IGraphGenerator
    {
        void GenerateGraph(SceneGroupGraphView graphView, List<SceneGroup> sceneGroups, bool skipWarnings = false);
    }
}
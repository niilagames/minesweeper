﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Editor.Sources.UI.SceneGroupGraph.Nodes
{
    public class NodeFactory
    {
        private readonly SceneGroupGraphView _graphView;
        private readonly Vector2 _defaultPosition = new Vector2(300, 200);

        private int _redirectNodesCreated = 0;

        private Dictionary<Type, INodeCreatorPopupEntry> _nodeCreators;
        
        public NodeFactory(SceneGroupGraphView graphView)
        {
            _graphView = graphView;
            
            _nodeCreators = new Dictionary<Type, INodeCreatorPopupEntry>
            {
                {typeof(SceneGroupNode), new SceneGroupNodeEntry()},
                {typeof(RedirectNode), new RedirectNodeEntry()}
            };
        }

        public NodeCreationResult CreateNode(Type nodeType)
        {
            var creator = _nodeCreators[nodeType];
            return creator.CreateNode();
        }
    }
}
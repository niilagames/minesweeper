﻿using UnityEngine;

namespace Editor.Sources.UI.SceneGroupGraph.Nodes
{
    public class NodeCreationResult
    {
        public BaseNode Node { get; private set; }
        public BaseNode[] RelatedNodes { get; private set; }
        
        public NodeCreationResult(BaseNode node, params BaseNode[] relatedNodes)
        {
            Node = node;
            RelatedNodes = relatedNodes;
        }
        
        /// <summary>
        /// Returns the node cast to the given type, if possible. Returns null otherwise.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetNode<T>() where T : BaseNode => Node as T;

        /// <summary>
        /// Positions and then adds the node (and related nodes, if not excluded) to the graph view. 
        /// </summary>
        /// <param name="graphView"></param>
        /// <param name="position"></param>
        /// <param name="excludeRelatedNodes"></param>
        public NodeCreationResult AddToGraph(SceneGroupGraphView graphView, Vector2 position, bool excludeRelatedNodes = false)
        {
            // Position node and add to graph view.
            var pos = position;
            Node.SetPosition(new Rect(pos, Node.DefaultSize));
            graphView.AddElement(Node);

            // If excluding related nodes, don't position and add them to the graph, but return instead.
            if (excludeRelatedNodes)
            {
                return this;
            }
            
            // Position related nodes and add to graph view.
            for (var i = 0; i < RelatedNodes.Length; i++)
            {
                var node = RelatedNodes[i];
                pos = position + new Vector2((i + 1) * (node.DefaultSize.x + 25), 0);
                node.SetPosition(new Rect(pos, node.DefaultSize));
                graphView.AddElement(node);
            }

            return this;
        }
    }
}
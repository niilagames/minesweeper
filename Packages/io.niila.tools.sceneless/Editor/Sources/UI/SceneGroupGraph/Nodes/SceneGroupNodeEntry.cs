﻿
namespace Editor.Sources.UI.SceneGroupGraph.Nodes
{
    public class SceneGroupNodeEntry : INodeCreatorPopupEntry
    {
        public string NodeType => "SCENE_GROUP_NODE_ENTRY";

        public string Path => "Scene group node";

        public int Order => -10;

        public NodeCreationResult CreateNode(string title = null)
        {
            var node = new SceneGroupNode("Scene Group Node");
            return new NodeCreationResult(node);
        }
    }
}
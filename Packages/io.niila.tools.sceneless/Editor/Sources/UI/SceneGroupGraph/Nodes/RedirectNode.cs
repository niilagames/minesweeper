﻿using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.Sources.UI.SceneGroupGraph.Nodes
{
    /// <summary>
    /// Redirects the flow to a redirect receiver node.
    /// </summary>
    public class RedirectNode : BaseNode
    {
        public string RedirectName => _redirectName;

        public RedirectReceiverNode ReceiverNode => _receiverNode;

        public override Vector2 DefaultSize => new Vector2(100, 150);

        private RedirectReceiverNode _receiverNode;

        #region Data

        private string _redirectName;
        
        #endregion
        
        #region ElementRefs

        private TextField _redirectNameField;

        #endregion

        public RedirectNode(RedirectReceiverNode receiverNode, string title) : base(title)
        {
            _receiverNode = receiverNode;
            _receiverNode.SetRedirectNode(this);
            _redirectName = title;
        }

        public override void SetTitle(string title)
        {
            base.SetTitle($"Redirecter ({title})");
            _redirectName = title;
        }

        public void SetRedirectName(string redirectName)
        {
            SetTitle(redirectName);
            _redirectNameField.SetValueWithoutNotify(redirectName);
            _redirectName = redirectName;
        }

        protected override void InitPorts()
        {
            // Add input port.
            var inputPort = InstantiatePort(Orientation.Horizontal, Direction.Input, Port.Capacity.Multi, null);
            inputPort.portName = "Redirect flow";
            inputContainer.Add(inputPort);
        }

        protected override void InitProperties()
        {
            // Add redirect name field.
            _redirectNameField = new TextField
                {tooltip = "Name of the redirect. This is to keep track, if more exist."};
            _redirectNameField.RegisterCallback<ChangeEvent<string>>(evt =>
            {
                SetRedirectName(evt.newValue);
                _receiverNode.SetRedirectName(evt.newValue);
            });
            extensionContainer.Add(_redirectNameField);
        }
    }
}
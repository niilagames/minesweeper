﻿using System;
using Niila.Tools.Sceneless;
using Niila.Tools.Sceneless.Editor;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.Sources.UI.SceneGroupGraph.Nodes
{
    public class SceneGroupNode : BaseNode
    {
        public SceneGroup SceneGroup
        {
            get => _sceneGroup;
            set
            {
                _sceneGroup = value;
                _sceneGroupSelector.value = value;
                OnSceneGroupSet();
            }
        }
        
        public override Vector2 DefaultSize => new Vector2(100, 150);

        #region Properties

        private SceneGroup _sceneGroup;

        #endregion

        #region ElementRefs

        private ObjectField _sceneGroupSelector;
        private TextField _sceneGroupNameField;

        #endregion

        public SceneGroupNode(string title) : base(title) { }

        protected override void InitPorts()
        {
            // Add input port.
            var inputPort = InstantiatePort(Orientation.Horizontal, Direction.Input, Port.Capacity.Multi, null);
            inputPort.portName = "Previous";
            inputContainer.Add(inputPort);

            // Add output port.
            var outputPort = InstantiatePort(Orientation.Horizontal, Direction.Output, Port.Capacity.Single, null);
            outputPort.portName = "Next";
            outputContainer.Add(outputPort);
        }

        protected override void InitProperties()
        {
            // Adding scene group editor elements.
            _sceneGroupSelector = new ObjectField
            {
                allowSceneObjects = false,
                objectType = typeof(SceneGroup),
            };

            _sceneGroupSelector.RegisterCallback<ChangeEvent<UnityEngine.Object>>(evt =>
            {
                _sceneGroup = evt.newValue as SceneGroup;
                OnSceneGroupSet();
            });
            extensionContainer.Add(_sceneGroupSelector);

            // Add scene group name field.
            _sceneGroupNameField = new TextField();
            _sceneGroupNameField.RegisterCallback<ChangeEvent<string>>(evt =>
            {
                if (_sceneGroup != null)
                {
                    _sceneGroup.SetName(evt.newValue);
                    _sceneGroup.name = evt.newValue;
                    this.title = evt.newValue;
                    // Make sure Unity knows that this scene group needs to be saved.
                    EditorUtility.SetDirty(_sceneGroup);
                }
            });
            if (SceneGroup != null)
            {
                extensionContainer.Add(_sceneGroupNameField);
                _sceneGroupNameField.value = SceneGroup.Name;
            }
        }
        
        private void OnSceneGroupSet()
        {
            title = _sceneGroup ? _sceneGroup.Name : "No Scene Group selected";
            if (_sceneGroup != null)
            {
                _sceneGroupNameField.value = _sceneGroup.Name;
                if (!extensionContainer.Contains(_sceneGroupNameField))
                {
                    extensionContainer.Add(_sceneGroupNameField);
                }
            }
            else if (extensionContainer.Contains(_sceneGroupNameField))
            {
                extensionContainer.Remove(_sceneGroupNameField);
            }
        }
    }
}
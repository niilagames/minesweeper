﻿namespace Editor.Sources.UI.SceneGroupGraph.Nodes
{
    public class RedirectNodeEntry : INodeCreatorPopupEntry
    {
        public string NodeType => "REDIRECT_NODE_ENTRY";

        public string Path => "Navigation/Redirect node";

        public int Order => 10;

        private static int _redirectNodesCreated = 0;
        
        public NodeCreationResult CreateNode(string title = null)
        {
            var index = ++_redirectNodesCreated;

            var nodeTitle = string.IsNullOrEmpty(title) ? index.ToString() : title;

            // Create the receiver node.
            var receiver = new RedirectReceiverNode(nodeTitle);

            // Create node.
            var redirector = new RedirectNode(receiver, nodeTitle);
            
            return new NodeCreationResult(redirector, receiver);
        }
    }
}
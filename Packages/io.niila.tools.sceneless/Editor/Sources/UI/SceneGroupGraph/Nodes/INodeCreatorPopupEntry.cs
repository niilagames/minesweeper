﻿using System.Collections.Generic;
using UnityEngine;

namespace Editor.Sources.UI.SceneGroupGraph.Nodes
{
    public interface INodeCreatorPopupEntry
    {
        string NodeType { get; }
        string Path { get; }
        int Order { get; }
        /// <summary>
        /// Creates the node (and related nodes) and returns them all.
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        NodeCreationResult CreateNode(string title = null);
    }
}
﻿using System;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace Editor.Sources.UI.SceneGroupGraph.Nodes
{
    public class SceneGroupEntryNode : BaseNode
    {
        public override Vector2 DefaultSize => new Vector2(100, 150);

        public SceneGroupEntryNode(string title) : base(title)
        {
            // Make it non-movalbe and non-deletable (~ means not in bitwise operations).
            #if UNITY_2019_4
            capabilities &= ~Capabilities.Movable & ~Capabilities.Deletable & ~Capabilities.Renamable;
            #elif UNITY_2020
            capabilities &= ~Capabilities.Movable & ~Capabilities.Deletable & ~Capabilities.Copiable;
            #endif
        }

        protected override void InitPorts()
        {
            // Add output port.
            var outputPort = InstantiatePort(Orientation.Horizontal, Direction.Output, Port.Capacity.Single, null);
            outputPort.portName = "Next";
            outputContainer.Add(outputPort);
        }

        protected override void InitProperties()
        {
            // No properties need to be initiated.
        }
    }
}
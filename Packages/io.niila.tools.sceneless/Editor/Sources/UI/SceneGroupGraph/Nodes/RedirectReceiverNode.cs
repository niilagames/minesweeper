﻿using System;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.Sources.UI.SceneGroupGraph.Nodes
{
    /// <summary>
    /// Receives the redirected flow from a redirect node.
    /// </summary>
    public class RedirectReceiverNode : BaseNode
    {
        
        public string RedirectName => _redirectName;
        
        public RedirectNode RedirectNode => _redirectNode;

        public override Vector2 DefaultSize => new Vector2(100, 150);

        private RedirectNode _redirectNode;

        #region Data

        private string _redirectName;

        #endregion
        
        #region ElementRefs

        private TextField _redirectNameField;

        #endregion

        public RedirectReceiverNode(string title) : base(title)
        {
            _redirectName = title;
        }

        public void SetRedirectNode(RedirectNode redirectNode)
        {
            _redirectNode = redirectNode;
        }

        public override void SetTitle(string title)
        {
            base.SetTitle($"Receiver ({title})");
            _redirectName = title;
        }

        public void SetRedirectName(string redirectName)
        {
            SetTitle(redirectName);
            _redirectNameField.SetValueWithoutNotify(redirectName);
            _redirectName = redirectName;
        }

        protected override void InitPorts()
        {
            // Add output port.
            var outputPort = InstantiatePort(Orientation.Horizontal, Direction.Output, Port.Capacity.Single, null);
            outputPort.portName = "Next";
            outputContainer.Add(outputPort);
        }

        protected override void InitProperties()
        {
            // Add redirect name field.
            _redirectNameField = new TextField
                {tooltip = "Name of the redirect. This is to keep track, if more exist."};
            _redirectNameField.RegisterCallback<ChangeEvent<string>>(evt =>
            {
                SetRedirectName(evt.newValue);
                _redirectNode.SetRedirectName(evt.newValue);
            });
            extensionContainer.Add(_redirectNameField);
        }
    }
}
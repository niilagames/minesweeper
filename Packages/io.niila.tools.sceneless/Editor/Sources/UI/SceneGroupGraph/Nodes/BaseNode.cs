﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace Editor.Sources.UI.SceneGroupGraph.Nodes
{
    public abstract class BaseNode : Node
    {
        public string GUID;

        public abstract Vector2 DefaultSize { get; }

        public BaseNode(string title)
        {
            GUID = Guid.NewGuid().ToString();
            SetTitle(title);

            // Add ports and properties.
            InitPorts();
            InitProperties();

            // Redraw the UI after adding port and properties.
            RefreshExpandedState();
            RefreshPorts();
        }

        public virtual void SetTitle(string title)
        {
            this.title = title;
        }

        public List<Edge> GetEdges()
        {
            var ports = inputContainer.Children().OfType<Port>().Concat(outputContainer.Children().OfType<Port>());
            var edges = ports.SelectMany(p => p.connections).Distinct().ToList();
            return edges;
        }
        
        protected abstract void InitPorts();

        protected abstract void InitProperties();
    }
}
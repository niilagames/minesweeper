﻿using System.Collections.Generic;
using System.Linq;
using Niila.Tools.EditorUiElements.Editor;
using Niila.Tools.EditorUiElements.Editor.Pages;
using Niila.Tools.Sceneless.Settings;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.Sceneless.Editor.Pages
{
    public class SettingsPage : PageElement
    {
        public override string PageId => "sceneless_settings_page";
        public override string PageName => "Settings";
        public override string PageDescription => "General settings for Sceneless.";

        public bool StartFromInitScene { get; private set; } = false;
        public int StartingSceneGroup { get; private set; } = -1;
        private int _startingSceneGroupIndex = -1;
        private ScenelessSetup _scenelessSetup;

        public override void OnEnabled()
        {
            // Load editor settings.
            StartFromInitScene = ScenelessEditorSettings.StartFromInitScene;
            StartingSceneGroup = ScenelessEditorSettings.StartingSceneGroup;

            // Load required assets.
            _scenelessSetup =
                ScenelessEditorUtils.GetAsset<ScenelessSetup>(ScenelessEditorUtils.ScenelessSetupAssetName);
        }

        public override void OnDisabled()
        {
            // Save editor settings.
            ScenelessEditorSettings.StartFromInitScene = StartFromInitScene;
            ScenelessEditorSettings.StartingSceneGroup = StartingSceneGroup;
        }

        public override void Draw()
        {
            DrawInitFromSceneSettings();
            DrawSaveLoadSettings();
        }

        public override void Update()
        {

        }

        private void DrawInitFromSceneSettings()
        {
            UiElements.BeginPaddedArea();

            UiElements.H2("Select play scene group");
            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(350));
            if (ScenelessEditorUtils.SceneSetup.SceneGroups.Count == 0)
            {
                UiElements.Text(
                    "At least a single scene group is necessary in order for this setting to be accessible.");
            }
            else
            {
                UiElements.Text(
                    "Select which scene group should be loaded, when the play button is hit.");
                var prevStartFromInitScene = StartFromInitScene;
                StartFromInitScene = EditorGUILayout.Toggle("Start from init scene",
                    StartFromInitScene);
                // Update asset, if setting changed.
                if (prevStartFromInitScene != StartFromInitScene)
                {
                    // Set starting scene group in setup, if wanting to start from init scene.
                    if (StartFromInitScene)
                    {
                        ScenelessEditorUtils.SceneSetup.StartupSceneGroup =
                            ScenelessEditorUtils.SceneSetup.SceneGroups.FirstOrDefault(sg =>
                                sg.GetInstanceID() == StartingSceneGroup);
                    }
                    else // Else set startup scene group to null.
                    {
                        ScenelessEditorUtils.SceneSetup.StartupSceneGroup = null;
                    }
                }

                // Only draw related settings, if going to start from init scene.
                if (StartFromInitScene)
                {
                    var options = new List<string>();
                    var optionValues = new List<int>();
                    for (var i = 0; i < ScenelessEditorUtils.SceneSetup.SceneGroups.Count; i++)
                    {
                        var sceneGroup = ScenelessEditorUtils.SceneSetup.SceneGroups[i];
                        options.Add(!string.IsNullOrWhiteSpace(sceneGroup.Name)
                            ? sceneGroup.Name
                            : $"Scene group {i + 1}: [No name]");
                        optionValues.Add(sceneGroup.GetInstanceID());
                        if (_startingSceneGroupIndex < 0 &&
                            StartingSceneGroup == sceneGroup.GetInstanceID())
                        {
                            _startingSceneGroupIndex = i;
                        }
                    }

                    if (_startingSceneGroupIndex < 0)
                    {
                        _startingSceneGroupIndex = 0;
                    }

                    _startingSceneGroupIndex = EditorGUILayout.Popup("Starting scene group",
                        _startingSceneGroupIndex, options.ToArray());
                    StartingSceneGroup = optionValues[_startingSceneGroupIndex];
                    ScenelessEditorUtils.SceneSetup.StartupSceneGroup =
                        ScenelessEditorUtils.SceneSetup.SceneGroups.FirstOrDefault(sg =>
                            sg.GetInstanceID() == StartingSceneGroup);
                }
            }

            EditorGUILayout.EndVertical();

            UiElements.EndPaddedArea();
        }

        private void DrawSaveLoadSettings()
        {
            EditorGUI.BeginChangeCheck();
            UiElements.BeginPaddedArea();

            UiElements.H2("Save/load settings");
            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(350));
            GUILayout.Space(5);
            EditorGUILayout.BeginHorizontal();
            if (UiElements.Button("Clear all player prefs"))
            {
                ClearAllPlayerPrefs();
            }

            if (UiElements.Button("Clear default save state"))
            {
                ClearDefaultSaveState();
            }

            EditorGUILayout.EndHorizontal();
            GUILayout.Space(5);
            UiElements.H3("Load on game start");
            UiElements.Text("If enabled, Sceneless will load the scene group state saved with the key indicated.");
            _scenelessSetup.LoadStateOnInit =
                EditorGUILayout.Toggle("Load on start", _scenelessSetup.LoadStateOnInit);
            if (_scenelessSetup.LoadStateOnInit)
            {
                _scenelessSetup.StateToLoadOnInitKey =
                    EditorGUILayout.TextField("State to load on start", _scenelessSetup.StateToLoadOnInitKey);
                if (UiElements.Button("Use default state"))
                {
                    GUI.FocusControl(null);
                    _scenelessSetup.StateToLoadOnInitKey = _scenelessSetup.DefaultSaveStateKey;
                }
            }

            UiElements.H3("Saved default state");
            _scenelessSetup.DefaultSaveStateKey =
                EditorGUILayout.TextField("Default state key", _scenelessSetup.DefaultSaveStateKey);
            if (PlayerPrefs.HasKey(_scenelessSetup.DefaultSaveStateKey))
            {
                UiElements.Text($"Saved default state:\n{PlayerPrefs.GetString(_scenelessSetup.DefaultSaveStateKey)}");
            }
            else
            {
                UiElements.Text("No save state found for default key...");
            }

            EditorGUILayout.EndVertical();

            UiElements.EndPaddedArea();
            if (EditorGUI.EndChangeCheck())
            {
                AssetDatabase.SaveAssets();
            }
        }

        private void ClearAllPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("All player prefs have been deleted!");
        }

        private void ClearDefaultSaveState()
        {
            PlayerPrefs.DeleteKey(_scenelessSetup.DefaultSaveStateKey);
            Debug.Log("Default save state has been deleted from player prefs!");
        }
    }
}
﻿
using Niila.Tools.EditorUiElements.Editor.Pages;
using Niila.Tools.Sceneless.Settings;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;
using UiElements = Niila.Tools.EditorUiElements.Editor.UiElements;

namespace Niila.Tools.Sceneless.Editor.Pages
{

    public class SceneGroupsPage : PageElement
    {
        public override string PageId => "sceneless_scene_groups_page";
        public override string PageName => "Scene groups";
        public override string PageDescription => "Setup the game's scene groups here.";

        private SceneGroupListElement _sceneGroupList;
        private NewSceneGroupElement _newSceneGroup;
        private SmartRenamerElement _smartRenamer;

        private bool _isEnabled;

        private bool _showSidebar = true;
        private readonly AnimFloat _sidebarAnimator = new AnimFloat(1);
        private int _sidebarWidthCurrent => (int)(_sidebarAnimator.value * (SidebarWidthOpen - SidebarWidthClosed) + SidebarWidthClosed);
        private const int SidebarWidthOpen = 300;
        private const int SidebarWidthClosed = 50;
        private Vector2 _sidebarScroll;

        public override void OnEnabled()
        {
            // Load editor settings.
            _showSidebar = ScenelessEditorSettings.SceneGroupsPageSidebarState;
            _sidebarAnimator.value = _showSidebar ? 1 : 0;

            // Init UI elements.
            _sceneGroupList = new SceneGroupListElement(ScenelessEditorUtils.SceneSetup);
            _sceneGroupList.OnEnable();
            _newSceneGroup = new NewSceneGroupElement(ScenelessEditorUtils.SceneSetup);
            _newSceneGroup.OnEnable();
            _smartRenamer = new SmartRenamerElement(ScenelessEditorUtils.SceneSetup);
            _smartRenamer.OnEnable();

            _isEnabled = true;
        }

        public override void OnDisabled()
        {
            ScenelessEditorSettings.SceneGroupsPageSidebarState = _showSidebar;
            _sceneGroupList?.OnDisable();
            DisableSidebarElements();

            _isEnabled = false;
        }

        public override void Draw()
        {
            
            UiElements.BeginColumns(true);
            UiElements.BeginColumn(0, true);

            UiElements.BeginPaddedArea();
            UiElements.H1(PageName);
            UiElements.Text(PageDescription);
            UiElements.Text("Below the scene groups are displayed. The number in the parenthesis after the title is the number of scene groups in total.\nWhen a scene group is open, the parenthesis after each of the scene reference lists displays the number of valid scenes and the total number of scenes in the list (including null references).");
            GUILayout.Space(10);
            UiElements.H2($"List of scene groups ({ScenelessEditorUtils.SceneSetup.SceneGroups.Count})");
            GUILayout.Space(15);
            _sceneGroupList.Draw();
            UiElements.EndPaddedArea();

            UiElements.EndColumn();
            UiElements.BeginColumn(_sidebarWidthCurrent, true, new[] {UiElements.Border.Left});

            DrawSidebar();

            UiElements.EndColumn();
            UiElements.EndColumns();
        }

        private void DrawSidebar()
        {
            UiElements.BeginRows(true);
            UiElements.BeginRow(25, true, new[] {UiElements.Border.Bottom});

            EditorGUILayout.BeginHorizontal();
            if (UiElements.Button(_showSidebar ? "Hide" : "Show"))
            {
                _showSidebar = !_showSidebar;
                _sidebarAnimator.target = _showSidebar ? 1 : 0;
                if (!_showSidebar)
                {
                    DisableSidebarElements();
                }
            }
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            UiElements.EndRow();

            // Only display tools, if the sidebar is open.
            if (_showSidebar)
            {
                UiElements.BeginRow(0, true);
                _sidebarScroll = EditorGUILayout.BeginScrollView(_sidebarScroll, false, false, GUIStyle.none, GUI.skin.verticalScrollbar, GUIStyle.none);
                UiElements.BeginRows(true);
                // New Scene Group Element.
                UiElements.BeginRow(0, true, new[] {UiElements.Border.Bottom});

                UiElements.BeginPaddedArea();
                _newSceneGroup.Draw();
                UiElements.EndPaddedArea();

                UiElements.EndRow();
                // Smart Renamer Element.
                UiElements.BeginRow(0, true, new[] { UiElements.Border.Bottom });

                UiElements.BeginPaddedArea();
                _smartRenamer.Draw();
                UiElements.EndPaddedArea();

                UiElements.EndRow();
                UiElements.EndRows();
                EditorGUILayout.EndScrollView();
                UiElements.EndRow();
            }

            UiElements.EndRows();
        }

        public override void Update()
        {
            if (!_isEnabled)
            {
                return;
            }

            _sceneGroupList.Update();
        }

        private void DisableSidebarElements()
        {
            _newSceneGroup?.OnDisable();
            _smartRenamer?.OnDisable();
        }
    }
}
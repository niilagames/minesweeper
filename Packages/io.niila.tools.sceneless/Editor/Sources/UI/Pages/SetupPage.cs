﻿using System;
using System.Collections;
using System.Collections.Generic;
using Niila.Tools.EditorUiElements.Editor;
using Niila.Tools.EditorUiElements.Editor.Pages;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.Sceneless.Editor
{

    public class SetupPage : PageElement
    {
        public override string PageId => "sceneless_setup_page";
        public override string PageName => "Setup";
        public override string PageDescription => "Setup Sceneless";

        private Action _onSetupSceneless;

        public SetupPage(Action onSetupSceneless)
        {
            _onSetupSceneless = onSetupSceneless;
        }

        public override void Draw()
        {
            UiElements.BeginPaddedArea();

            UiElements.H2("Setup");
            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(350));
            if (ScenelessEditorUtils.IsSetupComplete())
            {
                EditorGUILayout.LabelField("Setup is valid!",
                    new GUIStyle {fontStyle = FontStyle.Italic});
            }
            else
            {
                if (UiElements.Button("Setup sceneless"))
                {
                    _onSetupSceneless.Invoke();
                }

                UiElements.Text("This will create the settings asset and init scene and add it to build settings.");
                GUILayout.Space(10);
                UiElements.Text("If nothing happens, check the console. Some part of the setup might have failed.");
                UiElements.Text("One common failure is for the setup to not be able to add the created init scene to the build settings or enabling it there. Try doing so manually.");
            }
            EditorGUILayout.EndVertical();

            UiElements.EndPaddedArea();
        }

        public override void Update()
        {
        }
    }
}
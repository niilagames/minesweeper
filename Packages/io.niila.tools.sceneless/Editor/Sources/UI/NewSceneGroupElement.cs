﻿using System;
using System.Collections;
using System.Collections.Generic;
using Niila.Tools.EditorUiElements.Editor;
using Niila.Tools.Sceneless;
using Niila.Tools.Sceneless.Settings;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.Sceneless.Editor
{

    public class NewSceneGroupElement : UiElement
    {
        public ScenelessSetup SceneSetup { get; set; }

        private SceneAsset _templateMainScene, _templateUiScene, _templateOtherScene, _templateEnvironmentScene;

        public NewSceneGroupElement(ScenelessSetup sceneSetup)
        {
            SceneSetup = sceneSetup;
        }

        public override void OnEnable()
        {
            var savedTemplate = ScenelessEditorSettings.NewSceneGroupTemplate;
            _templateMainScene = savedTemplate.TemplateMainScene;
            _templateUiScene = savedTemplate.TemplateUiScene;
            _templateOtherScene = savedTemplate.TemplateOtherScene;
            _templateEnvironmentScene = savedTemplate.TemplateEnvironmentScene;
        }

        public override void OnDisable()
        {
            ScenelessEditorSettings.NewSceneGroupTemplate =
                new ScenelessEditorSettings.NewSceneGroupTemplateObj(_templateMainScene, _templateUiScene, _templateOtherScene, _templateEnvironmentScene);
        }

        public override void Draw()
        {
            if (SceneSetup == null)
            {
                return;
            }

            EditorGUILayout.BeginVertical();

            UiElements.H2("New scene group");
            UiElements.Text(
                "These scenes are the template for creating a new scene group. These scenes below will be added to a newly created scene group.");

            EditorGUILayout.Space();

            _templateMainScene =
                EditorGUILayout.ObjectField("Main", _templateMainScene, typeof(SceneAsset), false) as SceneAsset;
            _templateUiScene =
                EditorGUILayout.ObjectField("UI", _templateUiScene, typeof(SceneAsset), false) as SceneAsset;
            _templateEnvironmentScene =
                EditorGUILayout.ObjectField("Environment", _templateEnvironmentScene, typeof(SceneAsset), false) as
                    SceneAsset;
            _templateOtherScene =
                EditorGUILayout.ObjectField("Other", _templateOtherScene, typeof(SceneAsset), false) as SceneAsset;

            if (UiElements.Button(UiIcon.Add, "Create new scene group"))
            {
                ScenelessEditorUtils.EnsureFolderStructure(ScenelessEditorUtils.FullResourcePath);

                var newSceneGroup = ScriptableObject.CreateInstance<SceneGroup>();
                newSceneGroup.SetScenes(
                    ScenelessEditorUtils.ToScenePathList(_templateMainScene),
                    ScenelessEditorUtils.ToScenePathList(_templateUiScene),
                    ScenelessEditorUtils.ToScenePathList(_templateEnvironmentScene),
                    ScenelessEditorUtils.ToScenePathList(_templateOtherScene)
                );

                AssetDatabase.AddObjectToAsset(newSceneGroup, ScenelessEditorUtils.SceneSetup);
                EditorUtility.SetDirty(newSceneGroup);
                EditorUtility.SetDirty(ScenelessEditorUtils.SceneSetup);
                AssetDatabase.SaveAssets();

                ScenelessEditorUtils.SceneSetup.SceneGroups.Add(newSceneGroup);
            }

            EditorGUILayout.EndVertical();
        }

        public override void Update()
        {

        }
    }
}
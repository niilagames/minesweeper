﻿using Niila.Tools.EditorUiElements.Editor;
using UnityEditor;

namespace Niila.Tools.Sceneless.Editor
{
    public class SetupTransitionSceneElement : UiElement
    {
        public ScenelessSetup SceneSetup
        {
            get => _sceneSetup;
            set
            {
                _sceneSetup = value;
                if (value != null && value.TransitionScene != null)
                {
                    _transitionScene = ScenelessEditorUtils.ToSceneAsset(SceneSetup.TransitionScene.ScenePath);
                }
            }

        }

        private ScenelessSetup _sceneSetup;
        private SceneAsset _transitionScene;

        public SetupTransitionSceneElement(ScenelessSetup sceneSetup)
        {
            SceneSetup = sceneSetup;
        }

        public override void Draw()
        {
            if (SceneSetup == null)
            {
                return;
            }

            UiElements.H2("Transition scene");
            UiElements.Text(
                "If a transition scene is set, it will be used while transitioning between scene groups (for instance a loading screen).");

            EditorGUILayout.Space();

            var newTransitionScene =
                EditorGUILayout.ObjectField("Transition scene", _transitionScene, typeof(SceneAsset), false) as
                    SceneAsset;
            if (newTransitionScene != _transitionScene)
            {
                _transitionScene = newTransitionScene;

                // Set to scene reference or null.
                SceneSetup.TransitionScene = _transitionScene != null
                    ? new SceneReference
                    {
                        ScenePath = ScenelessEditorUtils.ToScenePath(_transitionScene),
                    }
                    : null;
            }
        }

        public override void Update()
        {

        }
    }
}
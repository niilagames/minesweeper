﻿using System.Collections.Generic;
using Niila.Tools.EditorUiElements.Editor;
using Niila.Tools.EditorUiElements.Editor.Pages;
using Niila.Tools.Sceneless.Editor.Pages;
using Niila.Tools.Sceneless.Settings;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.Sceneless.Editor
{

    public class ScenelessWindow : HeaderRowContentRowWindow
    {
        

        [MenuItem("Tools/Sceneless/Sceneless setup")]
        public static void ShowWindow()
        {
            ScenelessWindow window = GetWindow<ScenelessWindow>();
            window.minSize = new Vector2(700, 250);
            window.titleContent = new GUIContent("Sceneless");
        }

        private SceneAsset _initScene;

        protected override int HeaderRowHeight => 80;
        protected override bool UsePaddedAreaInHeader => true;
        protected override bool UsePaddedAreaInContentRow => false;

        private IPageElement _setupPage;

        /// <summary>
        /// The key used to save the which page was last opened on the current machine. Must be unique! Preferably use a prefix besides a name.
        /// </summary>
        protected string PageMenuLastPageSaveKey => "sceneless_page";

        protected int PageMenuWidth => 150;

        protected PagesAndMenuElement<IPageElement> PagesAndMenu;

        private List<IPageElement> GetPageList()
        {
            

            return new List<IPageElement>
            {
                new SettingsPage(),
                new SceneGroupsPage(),
            };
        }

        public void OnEnable()
        {
            // Create setup page.
            _setupPage = new SetupPage(SetupSceneless);
            _setupPage.OnEnabled();

            // Load/Create/Setup data.
            ScenelessEditorUtils.SetupScenelessEditorUtils();

            // Setup elements.
            _setupTransitionGroup = new SetupTransitionSceneElement(ScenelessEditorUtils.SceneSetup);


            if (PagesAndMenu == null)
            {
                PagesAndMenu = new PagesAndMenuElement<IPageElement>(PageMenuLastPageSaveKey, PageMenuWidth);
            }

            PagesAndMenu.SetPages(GetPageList());
            if (ScenelessEditorUtils.IsSetupComplete())
            {
                PagesAndMenu.OnEnabled();
            }
        }

        private void Update()
        {
            if (ScenelessEditorUtils.IsSetupComplete())
            {
                PagesAndMenu.Update();
                Repaint();
            }
        }

        public void OnDisable()
        {
            

            // Disable pages and menu.
            _setupPage.OnDisabled();
            PagesAndMenu.OnDisabled();
        }

        private static SettingsPage _settingsPage;
        private static SceneGroupsPage _sceneGroupsPage;

        private SetupTransitionSceneElement _setupTransitionGroup;

        private Vector2 _columnOneScroll;

        protected override void OnHeaderRow()
        {
            UiElements.H1("Sceneless");
            EditorGUILayout.LabelField("A seamless scene management tool.");
        }

        protected override void OnContentRow()
        {
            if (!ScenelessEditorUtils.IsSetupComplete())
            {
                _setupPage.Draw();
            }
            else
            {
                PagesAndMenu.Draw();
            }
        }

        private void SetupSceneless()
        {
            // Ensure that required folders exist.
            ScenelessEditorUtils.EnsureFolderStructure(ScenelessEditorUtils.FullResourcePath);

            // Create scene setup asset.
            ScenelessEditorUtils.CreateSMTSetupAsset();

            // Create the init scene.
            ScenelessEditorUtils.CreateInitScene();
            ScenelessEditorUtils.EnsureInitSceneFirstInBuildMenu();

            // Setting up sceneless settings.
            ScenelessEditorSettings.StartFromInitScene = true;

            // Call on enable for pages and menu, if sceneless was successfully set up.
            if (ScenelessEditorUtils.IsSetupComplete())
            {
                PagesAndMenu.OnEnabled();
            }
        }
    }
}

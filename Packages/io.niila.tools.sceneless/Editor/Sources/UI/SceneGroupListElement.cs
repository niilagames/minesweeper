﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Editor.Sources.UI.SceneGroupGraph;
using Niila.Tools.EditorUiElements.Editor;
using Niila.Tools.Sceneless;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Niila.Tools.Sceneless.Editor
{
    public class SceneGroupListElement : UiElement
    {
        public ScenelessSetup SceneSetup { get; set; }
        private Vector2 _sceneGroupScrollPos;
        private int _sceneGroupCount;
        private Dictionary<int, bool> _sceneGroupListFoldoutStates = new Dictionary<int, bool>();
        private Dictionary<int, string> _sceneGroupNames = new Dictionary<int, string>();

        private Debouncer _saveAssetsDebouncer = null;

        private SceneGroupDrawer _sceneGroupDrawer = new SceneGroupDrawer();

        private const int IconSize = 25;
        private readonly GUILayoutOption[] _iconLayoutOptions = {GUILayout.Width(IconSize), GUILayout.Height(IconSize)};

        public SceneGroupListElement(ScenelessSetup sceneSetup)
        {
            SceneSetup = sceneSetup;
            _saveAssetsDebouncer = new Debouncer(AssetDatabase.SaveAssets);
        }

        public override void OnEnable()
        {
            // Reset all selected scenes, so it doesn't create errors, if others didn't clean up.
            ScenelessEditorUtils.DeselectAllSceneGroups();
        }
        
        public override void OnDisable()
        {
            // Reset all selected scenes, so it doesn't create errors at other stages.
            ScenelessEditorUtils.DeselectAllSceneGroups();
        }

        public override void Draw()
        {
            if (SceneSetup == null)
            {
                return;
            }

            // Select/deselect all scene groups.
            if (!ScenelessEditorUtils.AreAllSceneGroupsSelected)
            {
                if (GUILayout.Button("Select All", GUILayout.Width(120)))
                {
                    ScenelessEditorUtils.SelectAllSceneGroups();
                }
            }
            else
            {
                if (GUILayout.Button("Deselect All", GUILayout.Width(120)))
                {
                    ScenelessEditorUtils.DeselectAllSceneGroups();
                }
            }
            GUILayout.Space(10);

            if (SceneSetup.SceneGroups.Count != _sceneGroupCount)
            {
                _sceneGroupCount = SceneSetup.SceneGroups.Count;
                _sceneGroupListFoldoutStates.Clear();
                _sceneGroupNames.Clear();
                foreach (var sceneGroup in SceneSetup.SceneGroups)
                {
                    _sceneGroupListFoldoutStates.Add(sceneGroup.GetInstanceID(), false);
                    _sceneGroupNames.Add(sceneGroup.GetInstanceID(), sceneGroup.Name);
                }
            }

            _sceneGroupScrollPos = EditorGUILayout.BeginScrollView(_sceneGroupScrollPos);
            GUILayout.Space(5);
            ListAction listAction = ListAction.None;
            int sceneGroupIndexForAction = -1;
            for (var i = 0; i < SceneSetup.SceneGroups.Count; i++)
            {
                var sceneGroup = SceneSetup.SceneGroups[i];
                var sceneGroupIndex = i;

                UiElements.BeginColumns(true);
                UiElements.BeginColumn(20, true);
                var isSelected = EditorGUILayout.Toggle(ScenelessEditorUtils.SelectedSceneGroups.Contains(sceneGroup));
                if (isSelected)
                {
                    ScenelessEditorUtils.SelectedSceneGroups.Add(sceneGroup);
                }
                else
                {
                    ScenelessEditorUtils.RemoveSelectedSceneGroup(sceneGroup);
                }

                UiElements.EndColumn();
                UiElements.BeginColumn(0, true, new[] {UiElements.Border.Left, UiElements.Border.Right});

                _sceneGroupListFoldoutStates[sceneGroup.GetInstanceID()] = EditorGUILayout.Foldout(
                    _sceneGroupListFoldoutStates[sceneGroup.GetInstanceID()],
                    $"Scene group {i + 1}: {sceneGroup.Name}", true, UiStyles.FoldoutH3);
                if (_sceneGroupListFoldoutStates[sceneGroup.GetInstanceID()])
                {
                    GUILayout.Space(5);

                    _sceneGroupDrawer.Draw(sceneGroup);

                    // Add margin to all but the last one.
                    if (i < SceneSetup.SceneGroups.Count - 1)
                    {
                        GUILayout.Space(30);
                    }
                }

                UiElements.EndColumn();
                UiElements.BeginColumn(150, true);
                UiElements.BeginRows(true);
                UiElements.BeginRow(30, true);

                // Move up and move to top.
                if (sceneGroupIndex > 0)
                {
                    if (UiElements.ButtonTooltip(UiIcon.MoveToTop, "Move to top", _iconLayoutOptions))
                    {
                        listAction = ListAction.MoveToTop;
                        sceneGroupIndexForAction = sceneGroupIndex;
                    }

                    if (UiElements.ButtonTooltip(UiIcon.MoveUp, "Move up", _iconLayoutOptions))
                    {
                        listAction = ListAction.MoveUp;
                        sceneGroupIndexForAction = sceneGroupIndex;
                    }
                }
                else
                {
                    var iconPadding = 6;
                    GUILayout.Space(IconSize * 2 + 2 * iconPadding);
                }

                // Delete.
                if (UiElements.ButtonTooltip(UiIcon.Trash, "Remove scene group", _iconLayoutOptions))
                {
                    if (EditorUtility.DisplayDialog("Are you sure?",
                        $"Are you sure you want to remove scene group {sceneGroup.Name}? Action cannot be undone.",
                        "Yes", "Cancel"))
                    {
                        listAction = ListAction.Remove;
                        sceneGroupIndexForAction = sceneGroupIndex;
                    }
                }

                // Move down & move to bottom.
                if (sceneGroupIndex < SceneSetup.SceneGroups.Count - 1)
                {
                    if (UiElements.ButtonTooltip(UiIcon.MoveDown, "Move down", _iconLayoutOptions)) {
                        listAction = ListAction.MoveDown;
                        sceneGroupIndexForAction = sceneGroupIndex;
                    }
                    
                    if (UiElements.ButtonTooltip(UiIcon.MoveToBottom, "Move to bottom", _iconLayoutOptions))
                    {
                        listAction = ListAction.MoveToBottom;
                        sceneGroupIndexForAction = sceneGroupIndex;
                    }
                }
                else
                {
                    // Add some padding until the next button,
                    // if the move down and move to bottom buttons are not present.
                    var iconPadding = 6;
                    GUILayout.Space(IconSize * 2 + 2 * iconPadding);
                }

                // Duplicate.
                if (UiElements.ButtonTooltip(UiIcon.Copy, "Duplicate scene group", _iconLayoutOptions))
                {
                    listAction = ListAction.Duplicate;
                    sceneGroupIndexForAction = sceneGroupIndex;
                }
                    

                UiElements.EndRow();
                UiElements.EndRows();
                UiElements.EndColumn();
                UiElements.EndColumns();

                GUILayout.Space(10);

                if (_sceneGroupNames[sceneGroup.GetInstanceID()] != sceneGroup.Name)
                {
                    _sceneGroupNames[sceneGroup.GetInstanceID()] = sceneGroup.Name;
                    sceneGroup.name = sceneGroup.Name;
                    _saveAssetsDebouncer.Invoke();
                }

                // Break out of the loop if a list action was selected.
                if (listAction != ListAction.None)
                {
                    break;
                }
            }

            GUILayout.FlexibleSpace();

            // If a list action is selected, do the action.
            if (listAction != ListAction.None)
            {
                switch (listAction)
                {
                    case ListAction.Remove:
                        // Find scene group.
                        var sceneGroupAsset = SceneSetup.SceneGroups[sceneGroupIndexForAction];
                        // Remove scene group from sceneless data afterwards.
                        SceneSetup.SceneGroups.RemoveAt(sceneGroupIndexForAction);
                        Object.DestroyImmediate(sceneGroupAsset, true);
                        AssetDatabase.SaveAssets();
                        break;
                    case ListAction.MoveUp:
                    {
                        var tmp = SceneSetup.SceneGroups[sceneGroupIndexForAction];
                        SceneSetup.SceneGroups[sceneGroupIndexForAction] =
                            SceneSetup.SceneGroups[sceneGroupIndexForAction - 1];
                        SceneSetup.SceneGroups[sceneGroupIndexForAction - 1] = tmp;
                    }
                        break;
                    case ListAction.MoveDown:
                    {
                        var tmp = SceneSetup.SceneGroups[sceneGroupIndexForAction];
                        SceneSetup.SceneGroups[sceneGroupIndexForAction] =
                            SceneSetup.SceneGroups[sceneGroupIndexForAction + 1];
                        SceneSetup.SceneGroups[sceneGroupIndexForAction + 1] = tmp;
                    }
                        break;
                    case ListAction.MoveToTop:
                    {
                        if (sceneGroupIndexForAction == 0)
                        {
                            break;
                        }

                        var tmp = SceneSetup.SceneGroups[sceneGroupIndexForAction];
                        SceneSetup.SceneGroups.RemoveAt(sceneGroupIndexForAction);
                        SceneSetup.SceneGroups.Insert(0, tmp);
                    }
                        break;
                    case ListAction.MoveToBottom:
                    {
                        if (sceneGroupIndexForAction == SceneSetup.SceneGroups.Count - 1)
                        {
                            break;
                        }

                        var tmp = SceneSetup.SceneGroups[sceneGroupIndexForAction];
                        SceneSetup.SceneGroups.RemoveAt(sceneGroupIndexForAction);
                        SceneSetup.SceneGroups.Add(tmp);
                    }
                        break;
                    case ListAction.Duplicate:
                    {
                        // Create duplicate.
                        var original = SceneSetup.SceneGroups[sceneGroupIndexForAction];
                        var duplicate = original.CreateDuplicate($"{original.Name} (Duplicate)");
                        
                        // Save duplicate.
                        AssetDatabase.AddObjectToAsset(duplicate, ScenelessEditorUtils.SceneSetup);
                        EditorUtility.SetDirty(duplicate);
                        EditorUtility.SetDirty(ScenelessEditorUtils.SceneSetup);
                        AssetDatabase.SaveAssets();

                        ScenelessEditorUtils.SceneSetup.SceneGroups.Add(duplicate);
                        
                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                EditorUtility.SetDirty(SceneSetup);
            }

            EditorGUILayout.EndScrollView();
        }

        public override void Update()
        {
            _saveAssetsDebouncer.Update();
        }

        private enum ListAction
        {
            None,
            Remove,
            MoveUp,
            MoveDown,
            MoveToTop,
            MoveToBottom,
            Duplicate,
        }
    }
}
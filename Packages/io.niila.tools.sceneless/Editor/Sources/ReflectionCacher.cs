﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Niila.Tools.Sceneless.Editor.Sources
{
    /// <summary>
    /// Class can be used to get or set values on objects using reflection.
    /// It will cache fields which have been used before.
    /// </summary>
    public class ReflectionCacher
    {
        private static Dictionary<Type, Dictionary<string, FieldInfo>> _objFieldCache =
            new Dictionary<Type, Dictionary<string, FieldInfo>>();
        
        /// <summary>
        /// Set the field value of an object using reflection.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="obj"></param>
        /// <param name="value"></param>
        /// <typeparam name="T"></typeparam>
        public void SetField<T>(string fieldName, object obj, T value)
        {
            var field = GetCachedField(fieldName, obj.GetType());
            field.SetValue(obj, value);
        }

        /// <summary>
        /// Get the field value of an object using reflection.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="obj"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetField<T>(string fieldName, object obj)
        {
            var field = GetCachedField(fieldName, obj.GetType());
            return (T) field.GetValue(obj);
        }
        
        private FieldInfo GetCachedField(string fieldName, Type objType)
        {
            // Check if we have any fields for the given object type.
            if (_objFieldCache.ContainsKey(objType))
            {
                // Check if we have the specific field for the given object type.
                if (_objFieldCache[objType].ContainsKey(fieldName))
                {
                    return _objFieldCache[objType][fieldName];
                }

                // Find field, cache it and return it.
                var field = objType.GetField(fieldName,
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                _objFieldCache[objType].Add(fieldName, field);
                return field;
            }

            // Add object type, find field, cache it and return it.
            _objFieldCache.Add(objType, new Dictionary<string, FieldInfo>());
            var fieldInfo = objType.GetField(fieldName,
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            _objFieldCache[objType].Add(fieldName, fieldInfo);
            return fieldInfo;
        }
    }
}
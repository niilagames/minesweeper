﻿using System.Runtime.CompilerServices;
using UnityEditor.UIElements;

[assembly: InternalsVisibleTo("Niila.Tools.Sceneless.Editor")]
[assembly: InternalsVisibleTo("Niila.Tools.Sceneless.Editor.Tests")]
#if UNITY_2018_3_OR_NEWER
[assembly: UxmlNamespacePrefix("Niila.Tools.Sceneless.Editor", "sceneless")]
#endif
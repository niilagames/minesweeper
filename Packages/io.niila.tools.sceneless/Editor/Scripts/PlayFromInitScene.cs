﻿using System.Linq;
using Niila.Tools.Sceneless;
using Niila.Tools.Sceneless.Settings;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class PlayFromInitScene
{
    private const string PlayFromInitMenuStr = "Tools/Sceneless/Start from init scene &p";

    // The menu won't be gray out, we use this validate method for update check state
    [MenuItem(PlayFromInitMenuStr, true)]
    static bool PlayFromInitSceneCheckMenuValidate()
    {
        Menu.SetChecked(PlayFromInitMenuStr, ScenelessEditorSettings.StartFromInitScene);
        return true;
    }

    // This method is called before any Awake. It's the perfect callback for this feature
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void LoadInitSceneAtGameBegins()
    {
        // Don't do anything, if setting is not checked.
        if (!ScenelessEditorSettings.StartFromInitScene)
        {
            return;
        }

        // If there are no scene groups created, also don't do anything.
        var sceneSetupPath = "Sceneless/ScenelessSetup";
        var sceneSetup = Resources.Load<ScenelessSetup>(sceneSetupPath);

        if (!sceneSetup.SceneGroups.Any())
        {
            return;
        }

        // Don't do anything, if there are no scenes in build settings.
        if (EditorBuildSettings.scenes.Length == 0)
        {
            Debug.LogWarning("The scene build list is empty. Can't play from init scene.");
            return;
        }

        foreach (GameObject obj in Object.FindObjectsOfType<GameObject>())
        {
            obj.SetActive(false);
        }

        // Load the scene.
        SceneManager.LoadScene(0);
    }

    static void ShowNotifyOrLog(string msg)
    {
        if (Resources.FindObjectsOfTypeAll<SceneView>().Length > 0)
            EditorWindow.GetWindow<SceneView>().ShowNotification(new GUIContent(msg));
        else
            Debug.Log(msg); // When there's no scene view opened, we just print a log
    }
}
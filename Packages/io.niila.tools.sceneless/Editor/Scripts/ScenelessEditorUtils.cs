﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Niila.Tools.Sceneless.Editor.Services;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Niila.Tools.Sceneless.Editor
{

    public static class ScenelessEditorUtils
    {

        public const string AssetPath = "Assets/NiilaToolSettings/";
        public const string FullResourcePath = AssetPath + "Resources/Sceneless/";
        public const string ScenelessSetupAssetName = "ScenelessSetup";
        public const string MainLayoutAssetName = "ScenelessMainLayout";

        public static ScenelessSetup SceneSetup { get; private set; }
        private static string _initScenePath;

        #region SelectedSceneGroupsProperties

        public static HashSet<SceneGroup> SelectedSceneGroups { get; set; } = new HashSet<SceneGroup>();
        public static bool AreAllSceneGroupsSelected => SelectedSceneGroups.Count == SceneSetup.SceneGroups.Count;

        #endregion
        
        public static void SetupScenelessEditorUtils()
        {
            if (SceneSetup == null)
            {
                var sceneSetupPath = FullResourcePath + $"{ScenelessSetupAssetName}.asset";
                SceneSetup = AssetDatabase.LoadAssetAtPath<ScenelessSetup>(sceneSetupPath);
            }

            if (string.IsNullOrWhiteSpace(_initScenePath))
            {
                _initScenePath = AssetPath + "Sceneless_InitSceneManagement.unity";
            }
        }

        /// <summary>
        /// Returns true, if Sceneless has been setup and is ready to use.
        /// </summary>
        /// <returns></returns>
        public static bool IsSetupComplete()
        {
            // Check if ScenelessSetup asset has been loaded (if not, it probably wasn't created).
            if (SceneSetup == null)
            {
                return false;
            }

            // Check if init scene has been created.
            if (!IsInitSceneCreated())
            {
                return false;
            }

            // Check if we have setup init scene as first in build settings.
            if (!BuildSettingsService.IsSceneFirstInBuildSettings(_initScenePath))
            {
                return false;
            }

            return true;
        }

        public static void EnsureInitSceneFirstInBuildMenu()
        {
            BuildSettingsService.EnsureSceneIsFirstInBuildSettings(_initScenePath);
        }

        public static bool IsInitSceneCreated()
        {
            // Check if init scene has already been created.
            var initSceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(_initScenePath);
            return initSceneAsset != null;
        }

        #region SelectedSceneGroupsMethods

        public static void AddSelectedSceneGroup(SceneGroup sceneGroup)
        {
            if (!SelectedSceneGroups.Contains(sceneGroup))
            {
                SelectedSceneGroups.Add(sceneGroup);
            }
        }

        public static void RemoveSelectedSceneGroup(SceneGroup sceneGroup)
        {
            if (SelectedSceneGroups.Contains(sceneGroup))
            {
                SelectedSceneGroups.Remove(sceneGroup);
            }
        }

        public static void ToggleSelectedSceneGroup(SceneGroup sceneGroup)
        {
            if (SelectedSceneGroups.Contains(sceneGroup))
            {
                SelectedSceneGroups.Remove(sceneGroup);
            }
            else
            {
                SelectedSceneGroups.Add(sceneGroup);
            }
        }

        public static void SelectAllSceneGroups()
        {
            foreach (var sceneGroup in SceneSetup.SceneGroups)
            {
                AddSelectedSceneGroup(sceneGroup);
            }
        }

        public static void DeselectAllSceneGroups()
        {
            SelectedSceneGroups.Clear();
        }
        
        #endregion
        
        #region ProjectAssetMethods

        public static string GetAssetPath(string assetName)
        {
            var assetPath = FullResourcePath + assetName + ".asset";
            return assetPath;
        }

        public static T GetAsset<T>(string assetName) where T : ScriptableObject
        {
            var assetPath = GetAssetPath(assetName);
            // Load asset.
            var asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
            return asset;
        }

        /// <summary>
        /// Creates the asset of the given type/name, if it doesn't exist.
        /// </summary>
        public static T GetOrCreateAsset<T>(string assetName) where T : ScriptableObject
        {
            var assetPath = GetAssetPath(assetName);

            // Load or create asset.
            var asset = GetAsset<T>(assetName);
            if (asset == null)
            {
                EnsureFolderStructure(FullResourcePath);
                asset = ScriptableObject.CreateInstance<T>();
                AssetDatabase.CreateAsset(asset, assetPath);
                AssetDatabase.SaveAssets();
            }

            return asset;
        }

        public static bool DeleteAsset(string assetName)
        {
            var assetPath = GetAssetPath(assetName);
            return AssetDatabase.DeleteAsset(assetPath);
        }

        /// <summary>
        /// Creates the ScenelessSetup asset, if it doesn't exist.
        /// </summary>
        public static void CreateSMTSetupAsset()
        {
            var sceneSetupPath = FullResourcePath + $"{ScenelessSetupAssetName}.asset";
            SceneSetup = AssetDatabase.LoadAssetAtPath<ScenelessSetup>(sceneSetupPath);
            if (SceneSetup == null)
            {
                EnsureFolderStructure(FullResourcePath);
                SceneSetup = ScriptableObject.CreateInstance<ScenelessSetup>();
                AssetDatabase.CreateAsset(SceneSetup, sceneSetupPath);
                AssetDatabase.SaveAssets();
            }
        }

        public static void CreateInitScene()
        {
            // Check if init scene has already been created.
            if (IsInitSceneCreated())
            {
                return;
            }

            // Create new scene, add scene manager prefab, save and then close again.
            var initScene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Additive);
            initScene.name = "Sceneless_InitSceneManagement";
            var activeScene = EditorSceneManager.GetActiveScene();
            EditorSceneManager.SetActiveScene(initScene);
            var smToolObj = new GameObject("Sceneless_SceneManagementTool");
            var smTool = smToolObj.AddComponent<ScenelessManager>();
            smTool.SetInput(true);
            EditorSceneManager.SaveScene(initScene, _initScenePath);
            EditorSceneManager.SetActiveScene(activeScene);
            EditorSceneManager.CloseScene(initScene, true);
        }

        #endregion

        public static SceneAsset ToSceneAsset(string scenePath)
        {
            return AssetDatabase.LoadAssetAtPath<SceneAsset>(scenePath);
        }

        public static string ToScenePath(SceneAsset sceneAsset)
        {
            return AssetDatabase.GetAssetPath(sceneAsset);
        }

        public static IEnumerable<string> ToScenePathList(params SceneAsset[] sceneAssets)
        {
            return sceneAssets.Where(sa => sa != null).Select(AssetDatabase.GetAssetPath);
        }

        public static void EnsureFolderStructure(string path)
        {
            // Remove trailing slashes.
            path = path.TrimEnd('/', ' ');

            // Get the absolute path to the folder to check if it exists.
            // Note: AssetDatabase.IsValidFolder doesn't seem to work properly.
            var relativePath = PathWithoutAssetsPrefix(path);
            var absolutePath = $"{Application.dataPath}/{relativePath}";
            if (Directory.Exists(absolutePath))
            {
                return;
            }

            var pathParts = path.Split('/');

            var parentFolder = pathParts[0];
            for (int i = 1; i < pathParts.Length; i++)
            {
                var newFolder = pathParts[i];
                AssetDatabase.CreateFolder(parentFolder, newFolder);
                parentFolder += $"/{newFolder}";
            }

            AssetDatabase.SaveAssets();
        }

        /// <summary>
        /// Removes the prefixing "Assets/" if it is there.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static string PathWithoutAssetsPrefix(string path)
        {
            if (path.StartsWith("Assets/") || path.StartsWith("assets/"))
            {
                return path.Remove(0, "Assets/".Length);
            }

            return path;
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using Niila.Tools.Sceneless.Editor.Sources;
using UnityEngine;

namespace Niila.Tools.Sceneless.Editor
{
    public static class SceneGroupExtensions
    {
        private static ReflectionCacher _fieldCache = new ReflectionCacher();

        private const string FIELD_NAME = "_name";
        private const string FIELD_DESC = "_description";
        private const string FIELD_MAIN_SCENES = "_mainScenes";
        private const string FIELD_UI_SCENES = "_uiScenes";
        private const string FIELD_ENV_SCENES = "_environmentScenes";
        private const string FIELD_OTHER_SCENES = "_otherScenes";
        private const string FIELD_ACTIVE_SCENE = "_activeScene";
        private const string FIELD_MAIN_CAMERA_SCENE = "_mainCameraScene";

        public static SceneGroup CreateDuplicate(this SceneGroup sceneGroup, string sceneGroupName = null)
        {
            var newSceneGroup = ScriptableObject.CreateInstance<SceneGroup>();
            // Set name and description.
            var newName = !string.IsNullOrWhiteSpace(sceneGroupName) ? sceneGroupName : sceneGroup.Name;
            newSceneGroup.name = newName;
            _fieldCache.SetField(FIELD_NAME, newSceneGroup, newName);
            _fieldCache.SetField(FIELD_DESC, newSceneGroup, sceneGroup.Description);
            // Set scenes.
            newSceneGroup.SetScenes(
                _fieldCache.GetField<List<SceneReference>>(FIELD_MAIN_SCENES, sceneGroup).Select(sr => sr.ScenePath),
                _fieldCache.GetField<List<SceneReference>>(FIELD_UI_SCENES, sceneGroup).Select(sr => sr.ScenePath),
            _fieldCache.GetField<List<SceneReference>>(FIELD_ENV_SCENES, sceneGroup).Select(sr => sr.ScenePath),
            _fieldCache.GetField<List<SceneReference>>(FIELD_OTHER_SCENES, sceneGroup).Select(sr => sr.ScenePath)
                );
            // Set active scene and main camera scene.
            _fieldCache.SetField(FIELD_ACTIVE_SCENE, newSceneGroup, sceneGroup.ActiveScene);
            _fieldCache.SetField(FIELD_MAIN_CAMERA_SCENE, newSceneGroup, sceneGroup.MainCameraScene);
            
            return newSceneGroup;
        }

        public static void SetName(this SceneGroup sceneGroup, string name)
        {
            _fieldCache.SetField(FIELD_NAME, sceneGroup, name);
        }
    }
}
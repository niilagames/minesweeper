﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Niila.Tools.Sceneless.Editor
{
    [InitializeOnLoad]
    public class OnUnityStartup
    {
        /// <summary>
        /// Runs as Unity Starts up.
        /// </summary>
        static OnUnityStartup()
        {
            CleanUpSceneGroups();
        }

        /// <summary>
        /// Removes old scene group assets from the main asset, when they are not linked to the main asset anymore.
        /// This could occur previously when deleting scene groups from the Scene Group page in the Sceneless editor window.
        /// </summary>
        private static void CleanUpSceneGroups()
        {
            var sceneSetup = ScenelessEditorUtils.SceneSetup;

            if (sceneSetup == null)
            {
                ScenelessEditorUtils.SetupScenelessEditorUtils();
                sceneSetup = ScenelessEditorUtils.SceneSetup;
                if (sceneSetup == null)
                {
                    // Don't clean up before there is anything to clean up.
                    return;
                }
            }

            // Find all scene group assets, which are subassets of the scene setup asset.
            var sceneSetupAssetPath = AssetDatabase.GetAssetPath(sceneSetup);
            var assetsAtPath = AssetDatabase.LoadAllAssetsAtPath(sceneSetupAssetPath);
            var sceneGroupsAtPath = assetsAtPath.Where(AssetDatabase.IsSubAsset).OfType<SceneGroup>().ToArray();

            // For each scene group asset, check if it is still linked in the main asset and destroy if not.
            int destroyedSceneGroups = 0;
            for (int i = sceneGroupsAtPath.Length - 1; i >= 0; i--)
            {
                var sceneGroup = sceneGroupsAtPath[i];
                if (!sceneSetup.SceneGroups.Contains(sceneGroup))
                {
                    Object.DestroyImmediate(sceneGroup, true);
                    destroyedSceneGroups++;
                }
            }

            // Save changes (if any).
            if (destroyedSceneGroups > 0)
            {
                AssetDatabase.SaveAssets();
                Debug.Log($"Cleaned up unlinked scene groups!\nRemoved {destroyedSceneGroups} old unlinked scene group assets from the main Sceneless asset.");
            }
        }

    }
}
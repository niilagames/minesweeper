# Project README

A scene manager for handling loading/unloading of scenes seamlessly.
It furthermore includes a way of handling a transition scene, which can be displayed between scene loads.

Find documentation under the documentation folder in the package.

## Dependencies

- Editor UI Elements (io.niila.tools.editoruielements) - v1.4.0
﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Niila.Tools.Sceneless.Settings
{

    public static class ScenelessEditorSettings
    {
        #if UNITY_EDITOR        
        
        public const string StartFromInitSceneKey = "Sceneless_StartFromInitSceneKey";
        public const string StartingSceneGroupKey = "Sceneless_StartingSceneGroupKey";
        public const string SceneGroupsPageSidebarStateKey = "Sceneless_SceneGroupsPageSidebarStateKey";
        public const string LayoutPageSidebarStateKey = "Sceneless_LayoutPageSidebarStateKey";
        public const string NewSceneGroupTemplateKey = "Sceneless_NewSceneGroupTemplateKey";
        public const string SmartRenamerPatternKey = "Sceneless_SmartRenamerPatternKey";

        public static bool StartFromInitScene
        {
            get => EditorPrefs.HasKey(StartFromInitSceneKey) && EditorPrefs.GetBool(StartFromInitSceneKey);
            set => EditorPrefs.SetBool(StartFromInitSceneKey, value);
        }

        /// <summary>
        /// Returns the instance ID of the starting scene group or -1 if none selected.
        /// </summary>
        public static int StartingSceneGroup
        {
            get => !EditorPrefs.HasKey(StartingSceneGroupKey) ? -1 : EditorPrefs.GetInt(StartingSceneGroupKey);
            set => EditorPrefs.SetInt(StartingSceneGroupKey, value);
        }

        public static bool SceneGroupsPageSidebarState
        {
            get => !EditorPrefs.HasKey(SceneGroupsPageSidebarStateKey) ||
                   EditorPrefs.GetBool(SceneGroupsPageSidebarStateKey);
            set => EditorPrefs.SetBool(SceneGroupsPageSidebarStateKey, value);
        }

        
        public static bool LayoutPageSidebarState
        {
            get => !EditorPrefs.HasKey(LayoutPageSidebarStateKey) || EditorPrefs.GetBool(LayoutPageSidebarStateKey);
            set => EditorPrefs.SetBool(LayoutPageSidebarStateKey, value);
        }

        
        public static NewSceneGroupTemplateObj NewSceneGroupTemplate
        {
            get
            {
                if (!EditorPrefs.HasKey(NewSceneGroupTemplateKey))
                {
                    return new NewSceneGroupTemplateObj(null, null, null, null);
                }

                var stringyfiedValue = EditorPrefs.GetString(NewSceneGroupTemplateKey);
                var newSceneGroupTemplate = JsonUtility.FromJson<NewSceneGroupTemplateObj>(stringyfiedValue);
                return newSceneGroupTemplate;
            }
            set
            {
                var stringyfiedValue = JsonUtility.ToJson(value);
                EditorPrefs.SetString(NewSceneGroupTemplateKey, stringyfiedValue);
            }
        }

        public static string SmartRenamerPattern
        {
            get => EditorPrefs.HasKey(SmartRenamerPatternKey) ? EditorPrefs.GetString(SmartRenamerPatternKey) : "";
            set => EditorPrefs.SetString(SmartRenamerPatternKey, value);
        }
        
        public struct NewSceneGroupTemplateObj
        {
            public string TemplateMainScenePath,
                TemplateUiScenePath,
                TemplateOtherScenePath,
                TemplateEnvironmentScenePath;

            public SceneAsset TemplateMainScene => !string.IsNullOrEmpty(TemplateMainScenePath)
                ? AssetDatabase.LoadAssetAtPath<SceneAsset>(TemplateMainScenePath)
                : null;

            public SceneAsset TemplateUiScene => !string.IsNullOrEmpty(TemplateUiScenePath)
                ? AssetDatabase.LoadAssetAtPath<SceneAsset>(TemplateUiScenePath)
                : null;

            public SceneAsset TemplateOtherScene => !string.IsNullOrEmpty(TemplateOtherScenePath)
                ? AssetDatabase.LoadAssetAtPath<SceneAsset>(TemplateOtherScenePath)
                : null;

            public SceneAsset TemplateEnvironmentScene => !string.IsNullOrEmpty(TemplateEnvironmentScenePath)
                ? AssetDatabase.LoadAssetAtPath<SceneAsset>(TemplateEnvironmentScenePath)
                : null;

            public NewSceneGroupTemplateObj(SceneAsset templateMainScene, SceneAsset templateUiScene,
                SceneAsset templateOtherScene, SceneAsset templateEnvironmentScene)
            {
                TemplateMainScenePath = templateMainScene != null
                    ? AssetDatabase.GetAssetOrScenePath(templateMainScene)
                    : null;
                TemplateUiScenePath =
                    templateUiScene != null ? AssetDatabase.GetAssetOrScenePath(templateUiScene) : null;
                TemplateOtherScenePath = templateOtherScene != null
                    ? AssetDatabase.GetAssetOrScenePath(templateOtherScene)
                    : null;
                TemplateEnvironmentScenePath = templateEnvironmentScene != null
                    ? AssetDatabase.GetAssetOrScenePath(templateEnvironmentScene)
                    : null;
            }

            public override string ToString()
            {
                return
                    $"Main: {TemplateMainScenePath}, UI: {TemplateUiScenePath}, Environment: {TemplateEnvironmentScenePath}, Other: {TemplateOtherScenePath}.";
            }
        }
        
        #endif
    }
}
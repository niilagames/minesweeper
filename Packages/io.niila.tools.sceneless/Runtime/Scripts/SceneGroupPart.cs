﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Niila.Tools.Sceneless
{

    public enum SceneGroupPart
    {
        MainScenes,
        UiScenes,
        EnvironmentScenes,
        OtherScenes
    }

    public static class SceneGroupPartUtils
    {
        /// <summary>
        /// Returns a new array of all scene group parts.
        /// </summary>
        public static SceneGroupPart[] All => new[]
        {
            SceneGroupPart.MainScenes, SceneGroupPart.EnvironmentScenes, SceneGroupPart.UiScenes,
            SceneGroupPart.OtherScenes
        };
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Niila.Tools.Sceneless.SceneGroupGraph;
#if UNITY_EDITOR
using Niila.Tools.Sceneless.Settings;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Niila.Tools.Sceneless
{
    public class ScenelessManager : MonoBehaviour
    {
        #region Singleton

        public static ScenelessManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    var obj = new GameObject("ScenelessManager");
                    _instance = obj.AddComponent<ScenelessManager>();
                }

                return _instance;
            }
        }

        private static ScenelessManager _instance;

        private void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                DontDestroyOnLoad(gameObject);
                _instance = this;
                Initialise();
            }
        }

        #endregion

        #region Setup

        /// <summary>
        /// NOTE: Used from the editor tool. Please do not call from code outside the tool.
        /// </summary>
        /// <param name="loadInitialSceneGroupOnAwake"></param>
        public void SetInput(bool loadInitialSceneGroupOnAwake)
        {
            _loadInitialSceneGroupOnAwake = loadInitialSceneGroupOnAwake;
        }

        #endregion

        #region Initialisation

        private void Initialise()
        {
            if (_hasInitialised)
            {
#if UNITY_EDITOR
                Debug.LogWarning(
                    "ScenelessManager has already been initialised. Nothing will happen when calling this after the first initialisation.");
#endif
                return;
            }

            // Set is loading to false.
            _isLoading = false;

            // Load data and init graph.
            var sceneSetupPath = "Sceneless/ScenelessSetup";
            _sceneSetup = Resources.Load<ScenelessSetup>(sceneSetupPath);
            _graphTraverser = new GraphTraverser();
            _sceneGroupLoader = new SceneGroupLoader();

            // Throw exception if setup wasn't found.
            if (_sceneSetup == null)
            {
                throw new NullReferenceException(
                    $"Couldn't find any scene setup file. Go to the setup window to create a scene setup.");
            }

            // Load first scene group (if requested).
            if (_loadInitialSceneGroupOnAwake)
            {
                if (_sceneSetup.SceneGroups.Count == 0)
                {
                    throw new Exception($"Scene setup must have at least one scene group to work.");
                }

#if UNITY_EDITOR
                // If using "Load on start" load saved scene if any, else if no startup scene group set, use the initial group (startup scene group is only used, when in the editor).
                if (_sceneSetup.LoadStateOnInit && HasSaveStateWithKey(_sceneSetup.StateToLoadOnInitKey))
                {
                    var sceneGroupNodeGUID = LoadStateWithKey(_sceneSetup.StateToLoadOnInitKey);
                    var navigationData = _graphTraverser.SetActiveNode(sceneGroupNodeGUID);
                    LoadSceneGroup(navigationData, true);
                }
                else
                {
                    // If we don't want to start from a scene group (or no startup scene group present),
                    // then skip loading a scene group, when in editor.
                    if (_sceneSetup.StartupSceneGroup == null || !ScenelessEditorSettings.StartFromInitScene)
                    {
                        return;
                    }
                    var sceneGroupToLoad = _sceneSetup.StartupSceneGroup;
                    var navigationData = new NavigationData(null, sceneGroupToLoad);
                    LoadSceneGroup(navigationData, true);
                }
#else
                // Always load first scene group (or saved scene group), when not in editor.
                if (_sceneSetup.LoadStateOnInit && HasSaveStateWithKey(_sceneSetup.StateToLoadOnInitKey))
                {
                    var sceneGroupNodeGUID = LoadStateWithKey(_sceneSetup.StateToLoadOnInitKey);
                    var navigationData = _graphTraverser.SetActiveNode(sceneGroupNodeGUID);
                    LoadSceneGroup(navigationData, true);
                }
                else
                {
                    var navigationData = new NavigationData(null,
                        _graphTraverser.ActiveSceneGroup);
                    LoadSceneGroup(navigationData, true);
                }
#endif
            }

            _hasInitialised = true;
        }

        private void OnEnable()
        {
            RegisterEvents();
        }

        private void OnDisable()
        {
            DeregisterEvents();
        }

        private void RegisterEvents()
        {
            if (_sceneGroupLoader == null)
            {
                return;
            }

            _sceneGroupLoader.OnBeforeUnloadPrevSceneGroup += RaiseOnBeforeUnloadPrevSceneGroup;
            _sceneGroupLoader.OnBeforeLoadNextSceneGroup += RaiseOnBeforeLoadNextSceneGroup;
            _sceneGroupLoader.OnAfterLoadedNextSceneGroup += RaiseOnAfterLoadedNextSceneGroup;
            _sceneGroupLoader.OnSceneGroupLoaded += OnSceneGroupLoaderSceneGroupLoaded;
        }

        private void DeregisterEvents()
        {
            if (_sceneGroupLoader == null)
            {
                return;
            }

            _sceneGroupLoader.OnBeforeUnloadPrevSceneGroup -= RaiseOnBeforeUnloadPrevSceneGroup;
            _sceneGroupLoader.OnBeforeLoadNextSceneGroup -= RaiseOnBeforeLoadNextSceneGroup;
            _sceneGroupLoader.OnAfterLoadedNextSceneGroup -= RaiseOnAfterLoadedNextSceneGroup;
            _sceneGroupLoader.OnSceneGroupLoaded -= OnSceneGroupLoaderSceneGroupLoaded;
        }

        private void OnSceneGroupLoaderSceneGroupLoaded(SceneGroup sceneGroup)
        {
            _isLoading = false;
            OnSceneGroupLoaded?.Invoke(sceneGroup);
        }

        #endregion

        #region Input

        [SerializeField] private bool _loadInitialSceneGroupOnAwake = true;

        #endregion

        #region Data

        private static ScenelessSetup _sceneSetup;

        #endregion

        #region State

        private static bool _hasInitialised;

        private static GraphTraverser _graphTraverser;

        private static SceneGroupLoader _sceneGroupLoader;

        private static bool _isLoading = false;

        #endregion

        #region Properties

        public bool IsLoading => _isLoading;

        public GraphTraverser GraphTraverser => _graphTraverser;

        public IEnumerable<SceneGroup> AllSceneGroups => _graphTraverser.AllSceneGroups;

        public SceneGroup ActiveSceneGroup => _graphTraverser.ActiveSceneGroup;

        public string ActiveSceneGroupId => _graphTraverser.ActiveNodeGUID;

        public string DefaultSaveStateKey => "NIILA-SCENELESS-STATE";

        #endregion

        #region Methods

        /// <summary>
        /// Finds the node referencing the given scene group in the <see cref="GraphTraverser"/> and sets it as
        /// the current active node. Then it loads the scene group. 
        /// </summary>
        /// <param name="sceneGroup"></param>
        /// <param name="forceFullLoad"></param>
        public void LoadSceneGroupViaGraphTraverser(SceneGroup sceneGroup, bool forceFullLoad)
        {
            LoadSceneGroup(_graphTraverser.SetActiveNode(sceneGroup), forceFullLoad);
        }
        
        /// <summary>
        /// NOTE: Only use this, if not using the Graph Traverser, as it will not update the Graph Traverser's state.
        /// </summary>
        /// <param name="sceneGroup"></param>
        /// <param name="forceFullLoad">Force all previous scenes to unload instead of only unloading the scenes that aren't part of the next scene group.</param>
        public void LoadSceneGroup(NavigationData navigationData, bool forceFullLoad)
        {
            if (_isLoading)
            {
                throw new Exception($"Can't load a scene group, while already loading one.");
            }

            _isLoading = true;
            
            // Update the inner state of the graph traverser, so the active scene group is correct.
            var graphActiveNodeUpdated = _graphTraverser.NotifySceneGroupChange(navigationData.TargetSceneGroup);
            if (!graphActiveNodeUpdated)
            {
                Debug.LogWarning($"Couldn't update graph traverser active node for the given scene group {navigationData.TargetSceneGroup.Name}. If this is fine, don't take notice of this warning.");
            }

            _sceneGroupLoader.LoadSceneGroup(navigationData.BaseSceneGroup, navigationData.TargetSceneGroup,
                forceFullLoad);
        }

        /// <summary>
        /// NOTE: Only use this, if not using the Graph Traverser, as it will not update the Graph Traverser's state.
        /// </summary>
        /// <param name="navigationData"></param>
        /// <param name="partsToForceLoad"></param>
        /// <exception cref="Exception"></exception>
        public void LoadSceneGroup(NavigationData navigationData, params SceneGroupPart[] partsToForceLoad)
        {
            if (_isLoading)
            {
                throw new Exception($"Can't load a scene group, while already loading one.");
            }

            _isLoading = true;
            
            // Update the inner state of the graph traverser, so the active scene group is correct.
            var graphActiveNodeUpdated = _graphTraverser.NotifySceneGroupChange(navigationData.TargetSceneGroup);
            if (!graphActiveNodeUpdated)
            {
                Debug.LogWarning($"Couldn't update graph traverser active node for the given scene group {navigationData.TargetSceneGroup.Name}. If this is fine, don't take notice of this warning.");
            }

            _sceneGroupLoader.LoadSceneGroup(navigationData.BaseSceneGroup, navigationData.TargetSceneGroup,
                partsToForceLoad);
        }

        /// <summary>
        /// Reload current active scene group. If parts to reload load are given, only those parts will be reloaded
        /// else if none specified all parts will be reloaded.
        /// </summary>
        /// <param name="partsToReloadLoad">None for full reload, else only specified will be reloaded.</param>
        public void ReloadActiveSceneGroupViaGraphTraverser(params SceneGroupPart[] partsToReloadLoad)
        {
            var navigationData = _graphTraverser.ReloadActive();

            if (partsToReloadLoad == null || partsToReloadLoad.Length == 0)
            {
                LoadSceneGroup(navigationData, true);
            }
            else
            {
                LoadSceneGroup(navigationData, partsToReloadLoad);
            }
        }

        #endregion

        #region SaveLoad

        private string GetDataKey(string key)
        {
            return $"{key}__data";
        }

        public void SaveStateWithKey<T>(string key, string sceneGroupId, T data)
        {
            PlayerPrefs.SetString(key, sceneGroupId);
            var dataString = JsonUtility.ToJson(data);
            PlayerPrefs.SetString(GetDataKey(key), dataString);
            PlayerPrefs.Save();
        }

        public void SaveStateWithKey(string key, string sceneGroupId)
        {
            PlayerPrefs.SetString(key, sceneGroupId);
            PlayerPrefs.Save();
        }

        public void SaveStateWithKey<T>(string key, T data)
        {
            SaveStateWithKey(key, ActiveSceneGroupId, data);
        }

        public void SaveStateWithKey(string key)
        {
            SaveStateWithKey(key, ActiveSceneGroupId);
        }

        public void SaveState<T>(string sceneGroupId, T data)
        {
            SaveStateWithKey(DefaultSaveStateKey, sceneGroupId, data);
        }

        public void SaveState(string sceneGroupId)
        {
            SaveStateWithKey(DefaultSaveStateKey, sceneGroupId);
        }

        public void SaveState<T>(T data)
        {
            SaveState(ActiveSceneGroupId, data);
        }

        public void SaveState()
        {
            SaveState(ActiveSceneGroupId);
        }

        public void SaveData<T>(string key, T data)
        {
            var dataString = JsonUtility.ToJson(data);
            PlayerPrefs.SetString(GetDataKey(key), dataString);
            PlayerPrefs.Save();
        }

        public void SaveData<T>(T data)
        {
            SaveData(DefaultSaveStateKey, data);
        }

        public string LoadStateWithKey(string key)
        {
            return PlayerPrefs.HasKey(key) ? PlayerPrefs.GetString(key) : null;
        }

        public string LoadState()
        {
            return LoadStateWithKey(DefaultSaveStateKey);
        }

        public T LoadDataWithKey<T>(string key)
        {
            var dataString = PlayerPrefs.HasKey(GetDataKey(key)) ? PlayerPrefs.GetString(GetDataKey(key)) : null;
            if (string.IsNullOrWhiteSpace(dataString))
            {
                return default(T);
            }

            return JsonUtility.FromJson<T>(dataString);
        }

        public T LoadData<T>()
        {
            return LoadDataWithKey<T>(DefaultSaveStateKey);
        }

        public bool HasSaveDataWithKey(string key)
        {
            return PlayerPrefs.HasKey(GetDataKey(key));
        }

        public bool HasSaveStateWithKey(string key)
        {
            return PlayerPrefs.HasKey(key);
        }

        public bool HasSaveData()
        {
            return HasSaveDataWithKey(DefaultSaveStateKey);
        }

        public bool HasSaveState()
        {
            return HasSaveStateWithKey(DefaultSaveStateKey);
        }

        /// <summary>
        /// Deletes both Sceneless state and custom data.
        /// </summary>
        /// <param name="key"></param>
        public void DeleteSaveStateWithKey(string key)
        {
            PlayerPrefs.DeleteKey(key);
            PlayerPrefs.DeleteKey(GetDataKey(key));
        }

        /// <summary>
        /// Deletes both Sceneless state and custom data.
        /// </summary>
        public void DeleteSaveState()
        {
            DeleteSaveStateWithKey(DefaultSaveStateKey);
        }

        #endregion

        #region Events

        /// <summary>
        /// Called after transition scene has been loaded and before previous scene group is unloaded.
        /// This is also called, when no transition scene exists..
        /// </summary>
        public event Action OnBeforeUnloadPrevSceneGroup;

        /// <summary>
        /// Called after previous scene group has been unloaded, but before next scene group is loaded.
        /// </summary>
        public event Action OnBeforeLoadNextSceneGroup;

        /// <summary>
        /// Called after next scene group has been loaded, but before transition scene is unloaded.
        /// </summary>
        public event Action OnAfterLoadedNextSceneGroup;

        /// <summary>
        /// Called at end of loading sequence, when transition scene has also been unloaded.
        /// </summary>
        public event Action<SceneGroup> OnSceneGroupLoaded;

        private void RaiseOnBeforeUnloadPrevSceneGroup()
        {
            OnBeforeUnloadPrevSceneGroup?.Invoke();
        }

        private void RaiseOnBeforeLoadNextSceneGroup()
        {
            OnBeforeLoadNextSceneGroup?.Invoke();
        }

        private void RaiseOnAfterLoadedNextSceneGroup()
        {
            OnAfterLoadedNextSceneGroup?.Invoke();
        }

        #endregion

        #region Utils

        public IEnumerable<Transform> CurrentSceneGroupRootObjects
        {
            get
            {
                var rootObjects = new List<Transform>();
                foreach (var sceneReference in ActiveSceneGroup.AllScenes)
                {
                    var scene = SceneManager.GetSceneByPath(sceneReference);
                    rootObjects.AddRange(scene.GetRootGameObjects().Select(go => go.transform));
                }

                return rootObjects;
            }
        }

        /// <summary>
        /// If all scenes in the current scene group have finished loading it just runs the action.
        /// If not, it adds the action to be called when all scenes have finished loading (it cleans up itself).
        /// This method automatically cleans up after the event is called.
        /// </summary>
        /// <param name="action"></param>
        public void RunOnceOnSceneGroupLoaded(Action<SceneGroup> action)
        {
            // If we are loading, add action to event listeners (also auto remove on notified).
            if (_isLoading)
            {
                OnSceneGroupLoaded += action;
                OnSceneGroupLoaded += (SceneGroup sceneGroup) => OnSceneGroupLoaded -= action;
            }
            // Else just run now with current scene group.
            else
            {
                action(ActiveSceneGroup);
            }
        }

        #endregion

        #region Internal

        private IEnumerator WaitForAsyncOperations(List<AsyncOperation> asyncOperations, Action callback)
        {
            while (!asyncOperations.All(ao => ao.isDone))
            {
                yield return null;
            }

            callback();
        }

        internal void StartWaitForAsyncOperation(List<AsyncOperation> asyncOperations, Action callback)
        {
            StartCoroutine(WaitForAsyncOperations(asyncOperations, callback));
        }

        private IEnumerator WaitFor(Func<bool> predicate, Action callback)
        {
            while (!predicate())
            {
                yield return null;
            }

            callback();
        }

        internal void StartWaitFor(Func<bool> predicate, Action callback)
        {
            StartCoroutine(WaitFor(predicate, callback));
        }

        #endregion
    }
}
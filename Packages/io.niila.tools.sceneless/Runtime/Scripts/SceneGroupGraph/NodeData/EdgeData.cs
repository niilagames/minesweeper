﻿using System;

namespace Niila.Tools.Sceneless.SceneGroupGraph.NodeData
{
    [Serializable]
    public struct EdgeData
    {
        public string BaseNodeGUID;
        public string TargetNodeGUID;

        public EdgeData(string baseNodeGuid, string targetNodeGuid)
        {
            BaseNodeGUID = baseNodeGuid;
            TargetNodeGUID = targetNodeGuid;
        }
    }
}
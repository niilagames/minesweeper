﻿using System;
using UnityEngine;

namespace Niila.Tools.Sceneless.SceneGroupGraph.NodeData
{
    [Serializable]
    public class RedirectReceiverNodeData : BaseNodeData
    {
        public string RedirectName;
        public string RedirectorNodeGUID;

        public RedirectReceiverNodeData(string guid, string redirectName, string redirectorNodeGuid, Vector2 position) : base(guid, position)
        {
            RedirectName = redirectName;
            RedirectorNodeGUID = redirectorNodeGuid;
        }
    }
}
﻿using System;
using UnityEngine;

namespace Niila.Tools.Sceneless.SceneGroupGraph.NodeData
{
    [Serializable]
    public abstract class BaseNodeData
    {
        public string GUID;
        public Vector2 Position;

        public BaseNodeData(string guid, Vector2 position)
        {
            GUID = guid;
            Position = position;
        }
    }
}
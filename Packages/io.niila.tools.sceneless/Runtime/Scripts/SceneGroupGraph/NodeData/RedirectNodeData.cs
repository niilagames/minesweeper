﻿using System;
using UnityEngine;

namespace Niila.Tools.Sceneless.SceneGroupGraph.NodeData
{
    [Serializable]
    public class RedirectNodeData : BaseNodeData
    {
        public string RedirectName;
        public string ReceiverNodeGUID;

        public RedirectNodeData(string guid, string redirectName, string receiverNodeGuid, Vector2 position) : base(guid, position)
        {
            RedirectName = redirectName;
            ReceiverNodeGUID = receiverNodeGuid;
        }
    }
}
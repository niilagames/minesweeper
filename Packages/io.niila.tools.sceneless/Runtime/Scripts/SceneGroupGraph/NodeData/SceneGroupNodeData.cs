﻿using System;
using UnityEngine;

namespace Niila.Tools.Sceneless.SceneGroupGraph.NodeData
{
    [Serializable]
    public class SceneGroupNodeData : BaseNodeData
    {
        public SceneGroup SceneGroup;

        public SceneGroupNodeData(string guid, SceneGroup sceneGroup, Vector2 position) : base(guid, position)
        {
            SceneGroup = sceneGroup;
        }
    }
}
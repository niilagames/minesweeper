﻿using System;
using System.Collections.Generic;
using System.Linq;
using Niila.Tools.Sceneless.SceneGroupGraph.NodeData;
using UnityEngine;

namespace Niila.Tools.Sceneless.SceneGroupGraph
{
    public class SceneGroupGraphData : ScriptableObject
    {
        public IEnumerable<BaseNodeData> NodeData => new List<BaseNodeData>()
            .Concat(SceneGroupNodeData)
            .Concat(RedirectNodeData)
            .Concat(RedirectReceiverNodeData)
            .ToList().AsReadOnly();

        public string EntryNodeGUID;
        
        [Header("Edge Data")]
        public List<EdgeData> EdgeData = new List<EdgeData>();

        [Header("Node Data")]
        public List<SceneGroupNodeData> SceneGroupNodeData = new List<SceneGroupNodeData>();
        public List<RedirectNodeData> RedirectNodeData = new List<RedirectNodeData>();
        public List<RedirectReceiverNodeData> RedirectReceiverNodeData = new List<RedirectReceiverNodeData>();


        public void Clear()
        {
            EntryNodeGUID = null;

            SceneGroupNodeData.Clear();
            RedirectNodeData.Clear();
            RedirectReceiverNodeData.Clear();

            EdgeData.Clear();
        }

        public void AddNodeData(BaseNodeData nodeData)
        {
            switch (nodeData)
            {
                case SceneGroupNodeData sgnData:
                    SceneGroupNodeData.Add(sgnData);
                    break;
                case RedirectNodeData rnData:
                    RedirectNodeData.Add(rnData);
                    break;
                case RedirectReceiverNodeData rrnData:
                    RedirectReceiverNodeData.Add(rrnData);
                    break;
                default:
                    var typeName = nodeData?.GetType().Name ?? "(Null)";
                    throw new ArgumentOutOfRangeException($"Given node cannot be saved as saving for the type {typeName} hasn't been implemented.");
            }
        }
    }
}
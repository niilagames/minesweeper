﻿using System;
using System.Collections.Generic;
using System.Linq;
using Niila.Tools.Sceneless.SceneGroupGraph.NodeData;
using UnityEngine;

namespace Niila.Tools.Sceneless.SceneGroupGraph
{
    public class GraphTraverser
    {

        #region Properties
        public SceneGroup ActiveSceneGroup => ((SceneGroupNodeData)_nodes[_activeNodeGUID]).SceneGroup;
        public string ActiveNodeGUID => _activeNodeGUID;
        public SceneGroup[] AllSceneGroups =>
            _nodes.Values.OfType<SceneGroupNodeData>().Where(n => n.SceneGroup != null).Select(n => n.SceneGroup).ToArray();

        public bool HasNext => _nextEdges.ContainsKey(_activeNodeGUID);

        public bool HasPrevious => _previousNodeGUIDs.Count > 0;
        
        #endregion
        
        #region State

        private Dictionary<string, BaseNodeData> _nodes = new Dictionary<string, BaseNodeData>();
        /// <summary>
        /// Maps from a scene group's instance ID to the GUID of the node it is connected to (if any).
        /// </summary>
        private Dictionary<int, string> _sceneGroupToNodeGuidMap = new Dictionary<int, string>();
        /// <summary>
        /// Maps from the GUID of the base node to the GUID of the target node. Represent the NEXT output on all nodes.
        /// </summary>
        private Dictionary<string, string> _nextEdges = new Dictionary<string, string>();

        private string _entryNodeGUID;
        private string _activeNodeGUID;
        private Stack<string> _previousNodeGUIDs = new Stack<string>();

        #endregion
        
        #region Constructor&Init
        
        public GraphTraverser()
        {
            // Load data.
            var graphPath = "Sceneless/SceneGraph";
            var graphData = Resources.Load<SceneGroupGraphData>(graphPath);
            
            // Init graph.
            InitGraph(graphData);
        }
        
        private void InitGraph(SceneGroupGraphData graphData)
        {
            // Clear existing data.
            _nodes.Clear();
            _nextEdges.Clear();
            _activeNodeGUID = null;
            _previousNodeGUIDs.Clear();

            // Save entry node GUID.
            _entryNodeGUID = graphData.EntryNodeGUID;
            
            // Add all nodes to a dictionary for easy lookup.
            foreach (var node in graphData.NodeData)
            {
                _nodes.Add(node.GUID, node);
                // Also add scene group instance ID -> node GUID map entry, if it is a scene group node. 
                if (node is SceneGroupNodeData sgnd)
                {
                    _sceneGroupToNodeGuidMap.Add(sgnd.SceneGroup.GetInstanceID(), sgnd.GUID);
                }
            }
            
            // Map edges.
            foreach (var edge in graphData.EdgeData)
            {
                _nextEdges.Add(edge.BaseNodeGUID, edge.TargetNodeGUID);
            }
            
            // Set current node GUID to that of the node after the entry node.
            _activeNodeGUID = _nextEdges[_entryNodeGUID];
        }

        #endregion

        #region Methods

        public NavigationData SetActiveNode(string nodeGUID)
        {
            // Save base node values for return data (is active node before switch).
            var baseNodeGUID = _activeNodeGUID;
            var baseNodeSceneGroup = ActiveSceneGroup;
            
            // Check if it is a valid GUID. If not, throw an exception.
            if (!_nodes.ContainsKey(nodeGUID))
            {
                throw new ArgumentException($"No node with the given node GUID {nodeGUID} exists.");
            }

            // Set active node GUID.
            _activeNodeGUID = ResolveTillAtSceneGroupNode(nodeGUID);
            
            // Return the scene group of the new active node.
            var activeNode = (SceneGroupNodeData)_nodes[_activeNodeGUID];
            if (activeNode.SceneGroup != null)
            {
                // Get target scene group from new active scene group.
                var targetSceneGroup = ActiveSceneGroup;
                return new NavigationData(baseNodeSceneGroup, targetSceneGroup);
            }
            
            // Throw an exception, if no scene group was attached to the node.
            throw new NullReferenceException($"The node with GUID {activeNode.GUID} doesn't have a scene group attached.");
        }

        /// <summary>
        /// Finds the node that references the given scene group and sets that node the be the active node.
        /// It then returns the navigation data object to use for navigating to the scene group.<br/>
        /// NOTE: Several nodes referring to the same scene group is currently not supported. It might result in unwanted behaviour.
        /// </summary>
        /// <param name="sceneGroup"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public NavigationData SetActiveNode(SceneGroup sceneGroup)
        {
            var nodeGUID = _sceneGroupToNodeGuidMap.ContainsKey(sceneGroup.GetInstanceID())
                ? _sceneGroupToNodeGuidMap[sceneGroup.GetInstanceID()]
                : null;

            if (nodeGUID == null)
            {
                throw new ArgumentNullException($"No graph node was found that references the given scene group with the name {sceneGroup.Name}.");
            }

            return SetActiveNode(nodeGUID);
        }

        /// <summary>
        /// NOTE: Should only be used internally.<br/>
        /// It sets the active scene group without adding to history or returning navigation data.
        /// </summary>
        /// <param name="sceneGroup"></param>
        /// <returns>Returns true, if active scene group was succesfully updated (if necessary). False if no valid node was found.</returns>
        /// <exception cref="ArgumentException"></exception>
        public bool NotifySceneGroupChange(SceneGroup sceneGroup)
        {
            var nodeGUID = _sceneGroupToNodeGuidMap.ContainsKey(sceneGroup.GetInstanceID())
                ? _sceneGroupToNodeGuidMap[sceneGroup.GetInstanceID()]
                : null;

            // If the node is already the active node, return success.
            if (nodeGUID == ActiveNodeGUID)
            {
                return true;
            }

            // Return false as the node with the scene group wasn't found.
            if (nodeGUID == null)
            {
                return false;
            }
            
            // Check if it is a valid GUID. If not, return false indicating failure.
            if (!_nodes.ContainsKey(nodeGUID))
            {
                return false;
            }

            // Set active node GUID and return success.
            _activeNodeGUID = ResolveTillAtSceneGroupNode(nodeGUID);
            return true;
        }
        
        #endregion
        
        #region Navigation
        
        /// <summary>
        /// Go to the current node's next node. Returns the scene group of the new node.
        /// </summary>
        /// <returns></returns>
        public NavigationData GotoNext()
        {
            // Save base node values for return data (is active node before switch).
            var baseNodeGUID = _activeNodeGUID;
            var baseNodeSceneGroup = ActiveSceneGroup;
            
            // Add active node GUID to stack of previous node GUIDs.
            _previousNodeGUIDs.Push(baseNodeGUID);
            
            // Get next scene group node GUID.
            _activeNodeGUID = GetNextSceneGroupNodeGUID(_activeNodeGUID);

            // Return the scene group of the new active node.
            var activeNode = (SceneGroupNodeData)_nodes[_activeNodeGUID];
            if (activeNode.SceneGroup != null)
            {
                // Get target scene group from new active scene group.
                var targetSceneGroup = ActiveSceneGroup;
                return new NavigationData(baseNodeSceneGroup, targetSceneGroup);
            }
            
            // Throw an exception, if no scene group was attached to the node.
            throw new NullReferenceException($"The node with GUID {activeNode.GUID} doesn't have a scene group attached.");
        }

        /// <summary>
        /// Previous means the previous active node. If none applicable, the method will do nothing and return null.
        /// </summary>
        /// <returns></returns>
        public NavigationData GotoPrevious()
        {
            if (_previousNodeGUIDs.Count == 0)
            {
                return null;
            }
            
            // Save base node values for return data (is active node before switch).
            var baseNodeSceneGroup = ActiveSceneGroup;

            // Remove the last previous node GUID and use that as the current one (go back in history).
            _activeNodeGUID = _previousNodeGUIDs.Pop();
            
            // Save target node values for return data (is active node after switch).
            var targetSceneGroup = ActiveSceneGroup;
            
            // Return the scene group of the new active node.
            return new NavigationData(baseNodeSceneGroup, targetSceneGroup);
        }

        /// <summary>
        /// Returns navigation data for reloading the active scene group.
        /// </summary>
        /// <returns></returns>
        public NavigationData ReloadActive()
        {
            return new NavigationData(ActiveSceneGroup, ActiveSceneGroup);
        }
        
        /// <summary>
        /// Goes to the next node and executes any special node logic until it gets to the next scene group node,
        /// whose GUID it returns. This is used to traverse from scene group to scene group.
        /// </summary>
        /// <param name="baseNodeGuid"></param>
        /// <returns></returns>
        private string GetNextSceneGroupNodeGUID(string baseNodeGuid)
        {
            // Get the GUID of the next node (or wrap around if none).
            string nextNodeGUID = GetNextOrInitialNodeGuid(baseNodeGuid);

            // Returns next node GUID, if it is a scene group node, else resolves until it is
            // and then returns the GUID.
            return ResolveTillAtSceneGroupNode(nextNodeGUID);
        }

        /// <summary>
        /// If the given node is a scene group node, return the node GUID.
        /// If not, return the next reached scene group node GUID.
        /// </summary>
        /// <param name="baseNodeGuid"></param>
        /// <returns></returns>
        private string ResolveTillAtSceneGroupNode(string baseNodeGuid)
        {
            // Get the node data.
            var nodeData = _nodes[baseNodeGuid];
            
            // Check node data type.
            switch (nodeData)
            {
                case RedirectNodeData rnData:
                    // Return the node GUID for the node that we redirect to.
                    var receiverGUID = rnData.ReceiverNodeGUID;
                    // Return the GUID of the node the receiver points to or the initial
                    // node GUID if it doesn't point anywhere.
                    return GetNextOrInitialNodeGuid(receiverGUID);
                case RedirectReceiverNodeData rrnData:
                    // Return the GUID of the node the receiver is pointing to
                    // or the initial node GUID, if not pointing anywhere.
                    return GetNextOrInitialNodeGuid(rrnData.GUID);
                default:
                    // If not a special node, return the node's GUID.
                    return nodeData.GUID;
            }
        }

        /// <summary>
        /// Returns the GUID of the node immediately following this node or the initial node, if none follows. 
        /// </summary>
        /// <param name="baseNodeGuid"></param>
        /// <returns></returns>
        private string GetNextOrInitialNodeGuid(string baseNodeGuid)
        {
            if (_nextEdges.ContainsKey(baseNodeGuid))
            {
                return _nextEdges[baseNodeGuid];
            }

            return _nextEdges[_entryNodeGUID];
        }
        
        #endregion
    }
}
﻿namespace Niila.Tools.Sceneless.SceneGroupGraph
{
    /// <summary>
    /// Represents where to navigate (and where from) used by the Sceneless Manager
    /// to properly load the next scene group.
    /// </summary>
    public class NavigationData
    {
        public SceneGroup BaseSceneGroup;
        public SceneGroup TargetSceneGroup;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseSceneGroup"></param>
        /// <param name="targetSceneGroup"></param>
        public NavigationData(SceneGroup baseSceneGroup, SceneGroup targetSceneGroup)
        {
            BaseSceneGroup = baseSceneGroup;
            TargetSceneGroup = targetSceneGroup;
        }
    }
}
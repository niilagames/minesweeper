﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Niila.Tools.Sceneless.Extensions
{
    public static class SceneReferenceExtensions
    {

        private static readonly IEqualityComparer<SceneReference> SceneReferenceComparer = new SceneReferenceEqualityComparer();

        public static IEnumerable<SceneReference> ExceptThese(this IEnumerable<SceneReference> originalList,
            IEnumerable<SceneReference> listOfExceptions)
        {
            if (originalList == null)
            {
                throw new ArgumentNullException(nameof(originalList));
            }
            if (listOfExceptions == null)
            {
                throw new ArgumentNullException(nameof(listOfExceptions));
            }

            return originalList.Except(listOfExceptions, SceneReferenceComparer);
        }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Niila.Tools.Sceneless
{
    [CreateAssetMenu(menuName = "Scene management tool/Scene group")]
    public class SceneGroup : ScriptableObject
    {
        public IEnumerable<SceneReference> AllScenes =>
            _uiScenes
                .Where(s => s != null && !string.IsNullOrWhiteSpace(s.ScenePath))
                .Concat(_environmentScenes.Where(s => s != null && !string.IsNullOrWhiteSpace(s.ScenePath)))
                .Concat(_otherScenes.Where(s => s != null && !string.IsNullOrWhiteSpace(s.ScenePath)))
                .Concat(_mainScenes.Where(s => s != null && !string.IsNullOrWhiteSpace(s.ScenePath))).ToArray();

        public int Count => _mainScenes.Count + _uiScenes.Count + _environmentScenes.Count + _otherScenes.Count;

        public SceneReference ActiveScene => _activeScene;
        public SceneReference MainCameraScene => _mainCameraScene;

        public string Name => _name;

        public string Description => _description;

        [SerializeField]
        private string _name = "";
        [TextArea]
        [SerializeField]
        private string _description = "";

        [SerializeField]
        private List<SceneReference> _mainScenes = new List<SceneReference>();
        [SerializeField]
        private List<SceneReference> _uiScenes = new List<SceneReference>();
        [SerializeField]
        private List<SceneReference> _environmentScenes = new List<SceneReference>();
        [SerializeField]
        private List<SceneReference> _otherScenes = new List<SceneReference>();
        [SerializeField]
        private SceneReference _activeScene = null;
        [SerializeField]
        private SceneReference _mainCameraScene = null;

        public void SetScenes(IEnumerable<string> mainScenePaths, IEnumerable<string> uiScenePaths, IEnumerable<string> environmentScenePaths, IEnumerable<string> otherScenePaths)
        {
            _mainScenes = mainScenePaths.Select(sp => new SceneReference {ScenePath = sp}).ToList();
            _uiScenes = uiScenePaths.Select(sp => new SceneReference { ScenePath = sp }).ToList();
            _environmentScenes = environmentScenePaths.Select(sp => new SceneReference { ScenePath = sp }).ToList();
            _otherScenes = otherScenePaths.Select(sp => new SceneReference { ScenePath = sp }).ToList();
        }

        public IEnumerable<SceneReference> GetScenesInclude(params SceneGroupPart[] partsToInclude)
        {
            List<SceneReference> scenes = new List<SceneReference>();

            if (partsToInclude.Contains(SceneGroupPart.MainScenes))
            {
                scenes.AddRange(_mainScenes);
            }

            if (partsToInclude.Contains(SceneGroupPart.UiScenes))
            {
                scenes.AddRange(_uiScenes);
            }

            if (partsToInclude.Contains(SceneGroupPart.EnvironmentScenes))
            {
                scenes.AddRange(_environmentScenes);
            }

            if (partsToInclude.Contains(SceneGroupPart.OtherScenes))
            {
                scenes.AddRange(_otherScenes);
            }

            return scenes;
        }

        public IEnumerable<SceneReference> GetScenesExcept(params SceneGroupPart[] partsToExclude)
        {
            List<SceneReference> scenes = new List<SceneReference>();

            if (!partsToExclude.Contains(SceneGroupPart.MainScenes))
            {
                scenes.AddRange(_mainScenes);
            }

            if (!partsToExclude.Contains(SceneGroupPart.UiScenes))
            {
                scenes.AddRange(_uiScenes);
            }

            if (!partsToExclude.Contains(SceneGroupPart.EnvironmentScenes))
            {
                scenes.AddRange(_environmentScenes);
            }

            if (!partsToExclude.Contains(SceneGroupPart.OtherScenes))
            {
                scenes.AddRange(_otherScenes);
            }

            return scenes;
        }
    }

    public abstract class SceneGroup<T> : SceneGroup where T : struct
    {

        public T Metadata => _metadata;

        [SerializeField]
        private T _metadata = default(T);

    }

}
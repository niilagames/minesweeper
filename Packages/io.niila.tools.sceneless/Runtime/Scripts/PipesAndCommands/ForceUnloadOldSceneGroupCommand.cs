﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Niila.Tools.Sceneless.PipesAndCommands
{
    /// <summary>
    /// Force unloading a scene group means to fully unload all current scenes, no matter whether they are used in the next scene group.
    /// NOTE: Always use this together with <see cref="ForceLoadNewSceneGroupCommand"/>. Using <see cref="LoadNewSceneGroupCommand"/> might result in errors.
    /// </summary>
    internal class ForceUnloadOldSceneGroupCommand : ACommand
    {
        private SceneGroup _currentSceneGroup;

        public ForceUnloadOldSceneGroupCommand(SceneGroup currentSceneGroup)
        {
            _currentSceneGroup = currentSceneGroup;
        }

        public override void Execute(Action callback)
        {
            // If there is no current scene group to unload, just invoke the callback.
            if (_currentSceneGroup == null)
            {
                callback?.Invoke();
                return;
            }

            List<AsyncOperation> unloadAsyncOperations = new List<AsyncOperation>();

            var scenesToUnload = _currentSceneGroup != null ? _currentSceneGroup.AllScenes : new SceneReference[0];

            foreach (var sceneToUnload in scenesToUnload)
            {
                var unloadOperation = SceneManager.UnloadSceneAsync(sceneToUnload);
                unloadAsyncOperations.Add(unloadOperation);
            }

            _waitForAsyncAction(unloadAsyncOperations, callback);
        }
    }

}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Niila.Tools.Sceneless.PipesAndCommands
{
    /// <summary>
    /// Force loading a scene group means to unload all previous scenes, although they are also part of the next scene group.
    /// This can also be used to reload a scene group completely.
    /// This command can be used with <see cref="ForceUnloadOldSceneGroupCommand"/>, but it's not strictly necessary, as this command makes sure no previous scenes stay loaded.
    /// </summary>
    internal class ForceLoadNewSceneGroupCommand : ACommand
    {
        private SceneGroup _nextSceneGroup;

        public ForceLoadNewSceneGroupCommand(SceneGroup nextSceneGroup)
        {
            _nextSceneGroup = nextSceneGroup;
        }

        public override void Execute(Action callback)
        {
            List<AsyncOperation> loadAsyncOperations = new List<AsyncOperation>();

            var scenesToLoad = _nextSceneGroup.AllScenes.ToList();



            // Removing it allows us to make sure that we load it first, no matter where in the list it was present.
            scenesToLoad.RemoveAll(sr => sr.ScenePath == _nextSceneGroup.MainCameraScene.ScenePath);
            // Load the main camera scene.
            // This should be synchronous to make sure that it is loaded as the first thing.
            // NOTE: Loading using single mode, so previous scenes are completely unloaded beforehand.
            SceneManager.LoadScene(_nextSceneGroup.MainCameraScene, LoadSceneMode.Single);

            foreach (var sceneToLoad in scenesToLoad)
            {
                var loadOperation = SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive);
                // If starting load of the active scene, set it as the active scene upon completion.
                if (_nextSceneGroup.ActiveScene == sceneToLoad)
                {
                    loadOperation.completed += operation =>
                    {
                        var activeScene = SceneManager.GetSceneByPath(sceneToLoad);
                        SceneManager.SetActiveScene(activeScene);
                    };
                }

                loadAsyncOperations.Add(loadOperation);
            }

            // Activate active scene and invoke the callback when loading is done.
            _waitForAsyncAction(loadAsyncOperations, () =>
            {
                var activeScene = SceneManager.GetSceneByPath(_nextSceneGroup.ActiveScene);
                SceneManager.SetActiveScene(activeScene);
                callback();
            });
        }
    }
}
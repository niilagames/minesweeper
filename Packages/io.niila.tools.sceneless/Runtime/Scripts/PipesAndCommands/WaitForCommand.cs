﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Niila.Tools.Sceneless.PipesAndCommands
{

    /// <summary>
    /// Waits until the predicate function returns true.
    /// </summary>
    internal class WaitForCommand : ACommand
    {
        private Func<bool> _predicate;

        public WaitForCommand(Func<bool> predicate)
        {
            _predicate = predicate;
        }

        public override void Execute(Action callback)
        {
            _waitForAction(_predicate, callback);
        }
    }

}
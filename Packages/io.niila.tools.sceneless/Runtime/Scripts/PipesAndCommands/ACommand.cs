﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Niila.Tools.Sceneless.PipesAndCommands
{

    internal abstract class ACommand
    {

        protected Action<List<AsyncOperation>, Action> _waitForAsyncAction;
        protected Action<Func<bool>, Action> _waitForAction;

        public void InitCommand(Action<List<AsyncOperation>, Action> waitForAsyncAction,
            Action<Func<bool>, Action> waitForAction)
        {
            _waitForAsyncAction = waitForAsyncAction;
            _waitForAction = waitForAction;
        }

        public abstract void Execute(Action callback);
    }
}
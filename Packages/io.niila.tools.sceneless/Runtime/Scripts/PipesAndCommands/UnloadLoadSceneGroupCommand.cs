﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Niila.Tools.Sceneless.Extensions;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Niila.Tools.Sceneless.PipesAndCommands
{

    internal class UnloadLoadSceneGroup : ACommand
    {

        private SceneGroup _currentSceneGroup, _nextSceneGroup;
        private SceneGroupPart[] _partsToForceLoad;

        public UnloadLoadSceneGroup(SceneGroup currentSceneGroup, SceneGroup nextSceneGroup, params SceneGroupPart[] partsToForceLoad)
        {
            _currentSceneGroup = currentSceneGroup;
            _nextSceneGroup = nextSceneGroup;
            _partsToForceLoad = partsToForceLoad;
        }

        public override void Execute(Action callback)
        {
            List<AsyncOperation> asyncOperations = new List<AsyncOperation>();

            var curScenes = _currentSceneGroup != null ? _currentSceneGroup.AllScenes : new SceneReference[0];
            var nextScenes = _nextSceneGroup.AllScenes;
            var scenesToForceLoad = _nextSceneGroup != null ? _nextSceneGroup.GetScenesInclude(_partsToForceLoad) : new SceneReference[0];

            // Find scenes that need to be unloaded (all scenes in current group that are not in next group, but including the ones to force load).
            var scenesToForceLoadExistingInCur = scenesToForceLoad.Intersect(curScenes).ToArray();
            var scenesToUnload = curScenes.ExceptThese(nextScenes).Union(scenesToForceLoadExistingInCur).ToArray();
            // Find scenes that need to be loaded (all scenes in next group, which are not in prev group, including the ones to force load).
            var scenesToLoad = nextScenes.ExceptThese(curScenes).Union(scenesToForceLoad).ToList();

            // If we need to load the next scene group's main camera scene, remove it from the list and then load it first before anything else.
            if (scenesToLoad.Contains(_nextSceneGroup.MainCameraScene) &&
                scenesToLoad.First() != _nextSceneGroup.MainCameraScene)
            {
                // Moving the main camera scene to the first spot will most likely make it load first.
                scenesToLoad.Remove(_nextSceneGroup.MainCameraScene);
                scenesToLoad.Prepend(_nextSceneGroup.MainCameraScene);
            }

            // Unload all scenes to be unloaded.
            foreach (var sceneToUnload in scenesToUnload)
            {
                asyncOperations.Add(SceneManager.UnloadSceneAsync(sceneToUnload));
            }

            // Load all scenes to be loaded.
            foreach (var sceneToLoad in scenesToLoad)
            {
                var loadOperation = SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive);

                asyncOperations.Add(loadOperation);
            }

            // Activate active scene and invoke the callback when loading is done.
            _waitForAsyncAction(asyncOperations, () =>
            {
                var activeScene = SceneManager.GetSceneByPath(_nextSceneGroup.ActiveScene);
                SceneManager.SetActiveScene(activeScene);
                callback();
            });
        }
    }

}
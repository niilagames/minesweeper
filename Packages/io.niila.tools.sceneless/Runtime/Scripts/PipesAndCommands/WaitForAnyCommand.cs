﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Niila.Tools.Sceneless.PipesAndCommands
{

    /// <summary>
    /// Waits for the scene wait for state set to contain any of the given waitFors.
    /// </summary>
    internal class WaitForAnyCommand : ACommand
    {
        private readonly HashSet<StopWaitingFor> _stopWaitingForSet;
        private readonly StopWaitingFor[] _waitForAnyOfThese;

        public WaitForAnyCommand(ref HashSet<StopWaitingFor> stopWaitingForSet, params StopWaitingFor[] waitForAnyOfThese)
        {
            _stopWaitingForSet = stopWaitingForSet;
            _waitForAnyOfThese = waitForAnyOfThese;
        }

        public override void Execute(Action callback)
        {
            _waitForAction(() =>
            {
                bool anyContained = false;
                foreach (var waitForThis in _waitForAnyOfThese)
                {
                    anyContained = anyContained || _stopWaitingForSet.Contains(waitForThis);
                }

                return anyContained;
            }, callback);
        }
    }

}
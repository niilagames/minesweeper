﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Niila.Tools.Sceneless.PipesAndCommands
{

    internal class UnloadTransitionSceneCommand : ACommand
    {
        private string _transitionScenePath;

        public UnloadTransitionSceneCommand(string transitionScenePath)
        {
            _transitionScenePath = transitionScenePath;
        }

        public override void Execute(Action callback)
        {
            List<AsyncOperation> transitionUnloadOperations = new List<AsyncOperation>
            {
                SceneManager.UnloadSceneAsync(_transitionScenePath)
            };

            _waitForAsyncAction(transitionUnloadOperations, callback);
        }
    }

}
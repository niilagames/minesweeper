﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Niila.Tools.Sceneless.PipesAndCommands
{

    /// <summary>
    /// Waits for the scene wait for state set to contain all given waitFors.
    /// </summary>
    internal class WaitForAllCommand : ACommand
    {
        private readonly HashSet<StopWaitingFor> _stopWaitingForSet;
        private readonly StopWaitingFor[] _waitForAllOfThese;

        public WaitForAllCommand(ref HashSet<StopWaitingFor> stopWaitingForSet, params StopWaitingFor[] waitForAllOfThese)
        {
            _stopWaitingForSet = stopWaitingForSet;
            _waitForAllOfThese = waitForAllOfThese;
        }

        public override void Execute(Action callback)
        {
            _waitForAction(() =>
            {
                bool allContained = true;
                foreach (var waitForThis in _waitForAllOfThese)
                {
                    allContained = allContained && _stopWaitingForSet.Contains(waitForThis);
                }

                return allContained;
            }, callback);
        }
    }
}
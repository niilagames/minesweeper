﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Niila.Tools.Sceneless.PipesAndCommands
{

    internal class RunActionCommand : ACommand
    {
        private readonly Action _action;

        public RunActionCommand(Action action)
        {
            _action = action;
        }

        public override void Execute(Action callback)
        {
            _action();
            callback();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Niila.Tools.Sceneless.PipesAndCommands
{

    public enum StopWaitingFor
    {
        ReadyToUnloadPrevSceneGroup,
        ReadyToLoadNextSceneGroup,
        ReadyToUnloadTransitionScene,
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Niila.Tools.Sceneless.Extensions;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Niila.Tools.Sceneless.PipesAndCommands
{

    internal class LoadNewSceneGroupCommand : ACommand
    {

        private SceneGroup _currentSceneGroup, _nextSceneGroup;

        public LoadNewSceneGroupCommand(SceneGroup currentSceneGroup, SceneGroup nextSceneGroup)
        {
            _currentSceneGroup = currentSceneGroup;
            _nextSceneGroup = nextSceneGroup;
        }

        public override void Execute(Action callback)
        {
            List<AsyncOperation> loadAsyncOperations = new List<AsyncOperation>();

            var curScenes = _currentSceneGroup != null ? _currentSceneGroup.AllScenes : new SceneReference[0];
            var nextScenes = _nextSceneGroup.AllScenes;

            // Only load scenes not in previous scene group.
            var scenesToLoad = nextScenes.ExceptThese(curScenes).ToList();

            // If we need to load the next scene group's main camera scene, remove it from the list and then load it first before anything else.
            if (scenesToLoad.Contains(_nextSceneGroup.MainCameraScene) &&
                scenesToLoad.First() != _nextSceneGroup.MainCameraScene)
            {
                // Moving the main camera scene to the first spot will most likely make it load first.
                scenesToLoad.Remove(_nextSceneGroup.MainCameraScene);
                scenesToLoad.Prepend(_nextSceneGroup.MainCameraScene);
            }

            foreach (var sceneToLoad in scenesToLoad)
            {
                var loadOperation = SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive);
                loadAsyncOperations.Add(loadOperation);
            }

            // Activate active scene and invoke the callback when loading is done.
            _waitForAsyncAction(loadAsyncOperations, () =>
            {
                var activeScene = SceneManager.GetSceneByPath(_nextSceneGroup.ActiveScene);
                SceneManager.SetActiveScene(activeScene);
                callback();
            });
        }
    }

}
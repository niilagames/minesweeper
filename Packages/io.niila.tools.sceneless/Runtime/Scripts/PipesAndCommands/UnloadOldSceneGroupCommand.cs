﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Niila.Tools.Sceneless.Extensions;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Niila.Tools.Sceneless.PipesAndCommands
{

    internal class UnloadOldSceneGroupCommand : ACommand
    {
        private SceneGroup _currentSceneGroup, _nextSceneGroup;

        public UnloadOldSceneGroupCommand(SceneGroup currentSceneGroup, SceneGroup nextSceneGroup)
        {
            _currentSceneGroup = currentSceneGroup;
            _nextSceneGroup = nextSceneGroup;
        }

        public override void Execute(Action callback)
        {
            List<AsyncOperation> unloadAsyncOperations = new List<AsyncOperation>();

            var curScenes = _currentSceneGroup != null ? _currentSceneGroup.AllScenes : new SceneReference[0];
            var nextScenes = _nextSceneGroup.AllScenes;

            // Only unload scenes not used in next scene group.
            var scenesToUnload = curScenes.ExceptThese(nextScenes).ToArray();

            foreach (var sceneToUnload in scenesToUnload)
            {
                unloadAsyncOperations.Add(SceneManager.UnloadSceneAsync(sceneToUnload));
            }

            _waitForAsyncAction(unloadAsyncOperations, callback);
        }
    }

}
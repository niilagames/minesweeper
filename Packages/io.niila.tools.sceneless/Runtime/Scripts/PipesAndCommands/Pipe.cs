﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Niila.Tools.Sceneless.PipesAndCommands
{

    internal class Pipe
    {
        private List<ACommand> _commands = new List<ACommand>();
        private Action _callback;

        private Action<List<AsyncOperation>, Action> _waitForAsyncAction;
        private Action<Func<bool>, Action> _waitForAction;

        public Pipe(Action<List<AsyncOperation>, Action> waitForAsyncAction, Action<Func<bool>, Action> waitForAction)
        {
            _waitForAsyncAction = waitForAsyncAction;
            _waitForAction = waitForAction;
        }

        public Pipe Add(ACommand command)
        {
            command.InitCommand(_waitForAsyncAction, _waitForAction);
            _commands.Add(command);
            return this;
        }

        public void Execute(Action callback = null)
        {
            _callback = callback;
            _commands[0].Execute(() => ExecuteNext(1));
        }

        private void ExecuteNext(int index)
        {
            if (index < _commands.Count)
            {
                _commands[index].Execute(() => ExecuteNext(index + 1));
            }
            else
            {
                _callback?.Invoke();
            }
        }
    }
}
﻿using System;
using System.Linq;
using Niila.Tools.Sceneless.PipesAndCommands;
using UnityEngine;

namespace Niila.Tools.Sceneless
{
    public class SceneGroupLoader
    {
        
        #region Events

        /// <summary>
        /// Called after transition scene has been loaded and before previous scene group is unloaded.
        /// This is also called, when no transition scene exists...
        /// </summary>
        public event Action OnBeforeUnloadPrevSceneGroup;
        /// <summary>
        /// Called after previous scene group has been unloaded, but before next scene group is loaded.
        /// </summary>
        public event Action OnBeforeLoadNextSceneGroup;
        /// <summary>
        /// Called after next scene group has been loaded, but before transition scene is unloaded.
        /// </summary>
        public event Action OnAfterLoadedNextSceneGroup;
        /// <summary>
        /// Called at end of loading sequence, when transition scene has also been unloaded.
        /// </summary>
        public event Action<SceneGroup> OnSceneGroupLoaded;

        protected void RaiseOnBeforeUnloadPrevSceneGroup()
        {
            OnBeforeUnloadPrevSceneGroup?.Invoke();
        }

        protected void RaiseOnBeforeLoadNextSceneGroup()
        {
            OnBeforeLoadNextSceneGroup?.Invoke();
        }

        protected void RaiseOnAfterLoadedNextSceneGroup()
        {
            OnAfterLoadedNextSceneGroup?.Invoke();
        }

        protected void RaiseOnSceneGroupLoaded(SceneGroup sceneGroup)
        {
            OnSceneGroupLoaded?.Invoke(sceneGroup);
        }

        #endregion
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="activeSceneGroup">The currently active scene group.</param>
        /// <param name="sceneGroup"></param>
        /// <param name="onComplete">An action to take upon finishing the loading.</param>
        /// <param name="forceFullLoad">Force all previous scenes to unload instead of only unloading the scenes that aren't part of the next scene group.</param>
        public void LoadSceneGroup(SceneGroup activeSceneGroup, SceneGroup sceneGroup, bool forceFullLoad)
        {
            // Force full reload, if there are no scenes in common, as this could lead to weird behaviour.
            if (activeSceneGroup == null || !activeSceneGroup.HasCommonScenes(sceneGroup))
            {
                forceFullLoad = true;
            }

            if (!forceFullLoad)
            {
                new Pipe(ScenelessManager.Instance.StartWaitForAsyncOperation, ScenelessManager.Instance.StartWaitFor)
                    .Add(new RunActionCommand(RaiseOnBeforeUnloadPrevSceneGroup))
                    .Add(new UnloadOldSceneGroupCommand(activeSceneGroup, sceneGroup))
                    .Add(new RunActionCommand(RaiseOnBeforeLoadNextSceneGroup))
                    .Add(new LoadNewSceneGroupCommand(activeSceneGroup, sceneGroup))
                    .Add(new RunActionCommand(RaiseOnAfterLoadedNextSceneGroup))
                    .Execute(() =>
                    {
                        RaiseOnSceneGroupLoaded(sceneGroup);
                    });
            }
            else
            {
                new Pipe(ScenelessManager.Instance.StartWaitForAsyncOperation, ScenelessManager.Instance.StartWaitFor)
                    .Add(new RunActionCommand(RaiseOnBeforeUnloadPrevSceneGroup))
                    // NOTE: Not force unloading, as that would remove the last scene and that is not possible in Unity.
                    // The ForceLoadNewSceneGroup command below also loads the first scene non-additively, so it still works as expected.
                    .Add(new RunActionCommand(RaiseOnBeforeLoadNextSceneGroup))
                    .Add(new ForceLoadNewSceneGroupCommand(sceneGroup))
                    .Add(new RunActionCommand(RaiseOnAfterLoadedNextSceneGroup))
                    .Execute(() =>
                    {
                        RaiseOnSceneGroupLoaded(sceneGroup);
                    });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="activeSceneGroup">The currently active scene group.</param>
        /// <param name="sceneGroup"></param>
        /// <param name="partsToForceLoad">Force the given parts of a scene group to unload instead of keeping scenes that are part of the next scene group loaded.</param>
        public void LoadSceneGroup(SceneGroup activeSceneGroup, SceneGroup sceneGroup, params SceneGroupPart[] partsToForceLoad)
        {
            // If skipping all scenes, just use a full force reload.
            // If there are no common scenes between the current and next scene group, use full force reload as well.
            if ((partsToForceLoad.Contains(SceneGroupPart.MainScenes) &&
                partsToForceLoad.Contains(SceneGroupPart.UiScenes) &&
                partsToForceLoad.Contains(SceneGroupPart.EnvironmentScenes) &&
                partsToForceLoad.Contains(SceneGroupPart.OtherScenes)) || 
                activeSceneGroup == null ||
                !activeSceneGroup.HasCommonScenesExclude(sceneGroup, partsToForceLoad))
            {
                LoadSceneGroup(activeSceneGroup, sceneGroup, true);
                return;
            }

            new Pipe(ScenelessManager.Instance.StartWaitForAsyncOperation, ScenelessManager.Instance.StartWaitFor)
                .Add(new RunActionCommand(RaiseOnBeforeUnloadPrevSceneGroup))
                .Add(new RunActionCommand(RaiseOnBeforeLoadNextSceneGroup))
                .Add(new UnloadLoadSceneGroup(activeSceneGroup, sceneGroup, partsToForceLoad))
                .Add(new RunActionCommand(RaiseOnAfterLoadedNextSceneGroup))
                .Execute(() =>
                {
                    RaiseOnSceneGroupLoaded(sceneGroup);
                });
        }
    }
}
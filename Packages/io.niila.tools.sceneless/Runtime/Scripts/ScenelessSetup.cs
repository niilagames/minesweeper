﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.Sceneless
{
    public class ScenelessSetup : ScriptableObject
    {
        public List<SceneGroup> SceneGroups = new List<SceneGroup>();

        public SceneReference TransitionScene;
        public string DefaultSaveStateKey = "NIILA-SCENELESS-STATE";
        public bool LoadStateOnInit;
        public string StateToLoadOnInitKey = "NIILA-SCENELESS-STATE";

        /// <summary>
        /// EDITOR ONLY VALUE. Used by the Sceneless editor tool for saving which scene group to load
        /// when clicking the play button in the editor.
        /// </summary>
        [Header("Debug")]
        public SceneGroup StartupSceneGroup;
    }
}
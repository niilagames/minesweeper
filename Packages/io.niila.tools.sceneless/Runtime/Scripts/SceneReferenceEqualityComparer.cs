﻿using System.Collections;
using System.Collections.Generic;

namespace Niila.Tools.Sceneless
{

    public class SceneReferenceEqualityComparer : IEqualityComparer<SceneReference>
    {
        public bool Equals(SceneReference a, SceneReference b)
        {
            return a?.ScenePath == b?.ScenePath;
        }

        public int GetHashCode(SceneReference obj)
        {
            return obj.ScenePath.GetHashCode();
        }
    }
}
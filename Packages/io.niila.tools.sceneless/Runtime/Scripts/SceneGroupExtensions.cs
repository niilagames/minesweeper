﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Niila.Tools.Sceneless
{

    public static class SceneGroupExtensions
    {
        /// <summary>
        /// Returns true if the two scene groups have at least one scene in common.
        /// </summary>
        /// <param name="sceneGroup"></param>
        /// <param name="otherSceneGroup"></param>
        /// <returns></returns>
        public static bool HasCommonScenes(this SceneGroup sceneGroup, SceneGroup otherSceneGroup)
        {
            // If the intersection between the two scene groups' scenes have one or more scenes then they have at least one scene in common.
            return sceneGroup.AllScenes.Intersect(otherSceneGroup.AllScenes).Any();
        }

        /// <summary>
        /// Returns true, if the two scene groups have at least one scene in common from the parts not excluded.
        /// </summary>
        /// <param name="sceneGroup"></param>
        /// <param name="otherSceneGroup"></param>
        /// <param name="partsToExclude"></param>
        /// <returns></returns>
        public static bool HasCommonScenesExclude(this SceneGroup sceneGroup, SceneGroup otherSceneGroup,
            params SceneGroupPart[] partsToExclude)
        {
            // If the intersection between the two scene groups' scenes (except the excluded ones) have one or more scenes then they have at least one scene in common.
            return sceneGroup.GetScenesExcept(partsToExclude).Intersect(otherSceneGroup.GetScenesExcept(partsToExclude))
                .Any();
        }

    }
}
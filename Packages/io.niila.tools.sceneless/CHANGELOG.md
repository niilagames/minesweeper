# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),

## [Unreleased]
### Fixed
- Sometimes an extra Sceneless Manager object is created (might be when force loading/reloading).
### Added
- Add all scenes in all scene groups to build settings.
- Load scene group additively (don't unload prev scene group).
    - Load a next scene ahead of time (with root object disabled).
    - Enable scene group objects via sceneless.
    - Unload only some of the scene groups.
- Gain access to root objects of specific scene group, when more than one is loaded.
- Warnings on the Scene Groups page, when scene groups contain invalid scene references (for instance null values in scene reference lists).
  
## [3.1.3] - 2021-02-05
### Fixed
- Fixed bug where building a game would result in errors due to editor only code being present in the build code.

## [3.1.2] - 2021-02-03
### Fixed
- Fixed bug where going to play mode from the editor discarded the setting (load from init scene) and always loaded a scene group no matter what.
- Potentially fixed bug where, when going to play mode from the editor, the game would be stuck on the init scene when no scene groups exist.
- Fixed issue where the editor, when going to play mode, didn't use the local machine settings for "load from init scene" properly, which could result in unwanted behaviour when sharing the project between devices.

## [3.1.1] - 2020-12-02
### Fixed
- Fixed bug where the graph view would crash, when loading a node with a scene group value of null (not set or deleted).
- Potentially fixed bug where a specific part of a scene group couldn't be reloaded using the `LoadSceneGroup` method using the `partsToForceLoad` argument.

## [3.1.0] - 2020-11-03
### Added
- Made it possible to get a navigation data object for reloading the active scene group from the Graph Traverser.

# Changed
- The editor parts of Sceneless are now not auto referenced in the project and will need to be referenced manually in assembly definitions (usually not needed for normal use).

## [3.0.0] - 2020-10-08
### Added
- Completely changed the way Sceneless handles layouts. The old way is out and the new Sceneless Graph editor is in.
    - Layouts are now defined in a graph structure and allows for a lot more flexibility.
    - Besides the basic Scene Group Node, two navigation nodes have been added in the form of the `RedirectNode` and `RedirectReceiverNode` pair.
### Removed
- The old layout system has been completely removed.

## [2.7.1] - 2020-10-06
### Fixed
- Fixed bug where scene changes sometimes didn't result in the asset being marked dirty.
- Fixed a potential bug with active and main scene dropdowns that would sometimes become bugged out.
### Changed
- Minimal Unity version downgraded to `2019.4`, which is the 2019 LTS version.
- Minimal required version of EditorUIElements updated to v1.5.0.
- Changed the duplicate icon to better represent the action.
- Added tooltips to the action buttons (for instance, move up, trash and duplicate).

## [2.7.0] - 2020-09-22
### Added
- Added a duplicate scene group feature. This duplicates the scene group and adds the new copy to the bottom of the list.
- Made it possible to also save some custom data quite easily along with saving state.
    - It uses the Unity JsonUtility to serialise/deserialise the object.
### Changed
- Minimal Unity version updated to `2020.1`.

## [2.6.0] - 2020-08-18
### Fixed
- Updated the way the new scene group template is saved to fix bug where it didn't load scene assets correctly.
    - It now saves the scene paths instead of the serialized scene asset.
### Added
- Added the possibility to select scene groups in the scene groups list. This is currently only used with the Smart Renamer.

## [2.5.0] - 2020-08-04
### Added
- Added bulk renaming of scene groups using specific patterns. This allows for renaming of all scene groups at once to for instance "Level 1", "Level 2" etc.

## [2.4.1] - 2020-03-17
### Fixed
- Fixed problem, where there was a misalignment between the Sceneless version and the required UIElements version. This version of Sceneless requires the v1.4.0 version of UIElements.

## [2.4.0] - 2020-03-10
### Fixed
- Fixed bug where the active scene and main camera scenes would not save correctly.
- Fixed bug where adding a scene might not save correctly.
### Added
- The New Scene Group sidebar element now remembers previously selected scenes.

## [2.3.0] - 2020-03-03
### Added
- Buttons have been added to the list of scene groups to allow to move a scene group to top or bottom. Buttons now also use icons instead of text.

## [2.2.1] - 2020-03-03
### Fixed
- Fixed bug where the active scene in the scene group would not always become the active scene after load.

## [2.2.0] - 2020-02-25
### Added
- Made it possible to only force load certain parts of a scene group instead of either all or none.

## [2.1.3] - 2020-02-18
### Fixed
- Fixed bug where active scene and main camera scene would be reset after playing in the editor.
- Fixed bug where saved scene would not be loaded in the editor even though the setting was activated.
- Fixed bug where order of execution would interfere with the execution of the OnSceneGroupLoaded event and it's derivatives.
- Fixed bug where force reloading a scene would result in an error due to Unity not being able to unload the last scene.
### Added
- Added a utility function to the Sceneless Manager `RunOnceOnSceneGroupLoaded(Action<SceneGroup> action)`, which can be useful in Awake, Start or OnEnable methods, since it will be called once when the scene group has been loaded, but wait for all scenes to load first, if not finished.

## [2.1.2] - 2020-02-04
### Fixed
- Fixed bug where the scene group asset of a deleted scene group would remain as a subasset to the main asset. Also added automatic cleanup which runs when Unity starts to clean up unlinked scene groups from projects that used Sceneless prior to this update.

## [2.1.1] - 2020-01-28
### Fixed
- Fixed bug where getting next and previous scene group IDs using the LinearLayoutManager returned wrong IDs.
- Fixed critical bug where editor code was compiled at build time and non-editor code was used when in the editor. Version 2.1.0 of Sceneless (and perhaps version below) might not work properly at all.

## [2.1.0] - 2020-01-14
### Added
- Made it possible to get an ID for a scene group in the context of a scene group layout. Use the ScenelessManager or correct layout manager to fetch the ID from a scene group.
- Simple save and load functions added. Can be used out of the box or used with save keys to save more states. State is saved in player prefs.
- Made it possible to load a scene group in using the scene group ID.
### Fixed
- Fixed bug, where main layout asset would not be created, when first time setup, was run.
- Fixed bugs, where dropdowns inside the scene group drawer for selecting active scene etc. would mess up sometimes, when scene lists (main, UI etc.) contained empty scenes. This would result in weird UI errors.

## [2.0.0] - 2019-12-10
### Added
- Added scene group layouts. These define the layout of the scene groups and how two traverse between groups. New layout types can be added.
- Basic linear layout added as current possibility for scene group layout. It allows to traverse to previous and next scene groups and keeps track of current scene group.
- An active scene and a main camera scene should now be selected per scene group. The active scene will be set as the active scene after load and the main camera scene will always load first as to reduce flicker when switching scene groups.
- Made it possible to fetch all scene groups in a layout, which can be used to load a specific scene group.
- Made it possible to enforce full unload of previous scene. This can be achieved by setting the forceFullLoad boolean to true, when calling to load a scene group.
### Fixed
- Fixed bug, where moving scene groups around in the Sceneless window would not result in the editor detecting a change in the asset file and therefore the change would not be saved properly.
### Changed
- Now using Editor UI Elements v1.3.0.

## [1.2.1] - 2019-11-19
### Added
- Console now displays error message, when the init scene couldn't be added to build settings. It explains how to recover from this error manually.
### Fixed
- Fixed bug, where it creating the folder structure would always throw an error, because it tried to create a folder with no name.
- Fixed some visual annoyances in the list of scene groups.
- Fixed bug, where parts of the system would get null pointer exceptions, due to unhandled null arguments for a UI element.

## [1.2.0] - 2019-10-15
### Added
- Deleting an existing scene group.
- Moving around existing scene groups.

## [1.1.0] - 2019-10-15
### Added
- Setup for a transition scene.
- Transition scene settings object that can control loading/unloading of scene groups.
### Fixed
- Various smaller bugs.

## [1.0.0] - 2019-10-01
### Added
- Basic working setup, where scene groups can be added to a list and can be loaded in order or by loading a specific scene group.
# Sceneless Guide

Sceneless is a scene manager for handling loading/unloading of scenes seamlessly.
It furthermore includes a way of handling a transition scene, which can be displayed between scene loads.

## Initial setup

To setup Sceneless go to the top menu bar and go to `Tools->Sceneless setup`.
From here click the button to set up Sceneless.

Setting up sceneless will create the following assets in your project:

- Sceneless/Resources/Sceneless/ScenelessSetup (scriptable object asset)
- Sceneless/Sceneless_InitSceneManagement (scene)

If succesful it should display the Sceneless setup options for you to change and scene groups should be able to be created.

### ScenelessSetup scriptable object asset

This asset should normally not be touched as it is edited using the Sceneless setup window.
It contains the current setup of Sceneless including created scene groups.

### Sceneless_InitSceneManagement scene asset

This scene contains an object, which will not be destroyed on scene switch. This object contains the ScenelessManager, which is used to interact with the Sceneless system during runtime.

This scene should be the first scene in build settings. If any errors occur during setup, please check if this is the case.

## Sceneless settings

The Sceneless settings (in the setup window) include 2 pages:

- The Settings page
	- Enabling and selecting startup scene group, which will load when clicking the play button in the editor.
	- Changing the save/load settings, including default save key and whether or not the game should load on start.
- The Scene Groups page
	- Adding and editing scene groups.
	- When editing scene groups, it is also possible to select which scenes are the active and main camera scenes.

### Active and main camera scenes

Each scene group contains a number of lists of scene references.
Besides this it's also possible to select which of the scene references is the active scene and which is the main camera scene (they can be the same).

The scene selected to be the active scene will be set to be the active scene amongst the scenes loaded, after a scene group has finished loading.
The active scene controls certain aspects in Unity, for instance some lighting settings, which is why it's important to be able to set the correct active scene amongst a list of scenes loaded.

The main camera scene will be the scene which is first loaded, before any other scenes. One of the reasons for doing this is that having no loaded scene with a camera can result in flickering, when switching between scene groups.
By always loading the scene with the main camera first, we minimise this tendancy.

## The ScenelessManager

The ScenelessManager is a class that contains most of the runtime functionality of Sceneless. This class is used to initiate loading of the next scene group (or of a given scene group) and also has other useful utility methods and events.

It works as a singleton, so call it like this: `ScenelessManager.Instance`.

Remember to include the namespace: `Niila.Tools.Sceneless`.

### ScenelessManager events

There are several events that can be hooked into, which will be raised at different points during the switching of scene groups.

It's necessary to unsubscribe to all events that you have subscribed to. It's usually easy to do during the `OnEnable` and `OnDisable` methods in a Unity Monobehaviour. Else use the `OnDestroy` method in a Unity Monobehaviour to remember to unsubscribe.

List of events:

- **`OnBeforeUnloadPrevSceneGroup`** - Called after transition scene has been loaded and before previous scene group is unloaded. This is also called, when no transition scene exists.
- **`OnBeforeLoadNextSceneGroup`** - Called after previous scene group has been unloaded, but before next scene group is loaded.
- **`OnAfterLoadedNextSceneGroup`** - Called after next scene group has been loaded, but before transition scene is unloaded. This is also called, if no transition scene exists.
- **`OnSceneGroupLoaded`** - Called at the very end of the loading sequence, when the transition scene has also been unloaded. This is also called, if no transition scene exists.

If you need to run a method just once when the current scene group has finished loading, you can use the utility method on `ScenelessManager` called `RunOnceOnSceneGroupLoaded(Action<SceneGroup> action)` to run this method.
It will call it immediately, if the scene group is done loading, but will wait if not.

## The Sceneless Graph

Scene groups can be setup to have a specific order of execution. This makes it possible to layout the whole structure of all scenes in the game
and use helper methods on the `GraphTraverser` class (fetched via the `ScenelessManager.Instance`) like `GraphTraverser.GotoNext()`, which returns a `NavigationData` object,
which can be passed to the `ScenelessManager`'s `LoadSceneGroup` method.

### Traversing the graph

Traversing the graph is as easy as using the methods `GoToNext()` and `GoToPrevious()` on the `GraphTraverser` class.

If trying to go to the next node, but there are no more nodes connected, it will wrap around an go to the first node again.

The previous functionality works a bit like an undo function. You can only go to the previous node, when you have left a node before
and it will remember the path you've taken.

**NOTE:**

If you want to jump to a specific scene group remember to use the `SetActiveNode` method on the `GraphTraverser`.
It finds the node that references your scene group and uses its GUID to set the current active node in the graph.

Basically you want to always go through the `GraphTraverser`, when getting the scene groups you want to load.

### Node types

#### Scene Group Node

The basic node, which sets up a basic flow and contains a scene group as input. These are the backbone of the graph.

#### Redirect and RedirectReceiver Nodes

When creating a redirect node a receiver node is also created. These nodes are used to control the flow of the graph.
Use them when you want to structure your nodes better and one node's following node is far away. It redirects the flow between nodes.

## Saving and loading custom data

It is possible to save custom data along with the Sceneless state. This can be done using the normal `SaveState` methods on the Sceneless manager
or it can be done using the `SaveData` method, if you want to save the custom data without saving Sceneless data.

Saving data will always overwrite the previously saved data.

There are equivalent load, delete and exists methods to load, delete and check for data.

This feature uses Unity's `JsonUtility` to serialise/deserialise objects, which means that it is quite restricted.
Properties are, for instance, not allowed and private members need the `SerializeField` attribute in order to be serialised.

Also there are some special work-arounds needed for arrays and collections. Please lookup the Unity documentation if you encounter problems. 

NOTE: It is often useful to load within the `RunOnceOnSceneGroupLoaded` event on the Sceneless manager.

```c#
private void Start()
{
    // Input for the method is the method to run on load.
    ScenelessManager.Instance.RunOnceOnSceneGroupLoaded(OnSceneGroupLoaded);
}

// Because of the above code,
// this will run when the scene group is finished loading (or immediately if it already has finished).
private void OnSceneGroupLoaded(SceneGroup sceneGroup)
{
    // TestSaveData is the type of my save data object.
    var saveData = ScenelessManager.Instance.LoadData<TestSaveData>();
    Debug.Log("Save Data: " + saveData.ToString());
}
```

## FAQ (or the 'something went wrong' section)

### After clicking the setup button in the Sceneless setup window, I don't get access to settings

Make sure that the two assets mentioned in the Setup section are actually present. The ScenelessSetup scriptable object asset needs to be in a resource folder under a Sceneless folder.

Make sure that the Sceneless_InitSceneManagement scene is in build settings and that it is the first scene there.

### Is it possible to bulk rename scene groups?

It is possible to bulk rename scene groups based on a pattern using the new Smart Renamer.
You can rename all scenes or only selected scenes as you see fit.

See the patterns available on the Scene Groups page in the Sceneless window.

### My custom save data is not saved/loaded correctly

Please check that you don't use C# properties in your classes (only fields) and that all private fields have the
`SerializeField` attribute.

If you are trying to save arrays or collections, look up the Unity documentation for
`JsonUtility` on how to handle those as they are not supported without workarounds.
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Niila.Tools.DoozyExtensions;
using TMPro;

public class AnnouncementsManager : MonoBehaviour
{
    [SerializeField]
    private TagCollector _tagCollector;

    private TextMeshProUGUI _announcementPickUpText;
    private Animator _announcementPickUpAnimator;

    private TextMeshProUGUI _announcementText;
    private Animator _announcementTextAnimator;
    private float _announcementPickUpTimer;

    [SerializeField]
    [Header("Pick Up Wait Time for Stacking")]
    public float PickUpWaitThreshold = 0.5f;


    private TextMeshProUGUI _counterTypeText;
    private TextMeshProUGUI _counterAmountText;
    private Animator _counterAnimator;
    private float _counterTimer;

    public static AnnouncementsManager Instance;

    [SerializeField]
    [Header("Counter Wait Time for Stacking")]
    public float CounterWaitThreshold = 0.5f;

    private void OnValidate()
    {
        _tagCollector = GetComponent<TagCollector>();
    }

    private void Awake()
    {
        if(Instance != null)
        {
            if(Instance != this)
            {
                Destroy(gameObject);
            }
            return;
        }

        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        _tagCollector?.FindTags();

        _announcementTextAnimator = _tagCollector.GetAnimator("AnnouncementTextAnimator");
    
        _announcementPickUpAnimator = _tagCollector.GetAnimator("AnnouncementPickUpAnimator");

        _counterAnimator = _tagCollector.GetAnimator("CounterAnimator");
    }

    public void ShowAnnouncementText(string text)
    {
        if (text == string.Empty)
            return;

        _tagCollector.SetLabel("AnnouncementText", text);
        _announcementTextAnimator.SetTrigger("In");
    }

    public void ShowAnnouncementPickUp(float amount)
    {
        _tagCollector.SetLabel("AnnouncementPickUpAmount", "+" + amount);

        // check if the animation is already playing
        if(_announcementPickUpAnimator.GetBool("Show"))
        {
            _announcementPickUpAnimator.SetTrigger("Punch");
            _announcementPickUpTimer = _announcementPickUpAnimator.GetCurrentAnimatorStateInfo(1).length + PickUpWaitThreshold;
        }
        else // if the animation is not playing
        {
            _announcementPickUpAnimator.SetBool("Show", true);
            StartCoroutine(WaitForAnnouncementAnimation());
        }


    }

    IEnumerator WaitForAnnouncementAnimation()
    {
        _announcementPickUpTimer = _announcementPickUpAnimator.GetCurrentAnimatorStateInfo(0).length + PickUpWaitThreshold;

        while (_announcementPickUpTimer > 0)
        {
            _announcementPickUpTimer -= Time.deltaTime;
            yield return null;
        }

        _announcementPickUpAnimator.SetBool("Show", false);
    }

    public void ShowCounter(string type, float amount)
    {
        _tagCollector.SetLabel("CounterTypeText", type);
        _tagCollector.SetLabel("CounterAmountText", "x" + amount);

        // check if the animation is already playing
        if (_counterAnimator.GetBool("Show"))
        {
            _counterAnimator.SetTrigger("Punch");
            _counterTimer = _counterAnimator.GetCurrentAnimatorStateInfo(1).length + CounterWaitThreshold;
        }
        else // if the animation is not playing
        {
            _counterAnimator.SetBool("Show", true);
            StartCoroutine(WaitForCounterAnimation());
        }


    }

    IEnumerator WaitForCounterAnimation()
    {
        _counterTimer = _counterAnimator.GetCurrentAnimatorStateInfo(0).length + CounterWaitThreshold;

        while (_counterTimer > 0)
        {
            _counterTimer -= Time.deltaTime;
            yield return null;
        }

        _counterAnimator.SetBool("Show", false);
    }
}

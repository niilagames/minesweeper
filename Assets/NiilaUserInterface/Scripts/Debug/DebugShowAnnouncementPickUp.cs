﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugShowAnnouncementPickUp : MonoBehaviour
{
    private float _amount = 0;
    private bool _show;

    public AnnouncementsManager AnnouncementsManager;
    public Animator PickUpAnimator;

    void Start()
    {
    }

    public void ShowAnnouncementPickUp()
    {
        _show = PickUpAnimator.GetBool("Show");
        if (_show == false)
        {
            _amount = 0f;
        }

        _amount++;
        AnnouncementsManager.ShowAnnouncementPickUp(_amount);
    }
}

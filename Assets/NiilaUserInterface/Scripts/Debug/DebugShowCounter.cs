﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugShowCounter : MonoBehaviour
{
    private float _amount = 0;
    private bool _show;

    [SerializeField]
    [Header("Counter Type")]
    private string type = "Flips";

    public AnnouncementsManager AnnouncementsManager;
    public Animator _counterAnimator;

    void Start()
    {
    }

    public void ShowCounter()
    {
        _show = _counterAnimator.GetBool("Show");
        if (_show == false)
        {
            _amount = 0f;
        }

        _amount++;
        AnnouncementsManager.ShowCounter(type, _amount);
    }
}

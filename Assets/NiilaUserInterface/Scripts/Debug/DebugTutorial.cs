﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugTutorial : MonoBehaviour
{
    public Animator Animator;

    public void ShowTutorial()
    {
        Animator.SetBool("Show", true);
    }

    public void HideTutorial()
    {
        Animator.SetBool("Show", false);
    }
}

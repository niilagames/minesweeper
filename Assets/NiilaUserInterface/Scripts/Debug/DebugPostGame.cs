﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine.Progress;

public class DebugPostGame : MonoBehaviour
{
    public Progressor Star01;
    public Progressor Star02;
    public Progressor Star03;

    public void ShowStars()
    {
        StartCoroutine(AnimateStars());
    }

    public void HideStars()
    {
        Star01.SetValue(0f);
        Star02.SetValue(0f);
        Star03.SetValue(0f);
    }

    IEnumerator AnimateStars()
    {
        Star01.SetValue(1f);

        yield return new WaitForSeconds(0.25f);

        Star02.SetValue(1f);

        yield return new WaitForSeconds(0.25f);

        Star03.SetValue(1f);
    }

}

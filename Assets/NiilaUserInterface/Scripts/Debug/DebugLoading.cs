﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Doozy.Engine;

public class DebugLoading : MonoBehaviour
{
    public Image LoadingFill;

    public void Load()
    {
        LoadingFill.fillAmount = 0f;
        LoadingFill.DOFillAmount(1f, 1f)
            .OnComplete(() => 
            {
                GameEventMessage.SendEvent("SceneLoaded");
                GameEventMessage.SendEvent("SceneReloaded");
            });
    }

}

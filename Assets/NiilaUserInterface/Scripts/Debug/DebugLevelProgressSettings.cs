﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine.Progress;
using DG.Tweening;
using TMPro;

public class DebugLevelProgressSettings : MonoBehaviour
{

    public ProgressTargetTextMeshPro Amount;
    public TextMeshProUGUI Total;

    public void ChangeMultiplier(float amount)
    {
        Amount.Multiplier = amount;
        Total.text = ""+amount;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillOnTrigger : MonoBehaviour
{
    #region Declarations

    #endregion

    #region Methods

    #endregion

    #region Callbacks
    private void Update()
    {
        if (GameManager.Instance.Player != null)
        {
            Vector3 pos = GameManager.Instance.Player.transform.position;
            pos.y = transform.position.y;
            transform.position = pos;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Controller controller = other.GetComponent<Controller>();
        if (controller != null)
        {
            EventManager.OnLevelLost();
        }
    }
    #endregion


}

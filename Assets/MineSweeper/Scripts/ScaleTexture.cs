﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ScaleTexture : MonoBehaviour
{
    #region Declarations
    public Material mat;
    public float scaleFactor = 2f;
    #endregion

    #region Methods
    
    #endregion

    #region Callbacks
    void Awake()
    {
        if (mat == null)
        {
            mat = GetComponent<MeshRenderer>().material;
        }
        mat.mainTextureScale = new Vector2(transform.localScale.x * scaleFactor, transform.localScale.z * scaleFactor);
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.hasChanged)
        {
            mat.mainTextureScale = new Vector2(transform.localScale.x * scaleFactor, transform.localScale.z * scaleFactor);
        }
    }
    #endregion
}

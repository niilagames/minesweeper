﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scan : MonoBehaviour
{
    #region Declarations
    public bool scanning;
    public float onDuration = 2f;
    public float offDuration = 2f;
    public List<Scannable> scannables = new List<Scannable>();

    //[SerializeField, GetSet("distance")]
    //float _distance = 60f;
    //public float distance
    //{
    //    get
    //    {
    //        return _distance;
    //    }
    //    set
    //    {
    //        _distance = value;
    //        if (Application.isPlaying) EventManager.OnConeSizeChanged();
    //    }
    //}
    //[SerializeField, GetSet("angle")]
    //float _angle = 60f;
    //public float angle
    //{
    //    get
    //    {
    //        return _angle;
    //    }
    //    set
    //    {
    //        _angle = value;
    //        if (Application.isPlaying) EventManager.OnConeSizeChanged();
    //    }
    //}
    public LayerMask layersToScan;
    public Material testMat;
    public List<Scannable> scannedObjects;

    //[Header("Visualization")]
    //[Tooltip("The number of segments with which to draw the cone of vision.")]
    //public int coneResolution = 10;
    //public MeshFilter scannerMeshFilter;
    //public MeshRenderer scannerMeshRenderer;

    Controller controller;
    Vector3 scanOrigin;
    float scanOriginOffsetY;

    #endregion

    #region Callbacks

    private void OnEnable()
    {
        EventManager.OnScannedBegin += DiscoverObject;
        EventManager.OnScannedEnd += ForgetObject;
        EventManager.OnRegister += Register;
        EventManager.OnDeRegister += DeRegister;

        //EventManager.OnConeSizeChanged += UpdateCone;
    }

    private void OnDisable()
    {
        EventManager.OnScannedBegin -= DiscoverObject;
        EventManager.OnScannedEnd -= ForgetObject;
        EventManager.OnRegister -= Register;
        EventManager.OnDeRegister -= DeRegister;


        //EventManager.OnConeSizeChanged -= UpdateCone;

    }

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<Controller>();
        //scanOriginOffsetY = controller.GetComponent<Collider>().bounds.extents.y;
        //scanOrigin = new Vector3(transform.position.x, 0, transform.position.z);
        //scannerMeshFilter.sharedMesh = GenerateCone(_distance, _angle, new Vector3(0, 0.1f - scanOriginOffsetY, 0), coneResolution);
        StartCoroutine(ScanCycle(onDuration, offDuration));
    }

    // Update is called once per frame
    void Update()
    {
        //if (scanning)
        //{
        //    //scanOrigin = new Vector3(transform.position.x, transform.position.y - scanOriginOffsetY, transform.position.z);

        //    //Collider[] hits = Physics.OverlapSphere(scanOrigin, _distance, layersToScan);
        //    //List<Scannable> hitObjects = new List<Scannable>();
        //    //for (int i = 0; i < hitObjects.Count; i++)
        //    //{
        //    //    hitObjects[i] = hits[i].GetComponent<Scannable>();
        //    //}

        //    //for (int i = 0; i < scannedObjects.Count; i++)
        //    //{
        //    //    if (!hitObjects.Contains(scannedObjects[i]))
        //    //    {
        //    //        EventManager.OnScannedEnd(scannedObjects[i]);
        //    //        scannedObjects.Remove(scannedObjects[i]);
        //    //    }
        //    //}

        //    //if (hits.Length > 0)
        //    //{
        //    //    foreach (Collider hit in hits)
        //    //    {
        //    //        Scannable scanTarget = hit.GetComponent<Scannable>();
        //    //        if (scanTarget != null && isWithinScanRange(hit.ClosestPoint(transform.position), scanOrigin, transform.forward, _angle))
        //    //        {
        //    //            EventManager.OnScannedBegin(scanTarget);
        //    //            scannedObjects.Add(scanTarget);
        //    //        }
        //    //    }
        //    //}
        //}
        //else
        //{
        //    for (int i = 0; i < scannables.Count; i++)
        //    {
        //        Scannable currentScannable = scannables[i];
        //        if (!currentScannable.beingScanned)
        //        {
        //            currentScannable.beingScanned = true;
        //            EventManager.OnScannedBegin(currentScannable);
        //        }
        //    }
        //}

    }

    private void OnDrawGizmos()
    {
        //Gizmos.DrawWireSphere(scanOrigin, distance);
        
        //for (int i = 0; i < coneResolution+1; i++)
        //{
        //    Vector3 origin = new Vector3(transform.position.x, transform.position.y-scanOriginOffsetY, transform.position.z);
        //    float angleDiff = i * (_angle / coneResolution);
        //    Vector3 targetPos = origin;
        //    targetPos += transform.forward * _distance;
        //    Vector3 dir = targetPos - origin;
        //    targetPos = Quaternion.Euler(0, angleDiff - (_angle / 2), 0) * (dir) + origin;

        //    //coneLineTerminus.z += distance * Mathf.Cos((angle/2 - angleDiff)*Mathf.Deg2Rad);
        //    //coneLineTerminus.x += distance * Mathf.Sin((angle/2 - angleDiff)*Mathf.Deg2Rad);
        //    Gizmos.color = Color.yellow;
        //    Gizmos.DrawLine(origin, targetPos);
        //}
    }
    #endregion

    #region Methods

    bool isWithinScanRange(Vector3 position, Vector3 _scanOrigin, Vector3 scanDirection, float scanAngle)
    {
        Vector3 targetDir = position - _scanOrigin;
        float angleBetween = Mathf.Abs(Vector3.SignedAngle(targetDir, scanDirection, Vector3.up));
        return (angleBetween <= scanAngle / 2);
    }

    IEnumerator ScanCycle(float onDuration, float offDuration)
    {
        while (true)
        {
            TurnOn();
            yield return new WaitForSeconds(onDuration);
            TurnOff();
            yield return new WaitForSeconds(offDuration);
        }
    }

    void TurnOn()
    {
        scanning = true;
        //scannerMeshRenderer.enabled = true;
        EventManager.ScanStart(this);
        for (int i = 0; i < scannables.Count; i++)
        {
            Scannable currentScannable = scannables[i];
            EventManager.OnScannedBegin(currentScannable);
        }
    }

    void TurnOff()
    {
        scanning = false;
        EventManager.ScanEnd(this);
        //scannerMeshRenderer.enabled = false;
        //for (int i = 0; i < scannedObjects.Count; i++)
        //{
        //    EventManager.OnScannedEnd(scannedObjects[i]);
        //    scannedObjects.Remove(scannedObjects[i]);
        //}
        for (int i = 0; i < scannables.Count; i++)
        {
            Scannable currentScannable = scannables[i];
            EventManager.OnScannedEnd(currentScannable);
        }
    }

    void DiscoverObject(Scannable target)
    {
        target.OnScannedBegin();
    }

    void ForgetObject(Scannable target)
    {
        target.OnScannedEnd();
    }

    void Register(Scannable target)
    {
        scannables.Add(target);
    }
    void DeRegister(Scannable target)
    {
        scannables.Remove(target);
    }

    //void UpdateCone()
    //{
    //    scannerMeshFilter.sharedMesh = GenerateCone(_distance, _angle, new Vector3(0, 0.1f - scanOriginOffsetY, 0), coneResolution);
    //}

    //Mesh GenerateCone(float _distance, float _angle, Vector3 origin, int resolution)
    //{
    //    Mesh mesh = new Mesh();

    //    Vector3[] vertices = new Vector3[resolution + 2];
    //    int[] tris = new int[resolution * 3];

    //    vertices[0] = origin;
    //    for (int i = 1; i < vertices.Length; i++)
    //    {
    //        float angleDiff = (i - 1) * (this._angle / resolution);
    //        Vector3 targetPos = origin;
    //        targetPos += Vector3.forward * this._distance;
    //        Vector3 dir = targetPos - origin;
    //        vertices[i] = Quaternion.Euler(0, angleDiff - (this._angle / 2), 0) * (dir) + origin;
    //    }

    //    for (int v = 0; v+2 < vertices.Length; v++)
    //    {
    //        int vi = v * 3;

    //        tris[vi] = 0;
    //        tris[vi + 1] = v +1;
    //        tris[vi + 2] = v + 2;
    //    }

    //    mesh.vertices = vertices;
    //    mesh.triangles = tris;

    //    return mesh;
    //}

    #endregion
}

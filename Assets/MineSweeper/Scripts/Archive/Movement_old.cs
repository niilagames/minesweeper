﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement_old : MonoBehaviour
{
    // TO DO: When that's working, make sure that mines work by turning on RB on collision and off on grounded.

    #region Declarations
    public float speed = 2f;
    public float gravity = 9.8f;
    public bool ground;
    public Collider rbCollider;

    Rigidbody rb;
    CharacterController controller;
    [SerializeField]
    bool canMove = true;
    Vector2 mousePos;
    [SerializeField]
    Vector3 direction;
    float rotationAmount;
    Vector2 clickPoint;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        controller = GetComponent<CharacterController>();
        rb.detectCollisions = false;
    }

    // Update is called once per frame
    void Update()
    {
        ground = controller.isGrounded;

        if (Input.GetMouseButton(0))
        {
            if (Input.GetMouseButtonDown(0))
            {
                clickPoint = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            }

            mousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            rotationAmount = Mathf.Clamp(mousePos.x - clickPoint.x, -.5f, .5f);
            transform.Rotate(transform.up, rotationAmount * Time.deltaTime);

            direction = transform.forward * speed;
        }
        else
        {
            direction = Vector3.zero;
        }

        if (controller.isGrounded)
        {

            if (Input.GetMouseButton(0))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    clickPoint = Camera.main.ScreenToViewportPoint(Input.mousePosition);
                }

                //Debug.Log(clickPoint);
                mousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
                rotationAmount = (mousePos.x - clickPoint.x) * 360;
                transform.Rotate(transform.up, rotationAmount * Time.deltaTime);
                direction = transform.forward * speed;
            }
        }

        direction.y -= gravity;
        if (canMove) controller.Move(direction * Time.deltaTime);
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.tag == "Mine")
        {
            rb.isKinematic = false;
            controller.detectCollisions = false;
            rbCollider.enabled = true;
            rb.detectCollisions = true;
            canMove = false;
            rb.AddForce((transform.position - hit.transform.position).normalized * 7 + new Vector3(0, 7, 0), ForceMode.Impulse);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            controller.detectCollisions = true;
            rbCollider.enabled = false;
            rb.detectCollisions = false;
            canMove = true;
            rb.isKinematic = true;
        }
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // TO DO: Consider not doing rigidbody knockback...

    #region Declarations
    public float speed = 2f;
    public float gravity = 9.8f;
    public bool ground;
    public Collider rbCollider;
    
    Rigidbody rb;
    //CharacterController controller;
    //[SerializeField]
    //bool canMove;
    Vector2 mousePos;
    [SerializeField]
    Vector3 direction;
    float rotationAmount;
    Vector2 clickPoint;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //controller = GetComponent<CharacterController>();
        rb.detectCollisions = true;
    }

    // Update is called once per frame
    void Update()
    {
        //ground = controller.isGrounded;
        //if (isGrounded()) canMove = true;
        if (true)
        {
            if (Input.GetMouseButton(0))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    clickPoint = Camera.main.ScreenToViewportPoint(Input.mousePosition);
                }

                mousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
                rotationAmount = Mathf.Clamp(mousePos.x - clickPoint.x, -.5f, .5f);
                transform.Rotate(transform.up, rotationAmount * 360 * Time.deltaTime);

                direction = transform.forward * speed;
            }
            else
            {
                direction = Vector3.zero;
            }
        }
        

        //direction.y -= gravity;
        //if (canMove) transform.Translate(direction * Time.deltaTime);
    }

    private void FixedUpdate()
    {
        rb.AddForce(direction-rb.velocity, ForceMode.VelocityChange);
    }



    private void OnCollisionEnter(Collision hit)
    {
        Debug.Log("enter");
        if (hit.gameObject.tag == "Mine")
        {
            rb.isKinematic = false;
            //rbCollider.enabled = true;
            //rb.detectCollisions = true;
            ////controller.enabled = false;
            //controller.detectCollisions = false;
            //canMove = false;
            rb.AddForce((transform.position - hit.transform.position).normalized * 30 + new Vector3(0, 30, 0), ForceMode.Impulse);

        }
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.tag == "Ground")
    //    {
    //        //rbCollider.enabled = false;
    //        //rb.detectCollisions = false;
    //        //rb.isKinematic = true;
    //        ////controller.enabled = true;
    //        //controller.detectCollisions = true;
    //        //controller.
    //        //canMove = true;
    //    }
    //}
}


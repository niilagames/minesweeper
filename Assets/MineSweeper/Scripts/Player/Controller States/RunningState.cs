﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningState : ControllerState
{
    protected float speed;
    protected float rotationSpeed;
    float rotationAmount;
    Vector3 dir;

    public RunningState(Controller controller, StateManager stateManager) : base(controller, stateManager)
    {

    }

    public override void Enter()
    {
        base.Enter();
        speed = controller.speed;
        rotationSpeed = controller.rotationSpeed;

        controller.SetAnimatorValue("Running", true);
    }

    public override void HandleInput()
    {
        base.HandleInput();
        if (Input.GetMouseButton(0))
        {
            rotationAmount = GetDragAmountX();
        }
        if (Input.GetMouseButtonUp(0))
        {
            rotationAmount = 0;
        }
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (controller.checkForGround && !controller.grounded && !controller.wasGroundedLastFrame)
        {
            stateManager.ChangeState(controller.gliding);
        }
    }

    public override void MovementUpdate()
    {
        base.MovementUpdate();
        controller.Rotate(rotationAmount * rotationSpeed * Time.deltaTime);
        dir = (Vector3.forward) * speed;
        controller.Move(dir * Time.deltaTime);
    }

    public override void Exit()
    {
        base.Exit();
        controller.SetAnimatorValue("Running", false);

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingState : ControllerState
{
    protected float speed;
    protected float rotationSpeed;
    protected float gravity;
    float currentGravity;
    float rotationAmount;
    Vector3 dir;

    public FallingState(Controller controller, StateManager stateManager) : base(controller, stateManager)
    {

    }

    public override void Enter()
    {
        base.Enter();
        speed = controller.speed;
        rotationSpeed = controller.rotationSpeed;
        gravity = controller.gravity;
        currentGravity = controller.fallingSpeed;
        //Debug.Break();
    }

    public override void HandleInput()
    {
        base.HandleInput();
        if (Input.GetMouseButton(0))
        {
            rotationAmount = GetDragAmountX();
        }
        if (Input.GetMouseButtonUp(0))
        {
            rotationAmount = 0;
        }
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (controller.checkForGround && controller.grounded)
        {
            stateManager.ChangeState(controller.running);
        }
    }

    public override void MovementUpdate()
    {
        base.MovementUpdate();
        //controller.Rotate(rotationAmount * rotationSpeed * Time.deltaTime);
        if (controller.transform.position.y > -500)
        {
            dir = (Vector3.forward) * speed;
            currentGravity += gravity;
            dir.y -= currentGravity;
            controller.Move(dir * Time.deltaTime);
        }
    }
}

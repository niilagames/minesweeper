﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingState : ControllerState
{
    protected float speed;
    protected float rotationSpeed;
    float rotationAmount;
    AnimationCurve jumpTrajectory;
    public float jumpHeight;
    public float jumpDistance;
    public Vector3 initialPosition;
    public float timer;
    Vector3 previousPos;
    float distanceTraversed;
    float currentFallingSpeed;
    Vector3 impact;
    Vector3 dir;

    public JumpingState(Controller controller, StateManager stateManager) : base(controller, stateManager)
    {

    }

    public override void Enter()
    {
        base.Enter();
        //Debug.Log("Entered jumping state.");
        timer = 0f;
        speed = controller.speed;
        rotationSpeed = controller.rotationSpeed;
        controller.checkForGround = false;

        initialPosition = controller.transform.position;
        impact = controller.impact;
        jumpTrajectory = controller.jumpCurve;
        jumpHeight = controller.jumpHeight;
        jumpDistance = controller.jumpDistance;
        distanceTraversed = 0;
        currentFallingSpeed = 0;
        previousPos = controller.transform.position;
        previousPos.y = 0;

        controller.transform.forward = impact;

        controller.SetAnimatorValue("Jumping", true);

    }

    public override void HandleInput()
    {
        base.HandleInput();
        if (Input.GetMouseButton(0))
        {
            rotationAmount = GetDragAmountX();
        }
        if (Input.GetMouseButtonUp(0))
        {
            rotationAmount = 0;
        }
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        timer += Time.deltaTime;

        if (controller.checkForGround && controller.grounded)
        {
            stateManager.ChangeState(controller.running);
        }

        Vector3 newPos = controller.transform.position;
        newPos.y = 0;
        distanceTraversed += Vector3.Distance(previousPos, newPos);
        previousPos = newPos;
        if (timer >= (jumpDistance / speed))
        {
            //Debug.Log("Finished jumping after " + timer + "seconds and traveled " + distanceTraversed + " units");
            if (controller.checkForGround && controller.grounded)
            {
                stateManager.ChangeState(controller.running);
            }
            else
            {
                stateManager.ChangeState(controller.gliding);
            }
        }
    }

    public override void MovementUpdate()
    {
        base.MovementUpdate();
        controller.Rotate(rotationAmount * rotationSpeed * Time.deltaTime);
        dir = (Vector3.forward) * speed * Time.deltaTime;
        //timer += Time.deltaTime;
        dir.y += Mathf.Lerp(initialPosition.y, initialPosition.y + jumpHeight, jumpTrajectory.Evaluate(timer / (jumpDistance / speed))) - controller.transform.position.y;
        controller.Move(dir);
    }

    public override void Exit()
    {
        base.Exit();
        controller.fallingSpeed = currentFallingSpeed;
        controller.SetAnimatorValue("Jumping", false);

    }

    public void UpdateJumpParameters(float _jumpDistance, float _jumpHeight)
    {
        jumpDistance = _jumpDistance;
        jumpHeight = _jumpHeight;
    }

    //public void UpdateJumpParameters(float _jumpDistance, float _jumpHeight, AnimationCurve _jumpTrajectory)
    //{
    //    jumpDistance = _jumpDistance;
    //    jumpHeight = _jumpHeight;
    //    jumpTrajectory = _jumpTrajectory;
    //}
}

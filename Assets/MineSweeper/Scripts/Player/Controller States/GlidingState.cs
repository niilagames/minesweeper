﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlidingState : ControllerState
{
    #region Declarations

    protected float speed;
    protected float rotationSpeed;
    float gravity;
    float maxGravity;
    float timer;
    float lerpTime;
    float rotationAmount;
    Vector3 dir;
    Pickup balloonController;
    float balloonPopInterval;

    #endregion

    public GlidingState(Controller controller, StateManager stateManager) : base(controller, stateManager)
    {

    }

    public override void Enter()
    {
        base.Enter();
        speed = controller.speed;
        rotationSpeed = controller.rotationSpeed;
        gravity = 0;
        maxGravity = controller.gravity;
        timer = 0;
        lerpTime = .8f;
        balloonController = controller.GetComponentInChildren<Pickup>();
        balloonPopInterval = balloonController.balloonPopInterval;

        balloonController.StartCoroutine("PopBalloonsAtInterval", balloonPopInterval);

        controller.SetAnimatorValue("Gliding", true);
    }

    public override void HandleInput()
    {
        base.HandleInput();
        if (Input.GetMouseButton(0))
        {
            rotationAmount = GetDragAmountX();
        }
        if (Input.GetMouseButtonUp(0))
        {
            rotationAmount = 0;
        }
    }

    public override void LogicUpdate()
    {
        timer += Time.deltaTime;
        if (gravity < maxGravity)
        {
            gravity = Mathf.Lerp(0, maxGravity, timer / lerpTime);
        }

        base.LogicUpdate();
        if (controller.checkForGround && controller.grounded)
        {
            stateManager.ChangeState(controller.running);
        }
    }

    public override void MovementUpdate()
    {
        base.MovementUpdate();
        controller.Rotate(rotationAmount * rotationSpeed * Time.deltaTime);
        dir = (Vector3.forward) * speed;
        dir.y -= gravity;
        controller.Move(dir * Time.deltaTime);
    }

    public override void Exit()
    {
        base.Exit();
        controller.SetAnimatorValue("Gliding", false);
        balloonController.StopCoroutine("PopBalloonsAtInterval");
    }

}

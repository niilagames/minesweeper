﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnockbackState : ControllerState
{
    protected float speed;
    protected float rotationSpeed;
    float rotationAmount;
    AnimationCurve jumpTrajectory;
    float height;
    float distance;
    Vector3 initialPosition;
    float timer;
    Vector3 impact;
    Vector3 dir;

    public KnockbackState(Controller controller, StateManager stateManager) : base(controller, stateManager)
    {

    }

    public override void Enter()
    {
        base.Enter();
        timer = 0f;
        speed = controller.speed;
        rotationSpeed = controller.rotationSpeed;

        initialPosition = controller.transform.position;
        impact = controller.impact;
        jumpTrajectory = controller.jumpCurve;
        height = controller.knockbackHeight;
        distance = controller.knockbackDistance;
    }

    public override void HandleInput()
    {
        base.HandleInput();
        if (Input.GetMouseButton(0))
        {
            rotationAmount = GetDragAmountX();
        }
        if (Input.GetMouseButtonUp(0))
        {
            rotationAmount = 0;
        }
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        timer += Time.deltaTime;
        if (timer >= (distance / speed))
        {
            if (controller.checkForGround && controller.grounded)
            {
                stateManager.ChangeState(controller.running);
            }
            else
            {
                stateManager.ChangeState(controller.falling);
            }
        }
    }

    public override void MovementUpdate()
    {
        base.MovementUpdate();
        controller.Rotate(rotationAmount * rotationSpeed * Time.deltaTime);
        dir = impact * speed * Time.deltaTime;
        dir.y += Mathf.Lerp(initialPosition.y, initialPosition.y + height, jumpTrajectory.Evaluate(timer / (distance / speed))) - controller.transform.position.y;
        controller.Move(dir);
    }

    public override void Exit()
    {
        base.Exit();
        controller.StartCoroutine(controller.Stun(controller.stunTime));
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ControllerState
{
    protected Controller controller;
    protected StateManager stateManager;

    protected ControllerState(Controller controller, StateManager stateManager)
    {
        this.controller = controller;
        this.stateManager = stateManager;
    }


    public float GetDragAmountX()
    {
        float dragAmountX = Input.GetAxis("Mouse X");
        if (Input.touchCount > 0)
        {
            dragAmountX = Input.touches[0].deltaPosition.x * .6f;
        }

        return dragAmountX;
    }

    /// <summary>
    /// Enter is called when a controller switches to this state.
    /// </summary>
    public virtual void Enter()
    {
        
    }

    // Place any input handling here to be used in other methods. Usually called in Update.
    public virtual void HandleInput()
    {

    }

    // Place logic handling here, such as most logic for state switching.
    public virtual void LogicUpdate()
    {

    }

    // Use this for non-physics movement.
    public virtual void MovementUpdate()
    {

    }

    /// <summary>
    /// Exit is called when a controller leaves this state.
    /// </summary>
    public virtual void Exit()
    {

    }

}

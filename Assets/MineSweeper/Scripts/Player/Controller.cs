﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    #region Declarations
    [HideInInspector]
    public StateManager sm;
    [HideInInspector]
    public RunningState running;
    [HideInInspector]
    public FallingState falling;
    [HideInInspector]
    public JumpingState jumping;
    [HideInInspector]
    public KnockbackState knockback;
    [HideInInspector]
    public GlidingState gliding;

    [SerializeField, Tooltip("The current state of this controller.")]
    string currentState;

    public float speed = 1f;
    public float runningSpeed;
    public float gravity = 9.81f;
    public float rotationSpeed = 20;
    public bool checkForGround = true;
    public LayerMask whatIsGround;
    [Header("Jump attributes")]
    public float jumpHeight = 8f;
    public float jumpDistance = 6f;
    [Tooltip("The curve to follow over the course of the jump.")]
    public AnimationCurve jumpCurve;
    [HideInInspector]
    public Vector3 impact = Vector3.zero;
    [Header("Stun attributes")]
    public float knockbackHeight = 2f;
    public float knockbackDistance = 2f;
    public float stunTime = .5f;
    [HideInInspector]
    public float distToGround;
    public float fallingSpeed;
    public bool canMove;
    [SerializeField, GetSet("Grounded")]
    public bool grounded;
    bool Grounded
    {
        get { 
            grounded =  isGrounded();
            return grounded;
        }
        set { grounded = value; }
    }
    public bool wasGroundedLastFrame;
    [SerializeField]
    public CapsuleCollider coll;
    [SerializeField]
    Animator anim;

    [Header("Debug")]
    List<Vector3> ungroundedPositions = new List<Vector3>(); // List of positions to draw jump trajectories.
    #endregion

    private void OnEnable()
    {
        EventManager.OnLevelLost += OnLost;
    }
    private void OnDisable()
    {
        EventManager.OnLevelLost -= OnLost;
    }
    private void Awake()
    {
        coll = GetComponent<CapsuleCollider>();
        distToGround = coll.bounds.extents.y;
    }

    void Start()
    {
        sm = new StateManager();

        running = new RunningState(this, sm);
        falling = new FallingState(this, sm);
        jumping = new JumpingState(this, sm);
        knockback = new KnockbackState(this, sm);
        gliding = new GlidingState(this, sm);

        sm.Initialize(running); // Set starting state.

        if (anim == null)
        {
            anim = GetComponent<Animator>();
        }
    }

    #region Callbacks

    void Update()
    {
        grounded = Grounded;

        currentState = sm.CurrentState.ToString(); // Display current state in inspector.

        sm.CurrentState.HandleInput();

        sm.CurrentState.LogicUpdate();

        sm.CurrentState.MovementUpdate();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            impact = transform.forward;
            sm.ChangeState(jumping);
        }

        wasGroundedLastFrame = grounded;
    }

    public void Move(Vector3 direction)
    {
        if (canMove) transform.Translate(direction);
    }

    public void Rotate(float angle)
    {
        transform.Rotate(0, angle, 0);
    }

    void OnLost()
    {
        sm.ChangeState(falling);
    }

    private void OnCollisionEnter(Collision collision)
    {
        
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.GetComponent<ForceApplier>() != null)
        {
            checkForGround = true;
        }
    }

    private void OnDrawGizmos()
    {
        // Draw ground ray
        //Debug.DrawRay(transform.position, -Vector3.up * (distToGround + 0.01f));

        if (!grounded)
        {
            if (Time.frameCount % 1 == 0)
            {
                Gizmos.color = Color.red;
                ungroundedPositions.Add(transform.position);
            }
            for (int i = 0; i < ungroundedPositions.Count - 1; i++)
            {
                Gizmos.DrawLine(ungroundedPositions[i], ungroundedPositions[i + 1]);
            }
        }
        else
        {
            ungroundedPositions.Clear();
        }

        if (Application.isPlaying)
        {
            if (!grounded)
            {
                float distance = jumpDistance - Mathf.Lerp(0, jumpDistance, jumping.timer / (jumpDistance / speed));
                Vector3 landPosition = transform.position + transform.forward * distance;
                landPosition.y = jumping.initialPosition.y;
                Gizmos.color = Color.yellow;
                Gizmos.DrawSphere(landPosition, .5f);
            }
            Gizmos.DrawWireSphere(transform.position + Vector3.up * (coll.height/2 + Physics.defaultContactOffset - coll.height + coll.radius), coll.radius + Physics.defaultContactOffset);
            //Gizmos.DrawWireSphere(transform.position + (Vector3.up * Physics.defaultContactOffset) - Vector3.up * (coll.height/2-coll.radius), coll.radius - Physics.defaultContactOffset);
        }
    }

    #endregion

    #region Methods
    bool isGrounded()
    {
        bool wasGrounded = grounded;
        Ray ray = new Ray(transform.position + Vector3.up * (coll.height/2 + Physics.defaultContactOffset), Vector3.down);
        float radius = coll.radius + Physics.defaultContactOffset;
        RaycastHit hit;
        if (Physics.SphereCast(ray, radius, out hit, coll.height-coll.radius, whatIsGround.value))
        {
            if (!wasGrounded)
            {
                //Debug.Log("Grounded in this frame. Hit object: " + hit.collider.gameObject.name);
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Changes the controller's state to JumpState with the controller's own jump direction, distance, and height.
    /// </summary>
    public void StartJump()
    {
        impact = transform.forward;
        sm.ChangeState(jumping);
    }

    /// <summary>
    /// Changes the controller's state to JumpState with the controller's own jump distance and height and the specified jump direction.
    /// </summary>
    /// <param name="forceDir">The direction that the controller's forward will be aligned to.</param>
    public void StartJump(Vector3 forceDir)
    {
        forceDir.y = 0;
        impact = forceDir;
        sm.ChangeState(jumping);
    }

    /// <summary>
    /// Changes the controller's state to JumpState with the force applier's jump direction, distance, and height.
    /// </summary>
    /// <param name="forceApplier">The force applier containing a jump direction, distance, and height.</param>
    public void StartJump(ForceApplier forceApplier)
    {
        Vector3 forceDir = forceApplier.GetForceDirection(transform);
        forceDir.y = 0;
        impact = forceDir;
        //jumpDistance = forceApplier.jumpDistance;
        //jumpHeight = forceApplier.jumpHeight;
        sm.ChangeState(jumping);
        jumping.UpdateJumpParameters(forceApplier.jumpDistance, forceApplier.jumpHeight);
    }

    public void StartStun(Vector3 forceDir)
    {
        forceDir.y = 0;
        impact = forceDir;
        sm.ChangeState(knockback);
    }

    public IEnumerator Stun(float time)
    {
        canMove = false;
        yield return new WaitForSeconds(time);
        canMove = true;
    }

    /// <summary>
    /// Changes a named boolean animator parameter to the specified value.
    /// </summary>
    /// <param name="name">The name of the animator parameter to change.</param>
    /// <param name="value">The new value of the animator parameter.</param>
    public void SetAnimatorValue(string name, bool value)
    {
        anim.SetBool(name, value);
    }

    /// <summary>
    /// Changes a named boolean animator float to the specified value.
    /// </summary>
    /// <param name="name">The name of the animator parameter to change.</param>
    /// <param name="value">The new value of the animator parameter.</param>
    public void SetAnimatorValue(string name, float value)
    {

    }
    #endregion
}

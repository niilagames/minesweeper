﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    #region Declarations
    public GameObject balloonPrefab;
    public Transform stringOrigin;
    public List<GameObject> balloons;
    public float balloonPopInterval = 1f;
    #endregion

    #region Methods
    void OnPickup(Balloon pickupBalloon)
    {
        Vector3 spawnPos = transform.position + (Random.onUnitSphere * 3);
        GameObject balloon = Instantiate(balloonPrefab, spawnPos, Quaternion.identity, transform);
        balloons.Add(balloon);
        BalloonAnim balloonAnim = balloon.GetComponent<BalloonAnim>();
        balloonAnim.target = transform;
        balloonAnim.rotationTarget = stringOrigin;
        MeshRenderer[] mr = balloon.GetComponentsInChildren<MeshRenderer>();
        Material balloonMaterial = pickupBalloon.material;
        for (int i = 0; i < mr.Length; i++)
        {
            mr[i].material = balloonMaterial;
        }
    }

    public void RemoveBalloon(int index)
    {
        if (balloons.Count > 0)
        {
            balloons[index].GetComponent<BalloonAnim>().Kill();
            balloons.RemoveAt(index);
        }
    }

    public void RemoveOldestBalloon()
    {
        RemoveBalloon(0);
    }

    public void RemoveRandomBalloon()
    {
        RemoveBalloon(Random.Range(0, balloons.Count));
    }

    public IEnumerator PopBalloonsAtInterval(float waitTime)
    {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            RemoveRandomBalloon();
        }
    }
    #endregion

    #region Callbacks
    private void OnEnable()
    {
        EventManager.OnCollectiblePickup += OnPickup;
    }

    private void OnDisable()
    {
        EventManager.OnCollectiblePickup -= OnPickup;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion
}

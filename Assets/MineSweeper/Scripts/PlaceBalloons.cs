﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceBalloons : MonoBehaviour
{
    #region Declarations
    public Transform point1, point2;
    public GameObject balloonPrefab;
    public int numberOfBalloons;
    public bool includeLastPoint = true;
    [Header("Debug")]
    public Color sphereColor;
    public float sphereRadius = 1;
    #endregion

    #region Methods
    Vector3 PositionOnPath(Vector3 startPos, Vector3 endPos, float ratio)
    {
        Vector3 pos = Vector3.Lerp(startPos, endPos, ratio);
        return pos;
    }
    
    void GenerateBalloons()
    {
        int loopNum = numberOfBalloons;
        int segmentNum = numberOfBalloons;
        if (includeLastPoint)
        {
            loopNum = numberOfBalloons;
            segmentNum = numberOfBalloons - 1;
        }
        for (int i = 0; i < loopNum; i++)
        {
            Instantiate(balloonPrefab, PositionOnPath(point1.position, point2.position, i / (float)segmentNum), Quaternion.identity, transform);
        }
    }
    #endregion

    #region Callbacks
    private void OnDrawGizmos()
    {
        if (!Application.isPlaying)
        {
            int loopNum = numberOfBalloons;
            int segmentNum = numberOfBalloons;
            if (includeLastPoint)
            {
                loopNum = numberOfBalloons;
                segmentNum = numberOfBalloons - 1;
            }
            for (int i = 0; i < loopNum; i++)
            {
                Gizmos.color = sphereColor;
                Gizmos.DrawSphere(PositionOnPath(point1.position, point2.position, i / (float)segmentNum), sphereRadius);
            }
        }
    }

    void Start()
    {
        GenerateBalloons();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion
}

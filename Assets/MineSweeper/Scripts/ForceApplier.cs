﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceApplier : MonoBehaviour
{
    #region Declarations
    public enum ForceDirection
    {
        Ahead,
        AheadAlongLocalForward,
        Back
    }
    //public float force = 3;
    public ForceDirection forceDirection;
    [Tooltip("Should the force applier stun the controller after it completes its trajectory?")]
    public bool stun;
    [Tooltip("The distance in units that the controller will traverse while jumping.")]
    public float jumpDistance;
    [Tooltip("The maximum height that the jump curve will reach")]
    public float jumpHeight;
    [Header("Debug: Distance circles")]
    public bool drawDistanceCircles = true;
    [SerializeField] Color nearColor = new Color(1, 1, 1, .2f);
    [SerializeField] Color centerColor = new Color(1, 1, 1, .6f);
    [SerializeField] Color farColor = new Color(1, 1, 1, .2f);
    [Header("Debug: Jump Trajectory")]
    public bool drawJumpTrajectory = true;
    public int resolution = 10;
    public Color trajectoryColor = new Color(1, 1, 1, .6f);
    public AnimationCurve trajectory;

    [SerializeField]
    Collider coll;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        if (coll == null)
        {
            coll = GetComponent<Collider>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(collision.gameObject.name);
        Controller controller = collision.gameObject.GetComponent<Controller>();
        // If we collide with a controller, apply force.
        if (controller != null)
        {
            //Debug.Log("Collided with " + gameObject.name);
            if (stun)
            {
                controller.gameObject.SendMessage("StartStun", GetForceDirection(controller.transform));
            }
            else
            {
                if (jumpDistance > 0 && jumpHeight > 0) // We don't want to use this force applier's values if they are zero or negative.
                {
                    controller.StartJump(this);
                }
                else
                {
                    controller.StartJump();
                }
            }
        }
    }

    ///<summary>
    ///Returns a direction based on incoming angle as well as the force applier's locally defined reflection axis.
    ///</summary>
    public Vector3 GetForceDirection(Transform forceTarget)
    {
        Vector3 dir = Vector3.zero;
        switch (forceDirection)
        {
            case ForceDirection.Ahead:
                dir = Vector3.Reflect(forceTarget.position - transform.position, forceTarget.forward);
                break;
            case ForceDirection.AheadAlongLocalForward:
                dir = forceTarget.forward;
                break;
            case ForceDirection.Back:
                dir = Vector3.Reflect(forceTarget.position - transform.position, forceTarget.right);
                break;
        }

        return dir;
    }

    private void OnDrawGizmos()
    {
        if (drawDistanceCircles)
        {
            Color[] rangeColors = { nearColor, centerColor, farColor };

            float offset = coll.bounds.extents.x * 2;
            for (int c = -1; c < 2; c++)
            {
                Vector3[] circlePoints = new Vector3[37];
                for (int i = 0; i < circlePoints.Length; i++)
                {
                    float angleDiff = i * (360 / (circlePoints.Length - 1));
                    Vector3 targetPos = transform.position;
                    targetPos.z += (jumpDistance + (c * offset)) * Mathf.Cos((angleDiff) * Mathf.Deg2Rad);
                    targetPos.x += (jumpDistance + (c * offset)) * Mathf.Sin((angleDiff) * Mathf.Deg2Rad);
                    circlePoints[i] = targetPos;
                }
                for (int i = 0; i < circlePoints.Length - 1; i++)
                {
                    Gizmos.color = rangeColors[c + 1];
                    Gizmos.DrawLine(circlePoints[i], circlePoints[i + 1]);
                }
            }
        }

        if (drawJumpTrajectory)
        {
            Vector3[] trajectoryPoints = new Vector3[resolution+1];

            for (int i = 0; i < trajectoryPoints.Length; i++)
            {
                Vector3 position = transform.position;
                position.z += (jumpDistance / resolution) * i;
                position.y += Mathf.Lerp(0, jumpHeight, trajectory.Evaluate((float)i/resolution));
                trajectoryPoints[i] = position;
            }
            Gizmos.color = trajectoryColor;
            for (int i = 0; i < trajectoryPoints.Length - 1; i++)
            {
                Gizmos.DrawLine(trajectoryPoints[i], trajectoryPoints[i + 1]);
            }
        }
    }
}

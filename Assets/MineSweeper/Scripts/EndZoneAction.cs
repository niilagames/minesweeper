﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndZoneAction : MonoBehaviour
{

    #region Declarations

    #endregion

    #region Methods

    #endregion

    #region Event Handlers
    void OnWon()
    {
        //SceneController.Instance.LoadNextLevel();
        // Add behavior here for loading next level. Perhaps run a pre-level switching routine of some sort.

    }
    #endregion

    #region Callbacks
    private void OnEnable()
    {
        EventManager.OnLevelWon += OnWon;
    }
    private void OnDisable()
    {
        EventManager.OnLevelWon -= OnWon;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject == GameManager.Instance.Player)
        {
            Debug.Log("Collided with player");
            EventManager.OnLevelWon();
        }
    }
    #endregion
}

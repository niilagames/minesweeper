﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScannerColors : MaterialChanger
{
    #region Declarations
    public Material emissionOn;
    public Material emissionOff;
    public Material bodyOn;
    public Material bodyOff;
    public MeshRenderer mr;
    #endregion

    #region Methods
    void ChangeMaterial(Scan scanner)
    {
        Material[] newMaterials = mr.materials;
        if (scanner.scanning)
        {
            newMaterials[2] = bodyOn;
            newMaterials[5] = emissionOn;
        }
        else
        {
            newMaterials[2] = bodyOff;
            newMaterials[5] = emissionOff;
        }

        mr.materials = newMaterials;
    }
    #endregion

    #region Callbacks
    void OnEnable()
    {
        EventManager.ScanStart += ChangeMaterial;
        EventManager.ScanEnd += ChangeMaterial;
    }

    void OnDisable()
    {
        EventManager.ScanStart -= ChangeMaterial;
        EventManager.ScanEnd -= ChangeMaterial;
    }

    void Start()
    {
        if (mr == null)
        {
            mr = GetComponent<MeshRenderer>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion
}

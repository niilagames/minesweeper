﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balloon : MonoBehaviour
{
    #region Declarations
    public int value;
    public Material material;
    public Material[] possibleMaterials;
    #endregion

    #region Methods
    Material GetRandomMaterial(Material[] materials)
    {
        return materials[Random.Range(0, materials.Length)];
    }

    void UpdateMaterials(Material mat)
    {
        MeshRenderer[] mrs = GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < mrs.Length; i++)
        {
            mrs[i].material = material;
        }
    }
    #endregion

    #region Callbacks
    private void OnEnable()
    {
    }

    void Start()
    {
        if (material == null)
        {
            material = GetRandomMaterial(possibleMaterials);
            UpdateMaterials(material);
        }
        transform.rotation = Quaternion.Euler(0, Random.Range(0f, 360f), 0);
        GetComponent<Animator>().Play("Float", -1, Random.Range(0f, 1f));
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        Controller controller = other.gameObject.GetComponent<Controller>();
        if (controller != null)
        {
            EventManager.OnCollectiblePickup(this);
            Destroy(gameObject);
        }
    }

    #endregion
}

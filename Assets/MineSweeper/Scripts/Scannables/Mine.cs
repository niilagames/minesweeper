﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : Scannable
{
    #region Declarations
    public MeshRenderer meshRenderer;
    #endregion

    #region Methods

    #endregion

    #region Callbacks

    public override void Start()
    {
        base.Start();
        if (meshRenderer == null)
        {
            meshRenderer = GetComponent<MeshRenderer>();
        }
    }

    public override void OnScannedBegin()
    {
        base.OnScannedBegin();
        meshRenderer.enabled = true;

    }
    public override void OnScannedEnd()
    {
        base.OnScannedEnd();
        meshRenderer.enabled = false;

    }


    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scannable : MonoBehaviour
{
    public bool beingScanned;

    void OnEnable()
    {
        //EventManager.OnRegister += Register;
        //EventManager.OnDeRegister += DeRegister;
        if (EventManager.OnRegister != null)
        {
            EventManager.OnRegister(this);
        }
    }

    void OnDisable()
    {
        //EventManager.OnRegister -= Register;
        //EventManager.OnDeRegister -= DeRegister;
        if (EventManager.OnDeRegister != null)
        {
            EventManager.OnDeRegister(this);
        }
    }

    public virtual void Start()
    {
        if (EventManager.OnRegister != null)
        {
            EventManager.OnRegister(this);
        }
    }

    public virtual void OnScannedBegin()
    {
        beingScanned = true;
        //meshRenderer.material = scannedMaterial;
        //meshRenderer.enabled = true;
    }

    public virtual void OnScannedEnd()
    {
        beingScanned = false;
        //meshRenderer.material = unscannedMaterial;
        //meshRenderer.enabled = false;
    }

    public virtual void Register(Scannable target)
    {

    }

    public virtual void DeRegister(Scannable target)
    {

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinefieldGenerator : MonoBehaviour
{
    #region Declarations
    public Vector2 placementSpacing;
    public Vector2Int gridSize;
    public Vector3 initialPosition;
    public float relaxationLimit = 1.25f;
    [Range(0, 1)]
    public float relaxationMultiplier = .5f;
    public float xOffset = 6;
    public Transform parent;
    public GameObject[] objectsToPlace;
    #endregion

    #region Methods
    
    #endregion

    #region Callbacks
    void Start()
    {
        initialPosition = parent.position;
        Vector3 startPos = new Vector3(initialPosition.x, 0, initialPosition.z);
        float newX = placementSpacing.x * (gridSize.x / 2);
        for (int i = 0; i < gridSize.y; i++)
        {
            startPos.x = initialPosition.x-newX;
            for (int j = 0; j < gridSize.x; j++)
            {
                int randomInt = Random.Range(0, objectsToPlace.Length);
                float offset = 0;
                if (i%2 == 0)
                {
                    offset = xOffset;
                }
                Vector3 instantiatePos = startPos;
                instantiatePos.x += offset;
                float relaxX = (placementSpacing.x/2 - 2*relaxationLimit) * relaxationMultiplier;
                float relaxY = (placementSpacing.y/2 - 2*relaxationLimit) * relaxationMultiplier;
                instantiatePos.x += Random.Range(-relaxX, relaxX);
                instantiatePos.z += Random.Range(-relaxY, relaxY);
                Instantiate(objectsToPlace[randomInt], instantiatePos, Quaternion.identity, parent);
                startPos.x += placementSpacing.x;
            }
            startPos.z += placementSpacing.y;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmosSelected()
    {
        float boundsX = (gridSize.x*placementSpacing.x) / 2;
        Gizmos.color = Color.yellow;
        Vector3 startLeft = initialPosition;
        Vector3 startRight = initialPosition;
        startLeft.x -= boundsX;
        startRight.x += boundsX;
        Vector3 endLeft = startLeft;
        Vector3 endRight = startRight;
        endLeft.z += gridSize.y * placementSpacing.y; 
        endRight.z += gridSize.y * placementSpacing.y;

        Gizmos.DrawLine(startLeft, endLeft);
        Gizmos.DrawLine(startRight, endRight);
        Gizmos.DrawLine(startLeft, startRight);
        Gizmos.DrawLine(endLeft, endRight);
    }
    #endregion
}

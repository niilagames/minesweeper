﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlideVisualizer : MonoBehaviour
{
    #region Declarations
    enum Direction
    {
        Up,
        Down
    }

    [SerializeField, Tooltip("The controller from which to get glide speed and gravity.")] Controller controller;
    public int lineCount = 30;
    public float lineSpacing = 20;
    float lineX;
    float lineY;
    float a;
    public float lineheight = 300;
    public Color lineColor = new Color(1, 1, 1, .4f);
    int directionMultiplier = 1;
    Vector3 firstContactWithZero = Vector3.zero;
    [SerializeField, Tooltip("The transform at which to begin drawing lines. Defaults to 0,0,0.")] Transform startPointTransform;
    [SerializeField, Tooltip("Whether to draw lines down and forward or up and backwards from beginning transform.")] Direction drawDirection;
    #endregion

    #region Methods
    
    #endregion

    #region Callbacks
    void OnValidate()
    {
        if (controller == null)
        {
            controller = GameObject.Find("Player").GetComponent<Controller>();
        }

        if (startPointTransform != null)
        {
            firstContactWithZero = startPointTransform.position;
        }

        lineX = controller.speed;
        lineY = controller.gravity;
        a = lineY / lineX;
        Debug.Log(a);
        if (drawDirection == Direction.Up)
        {
            directionMultiplier = 1;
        }
        else
        {
            directionMultiplier = -1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        if (controller.speed != lineX || controller.gravity != lineY)
        {
            OnValidate();
        }

        Gizmos.color = lineColor;
        for (int i = 0; i < lineCount; i++)
        {
            Vector3 p1 = firstContactWithZero;
            //p1.y += lineheight / 2;
            p1.z += lineSpacing * i * -directionMultiplier;
            Vector3 p2 = p1;
            p2.y += lineheight * directionMultiplier;
            p2.z += lineheight * -directionMultiplier / a;
            Gizmos.DrawLine(p1, p2);
        }
    }
    #endregion
}

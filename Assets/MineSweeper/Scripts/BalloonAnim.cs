﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalloonAnim : MonoBehaviour
{
    #region Declarations
    public float forceTowardsCenter;
    public float liftForce = 1;
    public Transform target;
    public Transform rotationTarget;
    Rigidbody rb;
    Vector3 dir;
    Quaternion rotation;
    MeshRenderer[] mrs;
    [Tooltip("The multiplier of the collider with when the balloon pops.")] public float popForceRadiusMultiplier = 2f;
    public ParticleSystem popEffect;
    public ParticleSystem popEffectExplosion;
    public LineRenderer balloonString;
    public SphereCollider coll;
    #endregion

    #region Methods
    public void Kill()
    {
        StartCoroutine("Pop");
    }

    IEnumerator Pop()
    {
        // Perform things here
        MeshRenderer[] mrs = GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < mrs.Length; i++)
        {
            mrs[i].enabled = false;
        }
        coll.radius *= popForceRadiusMultiplier;
        yield return 0;
        rb.detectCollisions = false;
        //popEffect.gameObject.SetActive(true);
        var ps = popEffect.main;
        var psExplosion = popEffectExplosion.main;
        float waitDuration = ps.startLifetime.Evaluate(0) + 1f;
        balloonString.enabled = false;
        popEffect.Play();
        //Debug.Log("Destroying self");
        yield return new WaitForSeconds(waitDuration);
        Destroy(gameObject);
    }
    #endregion

    #region Callbacks
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        mrs = GetComponentsInChildren<MeshRenderer>();
        if (popEffect == null)
        {
            popEffect = GetComponentInChildren<ParticleSystem>();
            popEffectExplosion = popEffect.GetComponentInChildren<ParticleSystem>();
        }
        if (balloonString == null)
        {
            balloonString = GetComponentInChildren<LineRenderer>();
        }
        popEffectExplosion.GetComponent<Renderer>().material = mrs[0].material;

        if (coll == null)
        {
            coll = GetComponentInChildren<SphereCollider>();
        }
    }

    private void Update()
    {
        dir = (target.position - transform.position) * forceTowardsCenter;
        dir.y += liftForce;
        rotation = Quaternion.LookRotation(rotationTarget.position - transform.position) * Quaternion.Euler(-90, 0, 0);
        if (balloonString.enabled)
        {
            balloonString.SetPosition(0, balloonString.transform.position);
            balloonString.SetPosition(1, rotationTarget.position);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //joint.connectedAnchor = joint.connectedBody.position;
        rb.velocity = dir;
        rb.rotation = rotation.normalized;
    }
    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    #region Declarations
    public int score;
    #endregion

    #region Methods
    
    public void ModifyScore(int value)
    {
        score += value;
    }

    void OnCollect(Balloon coin)
    {
        score += coin.value;
        EventManager.OnScoreChanged(score);
    }

    #endregion

    #region Callbacks
    private void OnEnable()
    {
        EventManager.OnCollectiblePickup += OnCollect;
    }
    private void OnDisable()
    {
        EventManager.OnCollectiblePickup -= OnCollect;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion
}

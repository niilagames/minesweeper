﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager
{
    public ControllerState CurrentState { get; private set; }

    public void Initialize(ControllerState startingState)
    {
        CurrentState = startingState;
        startingState.Enter();
    }

    public void ChangeState(ControllerState newState)
    {
        CurrentState.Exit();

        CurrentState = newState;
        newState.Enter();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singleton
    private static GameManager instance;
    public static GameManager Instance { get { return instance; } }
    #endregion

    #region Declarations
    GameObject player;
    public GameObject Player { get => player; }
    public bool isPlaying;
    #endregion

    #region Methods
    void SetPlayer()
    {
        player = GameObject.Find("Player");
        if (player == null)
        {
            Debug.Log("Can't find player");
        }
    }
    #endregion

    #region Event Handlers
    void OnLevelLoaded()
    {
        SetPlayer();
        isPlaying = true;
    }

    void OnLost()
    {
        isPlaying = false;
    }
    #endregion

    #region Callbacks
    private void OnEnable()
    {
        EventManager.OnLevelLoaded += OnLevelLoaded;
        EventManager.OnLevelLost += OnLost;
    }
    private void OnDisable()
    {
        EventManager.OnLevelLoaded -= OnLevelLoaded;
        EventManager.OnLevelLost -= OnLost;

    }

    private void Awake()
    {
        #region Singleton Initialization
        if (instance != null && instance != this)
        {
            Destroy(this);
            Debug.LogError("Multiple instances of " + GetType().Name + " was found. Please make sure you only create one instance! The newest instance was destroyed.");
        }
        else
        {
            instance = this;
        }
        #endregion
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion
}

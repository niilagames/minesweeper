﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventManager : MonoBehaviour
{
    public delegate void ScanEvent(Scan scanner);
    public static ScanEvent ScanStart;
    public static ScanEvent ScanEnd;
    
    public delegate void ObjectScanEvent(Scannable target);
    public static ObjectScanEvent OnScannedBegin;
    public static ObjectScanEvent OnScannedEnd;
    public static ObjectScanEvent OnRegister;
    public static ObjectScanEvent OnDeRegister;

    public delegate void ValueChange();
    public static ValueChange OnConeSizeChanged;

    public delegate void CollectibleEvent(Balloon collectible);
    public static CollectibleEvent OnCollectiblePickup;

    public delegate void ScoreEvent(int value);
    public static ScoreEvent OnScoreChanged;

    public delegate void LevelEvent();
    public static LevelEvent OnLevelLoaded;
    public static LevelEvent OnLevelWon;
    public static LevelEvent OnLevelLost;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

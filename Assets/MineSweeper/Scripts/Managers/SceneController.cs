﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Niila.Tools.Sceneless;
using Niila.Tools.Sceneless.SceneGroupGraph;
using System.Linq;
using Doozy.Engine;

public class SceneController : MonoBehaviour
{
    #region Singleton
    private static SceneController instance;
    public static SceneController Instance { get { return instance; } }
    #endregion

    #region Declarations
    List<SceneGroup> sceneGroups = new List<SceneGroup>();
    #endregion

    private void OnEnable()
    {
        ScenelessManager.Instance.OnSceneGroupLoaded += OnLoad;
        EventManager.OnLevelWon += OnWonLevel;
        EventManager.OnLevelLost += OnLostLevel;
        Message.AddListener<GameEventMessage>("LoadScene", OnContinue);
        Message.AddListener<GameEventMessage>("ReloadScene", OnRetry);
    }
    private void OnDisable()
    {
        ScenelessManager.Instance.OnSceneGroupLoaded -= OnLoad;
        EventManager.OnLevelWon -= OnWonLevel;
        EventManager.OnLevelLost -= OnLostLevel;
        Message.RemoveListener<GameEventMessage>("LoadScene", OnContinue);
        Message.RemoveListener<GameEventMessage>("ReloadScene", OnRetry);

    }

    private void Awake()
    {
        #region Singleton Initialization
        if (instance != null && instance != this)
        {
            Destroy(this);
            Debug.LogError("Multiple instances of " + GetType().Name + " was found. Please make sure you only create one instance! The newest instance was destroyed.");
        }
        else
        {
            instance = this;
        }
        #endregion
        sceneGroups = ScenelessManager.Instance.AllSceneGroups.ToList();
    }

    public void LoadScene(NavigationData navData)
    {
        ScenelessManager.Instance.LoadSceneGroup(navData, SceneGroupPart.MainScenes);
    }

    public void ReloadScene()
    {
        SceneGroup currentScene = GetCurrentScenegroup();
        NavigationData navData = new NavigationData(currentScene, currentScene);
        LoadScene(navData);
    }

    public void LoadNextLevel()
    {
        if (ScenelessManager.Instance.GraphTraverser.HasNext) // Only load next level if it exists.
        {
            NavigationData nextLevel = ScenelessManager.Instance.GraphTraverser.GotoNext();
            LoadScene(nextLevel);
        }
        else
        {
            Debug.LogError(GetType().Name + ": Next SceneGroup not defined!");
        }
    }

    SceneGroup GetCurrentScenegroup()
    {
        return ScenelessManager.Instance.ActiveSceneGroup;
    }

    #region Event Handlers
    void OnLoad(SceneGroup sceneGroup)
    {
        Debug.Log("Scene was successfully loaded.");
        GameEventMessage.SendEvent("SceneLoaded");
        GameEventMessage.SendEvent("SceneReloaded");
        EventManager.OnLevelLoaded();
        //Debug.Log("Scene loaded.");
    }

    void OnWonLevel()
    {
        Debug.Log("Sent message to doozy.");
        GameEventMessage.SendEvent("PostGame");
    }

    void OnLostLevel()
    {
        GameEventMessage.SendEvent("GameOver");
    }
    void OnContinue(GameEventMessage message)
    {
        LoadNextLevel();
    }

    void OnRetry(GameEventMessage message)
    {
        ReloadScene();
    }
    #endregion
}

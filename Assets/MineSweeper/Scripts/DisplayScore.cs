﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayScore : MonoBehaviour
{
    #region Declarations
    [SerializeField] Text scoreText;

    #endregion

    #region Methods
    void UpdateScore(int value)
    {
        scoreText.text = value.ToString();
    }
    #endregion

    #region Callbacks
    private void OnEnable()
    {
        EventManager.OnScoreChanged += UpdateScore;
    }
    private void OnDisable()
    {
        EventManager.OnScoreChanged -= UpdateScore;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion
}

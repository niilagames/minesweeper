﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Niila.Tools.DoozyExtensions
{
    public abstract class ATag : MonoBehaviour, ITag
    {
        public abstract string Name { get; }

        /// <summary>
        /// Finds the nearest tag collector above in the hierarchy and makes it find all tags below it again.
        /// </summary>
        protected void RefreshTagCollector()
        {
            var tagCollector = GetComponentInParent<TagCollector>();

            if (tagCollector == null)
            {
                Debug.LogError(
                    $"No tag collector found in the hierarchy above this object ({Name}). A tag collector is required for tags to work.",
                    gameObject);
                return;
            }

            tagCollector.FindTags();
        }
    }
}
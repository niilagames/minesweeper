# Niila Doozy Extensions Guide

An extension to Doozy UI, which improves on some workflows, especially from a code perspective.

## Working with popups

The extension allows for easier handling of dynamic popups.
Dynamic popups are popups, where the content of the popup might differ for each use,
for instance a popup displaying the score of a level, where the score differs each time.

### Requirements and suggestions

It's preferrable that popups are instantiated using Doozy UI's methods and having the popup prefab
be set up in the popup database of the Doozy UI Control Panel instead of having them reside inside a scene.

### Tagging dynamic content

The way we allow content to be changed is by using tags which gives a piece of content a name.
It's then possible to update the content using its name.

To set up tagging for a popup, first add the `TagCollector` component on the top level of the popup (on the same game object as the UIPopup component from Doozy UI).
This component is what keeps track of tags inside the popup and upon which the content setter methods can be called.
There are extension methods for the UIPopup class as well, which we will explain later in the section "Updating content".

When the `TagCollector` component has been added, add a tag next to each component that should be changable.
For instance, add a `LabelTag` next to a `Text` or `TextMeshPro` component and give it a name.

The kinds of tags that are available are:
- `LabelTag`
- `ButtonTag`
- `ProgressorTag`
- `ImageTag`

The tags should register themselves with the Tag Collector, meaning their names should be visible from the Tag Collector inspector.
If not, you can click the `Find tags` button in the Tag Collector inspector to make it find all the tags.
It searches for tags that are on the same game object or the game object's children.

### Updating content

The reason for tagging is to be able to easily change the values of content in a popup.

When you want to display a popup you use `var popup = UIPopupManager.GetPopup("popupName");`. This returns an object of type UIPopup.

To continue you can either call `popup.GetTagCollector()` and call methods on that, but it's also possible, through extension methods,
to call the same methods directly on the popup object itself `popup.SetLabel("labelName", "labelValue")`.

All the "SetX(name, value)" methods return the Tag Collector object, which means that the methods can be chained together like this:

```C#
popup.SetLabel("Header", "You won!")
	.SetLabel("Description", "You were victorious in your battle against evil!")
	.SetButtonText("Continue", "Awesome!");
```

### Update content with delays.

Sometimes you don't want to update content right away (before the popup is shown) or at the same time (staggering changes over time).

For triggering changes with delay you can use the `WaitForSeconds` method on the Tag Collector (or directly on the UIPopup via extension methods).
When calling this method, the next "SetX(name, value)" method called will be delayed by the amount of seconds indicated.
If stacked, they will multiply the wait time, so it's possible to delay with 4 seconds using the following:

```C#
popup.WaitForSeconds(2f)
	.WaitForSeconds(2f)
	.SetLabel("Header", "You won!");
```

Using this, you can trigger staggered animations using the progressor, for instance.

If you want something to first trigger after the popup is done animating,
you should use the UIPopup's events and then use the methods above to set the popup contents' values.

The WaitForSeconds method runs in realtime by default. If you want it to use relative time, you can add it as a boolean parameter after the time.

## Using the tagging system from editor mode

It's possible to use the tagging system in editor mode, for instance when making editor tools that set values on dropdowns or on other types of UI.

In order for the TagCollector to work properly in editor mode it's necessary to call the function `InitializeForEditorMode` either on the UIPopup or directly on the TagCollector.
This finds tags (if not set to skip) and also deserializes the lists of found tags into the dictionaries that the rest of the methods use.

All setter methods have also gotten an "immediate" variant which doesn't add anything to the queue and sets values directly. This is to circumvent the problem with coroutines in editor mode.

## FAQ (or the 'something went wrong' section)
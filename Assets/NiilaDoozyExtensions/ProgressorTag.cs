﻿using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.Progress;
using UnityEngine;

namespace Niila.Tools.DoozyExtensions
{
    public class ProgressorTag : ATag, ITag<float>
    {
        public override string Name => _name;

        public Progressor Progressor => _progressor;

        [SerializeField] private string _name;

        [SerializeField] private Progressor _progressor;

        public void SetValue(float value)
        {
            _progressor.SetValue(value);
        }

        private void Reset()
        {
            if (string.IsNullOrEmpty(_name))
            {
                _name = gameObject.name;
            }

            _name = _name.Trim();

            if (_progressor == null)
            {
                _progressor = GetComponent<Progressor>();
            }

            RefreshTagCollector();
        }

    }
}
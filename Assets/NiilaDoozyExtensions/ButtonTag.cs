﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Niila.Tools.DoozyExtensions
{
    public class ButtonTag : ATag, ITag<string>, ITag<UnityAction>
    {
        public override string Name => _name;

        [SerializeField] private string _name;

        [SerializeField] private Text _text;
        [SerializeField] private TextMeshProUGUI _textTmp;
        [SerializeField] private Button _button;

        public void SetValue(string value)
        {
            if (_text != null)
            {
                _text.text = value;
            }
            else if (_textTmp != null)
            {
                _textTmp.text = value;
            }
        }

        public void SetValue(UnityAction value)
        {
            // Remove before adding to make sure listener is not duplicated.
            _button.onClick.RemoveListener(value);
            _button.onClick.AddListener(value);
        }

        private void Reset()
        {
            if (string.IsNullOrEmpty(_name))
            {
                _name = gameObject.name;
            }

            _name = _name.Trim();

            if (_text == null && _textTmp == null)
            {
                _text = GetComponent<Text>();
                _textTmp = GetComponent<TextMeshProUGUI>();
            }

            if (_button == null)
            {
                _button = GetComponent<Button>();
            }

            RefreshTagCollector();
        }
    }
}
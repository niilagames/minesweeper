﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Doozy.Engine.Progress;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Niila.Tools.DoozyExtensions
{
    public class TagCollector : MonoBehaviour
    {

        private Dictionary<string, LabelTag> _labelTags = null;
        private Dictionary<string, ButtonTag> _buttonTags = null;
        private Dictionary<string, ProgressorTag> _progressorTags = null;
        private Dictionary<string, ImageTag> _imageTags = null;
        private Dictionary<string, AnimatorTag> _animatorTags = null;

        #region Serialization

        [SerializeField] private List<LabelTag> _serializableLabelTags = new List<LabelTag>();
        [SerializeField] private List<ButtonTag> _serializableButtonTags = new List<ButtonTag>();
        [SerializeField] private List<ProgressorTag> _serializableProgressorTags = new List<ProgressorTag>();
        [SerializeField] private List<ImageTag> _serializableImageTags = new List<ImageTag>();
        [SerializeField] private List<AnimatorTag> _serializableAnimatorTags = new List<AnimatorTag>();

        private void ConvertSerializedTagsToDictionaries()
        {
            _labelTags = _serializableLabelTags.Where(t => t != null).ToDictionary(t => t.Name);
            _buttonTags = _serializableButtonTags.Where(t => t != null).ToDictionary(t => t.Name);
            _progressorTags = _serializableProgressorTags.Where(t => t != null).ToDictionary(t => t.Name);
            _imageTags = _serializableImageTags.Where(t => t != null).ToDictionary(t => t.Name);
            _animatorTags = _serializableAnimatorTags.Where(t => t != null).ToDictionary(t => t.Name);
        }

#if UNITY_EDITOR

        /// <summary>
        /// Finds tags (if not set to skip) and deserializes found tags into dictionaries to use while in editor mode.
        /// </summary>
        /// <param name="skipFindingTags">If true, doesn't find tags before deserialization.</param>
        /// <returns></returns>
        public TagCollector InitializeForEditorMode(bool skipFindingTags = false)
        {
            if (!skipFindingTags)
            {
                FindTags();
            }
            ConvertSerializedTagsToDictionaries();
            return this;
        }

#endif

#endregion

        /// <summary>
        /// Is true if next "set value" method should wait before applying.
        /// </summary>
        private bool _waitBeforeNext = false;

        /// <summary>
        /// The amount of seconds the next "set value" method should wait.
        /// </summary>
        private float _nextWaitForSeconds = 0;

        /// <summary>
        /// Whether or not the next "set value" method should wait in realtime or in relative time.
        /// </summary>
        private bool _nextWaitForIsRealtime = true;

        /// <summary>
        /// Is true, while the wait for coroutine is still running.
        /// </summary>
        private bool _isWaitingForNext = false;

        private Queue<WaitForCallbackInfo> _callbackInfos = new Queue<WaitForCallbackInfo>();

        private void Awake()
        {
            ConvertSerializedTagsToDictionaries();
        }

        #region RuntimeOnly

        public TagCollector SetLabel(string name, string text)
        {
            if (!_labelTags.ContainsKey(name))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            EnqueueAndStartWaitForCallback(() => { _labelTags[name].SetValue(text); });
            return this;
        }

        public TagCollector SetButtonText(string name, string text)
        {
            if (!_buttonTags.ContainsKey(name))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            EnqueueAndStartWaitForCallback(() => { _buttonTags[name].SetValue(text); });
            return this;
        }

        public TagCollector SetButtonCallback(string name, UnityAction action)
        {
            if (!_buttonTags.ContainsKey(name))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            EnqueueAndStartWaitForCallback(() => { _buttonTags[name].SetValue(action); });
            return this;
        }

        public TagCollector SetProgressorValue(string name, float value)
        {
            if (!_progressorTags.ContainsKey(name))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            EnqueueAndStartWaitForCallback(() => { _progressorTags[name].SetValue(value); });
            return this;
        }

        public TagCollector SetImageSprite(string name, Sprite sprite)
        {
            if (!_imageTags.ContainsKey(name))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            EnqueueAndStartWaitForCallback(() => { _imageTags[name].SetValue(sprite); });
            return this;
        }

        public TagCollector SetAnimatorBool(string tagName, string boolName, bool value)
        {
            if (!_animatorTags.ContainsKey(tagName))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            EnqueueAndStartWaitForCallback(() => {
                _animatorTags[tagName].SetValue(boolName, value);
            });
            return this;
        }

        public TagCollector SetAnimatorFloat(string tagName, string floatName, float value)
        {
            if (!_animatorTags.ContainsKey(tagName))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            EnqueueAndStartWaitForCallback(() => {
                _animatorTags[tagName].SetValue(floatName, value);
            });
            return this;
        }

        public TagCollector SetAnimatorInt(string tagName, string intName, int value)
        {
            if (!_animatorTags.ContainsKey(tagName))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            EnqueueAndStartWaitForCallback(() => {
                _animatorTags[tagName].SetValue(intName, value);
            });
            return this;
        }

        public TagCollector SetAnimatorTrigger(string tagName, string triggerName)
        {
            if (!_animatorTags.ContainsKey(tagName))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            EnqueueAndStartWaitForCallback(() => {
                _animatorTags[tagName].SetValue(triggerName);
            });
            return this;
        }

        /// <summary>
        /// Adds an action to the queue. Should not be used from editor scripts.
        /// </summary>
        /// <param name="callback">The action to run when reaching this item in the queue.</param>
        /// <returns></returns>
        public TagCollector SetCallback(Action callback)
        {
            EnqueueAndStartWaitForCallback(callback);
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="seconds">If adding multiple wait fors after each other it adds up the time total.</param>
        /// <param name="isRealtime">If several wait fors are added after each other, the isRealtime overrides the previously set value.</param>
        /// <returns></returns>
        public TagCollector WaitForSeconds(float seconds, bool isRealtime = true)
        {
            if (_waitBeforeNext)
            {
                _nextWaitForSeconds += seconds;
            }
            else
            {
                _nextWaitForSeconds = seconds;
            }

            _nextWaitForIsRealtime = isRealtime;
            _waitBeforeNext = true;

            return this;
        }

        #endregion

        #region RuntimeAndEditor

        public TagCollector SetLabelImmediate(string name, string text)
        {
            if (!_labelTags.ContainsKey(name))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            _labelTags[name].SetValue(text);
            return this;
        }

        public TagCollector SetButtonTextImmediate(string name, string text)
        {
            if (!_buttonTags.ContainsKey(name))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            _buttonTags[name].SetValue(text);
            return this;
        }

        public TagCollector SetButtonCallbackImmediate(string name, UnityAction action)
        {
            if (!_buttonTags.ContainsKey(name))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            _buttonTags[name].SetValue(action);
            return this;
        }

        public TagCollector SetProgressorValueImmediate(string name, float value)
        {
            if (!_progressorTags.ContainsKey(name))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            _progressorTags[name].SetValue(value);
            return this;
        }

        public TagCollector SetImageSpriteImmediate(string name, Sprite sprite)
        {
            if (!_imageTags.ContainsKey(name))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            _imageTags[name].SetValue(sprite);
            return this;
        }

        public TagCollector SetAnimatorBoolImmediate(string tagName, string boolName, bool value)
        {
            if (!_animatorTags.ContainsKey(tagName))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            _animatorTags[tagName].SetValue(boolName, value);
            return this;
        }

        public TagCollector SetAnimatorFloatImmediate(string tagName, string floatName, float value)
        {
            if (!_animatorTags.ContainsKey(tagName))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            _animatorTags[tagName].SetValue(floatName, value);
            return this;
        }

        public TagCollector SetAnimatorIntImmediate(string tagName, string intName, int value)
        {
            if (!_animatorTags.ContainsKey(tagName))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            _animatorTags[tagName].SetValue(intName, value);
            return this;
        }

        public TagCollector SetAnimatorTriggerImmediate(string tagName, string triggerName)
        {
            if (!_animatorTags.ContainsKey(tagName))
            {
                throw new ArgumentException($"The label with the tag {name} was not found on the tag collector on object {this.name}.");
            }
            _animatorTags[tagName].SetValue(triggerName);
            return this;
        }

        public Progressor GetProgressor(string name)
        {
            return _progressorTags[name].Progressor;
        }

        public Image GetImage(string name)
        {
            return _imageTags[name].Image;
        }

        public Animator GetAnimator(string name)
        {
            return _animatorTags[name].Animator;
        }

        #endregion

        [ContextMenu("Find tags")]
        public void FindTags()
        {

            // Clear all tag lists.
            _serializableLabelTags.Clear();
            _serializableButtonTags.Clear();
            _serializableProgressorTags.Clear();
            _serializableImageTags.Clear();
            _serializableAnimatorTags.Clear();

            // Initialise queue and add first nodes to check (the child nodes of this tag collector).
            var nodeQueue = new Queue<Transform>();
            for (int i = 0; i < transform.childCount; i++)
            {
                nodeQueue.Enqueue(transform.GetChild(i));
            }

            // Check nodes until the queue is empty.
            while (nodeQueue.Any())
            {
                var node = nodeQueue.Dequeue();

                // Check if the node is another tag collector. If it is skip it and its children.
                if (node.GetComponent<TagCollector>() != null)
                {
                    continue;
                }

                // Check if the node is any type of tag and add it to the correct tag list if it is.
                var tag = node.GetComponent<ATag>();
                if (tag != null)
                {
                    if (tag is LabelTag labelTag)
                    {
                        _serializableLabelTags.Add(labelTag);
                    }
                    else if (tag is ButtonTag buttonTag)
                    {
                        _serializableButtonTags.Add(buttonTag);
                    }
                    else if (tag is ProgressorTag progressorTag)
                    {
                        _serializableProgressorTags.Add(progressorTag);
                    }
                    else if (tag is ImageTag imageTag)
                    {
                        _serializableImageTags.Add(imageTag);
                    }
                    else if (tag is AnimatorTag animatorTag)
                    {
                        _serializableAnimatorTags.Add(animatorTag);
                    }
                }

                // Take the node's children and add them to the queue for checking.
                for (int i = 0; i < node.childCount; i++)
                {
                    nodeQueue.Enqueue(node.GetChild(i));
                }
            }
        }

        #region QueueRelated

        private bool ShouldAddToWaitForQueue => _waitBeforeNext || _callbackInfos.Count > 0 || _isWaitingForNext;

        /// <summary>
        /// Adds the callback to the queue with the correct time
        /// </summary>
        /// <param name="callback"></param>
        private void EnqueueAndStartWaitForCallback(Action callback)
        {
            if (ShouldAddToWaitForQueue)
            {
                // If waitBeforeNext is true, enqueue the callback with the set number of seconds for wait time.
                if (_waitBeforeNext)
                {
                    _callbackInfos.Enqueue(new WaitForCallbackInfo(_nextWaitForSeconds, callback, _nextWaitForIsRealtime));
                    // Reset the marker for if we should wait.
                    _waitBeforeNext = false;
                }
                // Else enqueue without seconds wait time (sets to -1, so it skips the wait for in the coroutine).
                else
                {
                    _callbackInfos.Enqueue(new WaitForCallbackInfo(-1, callback, true));
                }

                // If the coroutine isn't started yet, start it.
                if (!_isWaitingForNext)
                {
                    StartCoroutine(WaitForCallback(_nextWaitForIsRealtime));
                }
            }
            // If there is no queue or it should not create a queue, just run the callback.
            else
            {
                callback();
            }
        }

        private IEnumerator WaitForCallback(bool useRealtime)
        {
            _isWaitingForNext = true;

            while (_callbackInfos.Count > 0)
            {
                var nextCallback = _callbackInfos.Dequeue();
                if (nextCallback.Seconds > 0)
                {
                    if (useRealtime)
                    {
                        yield return new WaitForSecondsRealtime(nextCallback.Seconds);
                    }
                    else
                    {
                        yield return new WaitForSeconds(nextCallback.Seconds);
                    }
                }

                nextCallback.Action();
            }

            _isWaitingForNext = false;

        }

        private struct WaitForCallbackInfo
        {
            public readonly float Seconds;
            public readonly bool UseRealtime;
            public readonly Action Action;

            public WaitForCallbackInfo(float seconds, Action action, bool useRealtime)
            {
                Seconds = seconds;
                Action = action;
                UseRealtime = useRealtime;
            }
        }

        #endregion

        private void OnValidate()
        {
            if (_serializableLabelTags.Contains(null))
            {
                _serializableLabelTags.Remove(null);
            }

            if (_serializableButtonTags.Contains(null))
            {
                _serializableButtonTags.Remove(null);
            }

            if (_serializableProgressorTags.Contains(null))
            {
                _serializableProgressorTags.Remove(null);
            }

            if (_serializableImageTags.Contains(null))
            {
                _serializableImageTags.Remove(null);
            }

            if (_serializableAnimatorTags.Contains(null))
            {
                _serializableAnimatorTags.Remove(null);
            }
        }

        private void Reset()
        {
            FindTags();
        }
    }
}
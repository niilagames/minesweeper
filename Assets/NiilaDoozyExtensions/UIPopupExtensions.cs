﻿using System;
using Doozy.Engine.Progress;
using Doozy.Engine.UI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Niila.Tools.DoozyExtensions
{
    public static class UIPopupExtensions
    {
        public static TagCollector GetTagCollector(this UIPopup popup)
        {
            return popup.GetComponent<TagCollector>();
        }

#if UNITY_EDITOR
        #region EditorOnly

        /// <summary>
        /// Finds tags (if not set to skip) and deserializes found tags into dictionaries to use while in editor mode.
        /// </summary>
        /// <param name="popup"></param>
        /// <param name="skipFindingTags">If true, doesn't find tags before deserialization.</param>
        /// <returns></returns>
        public static TagCollector InitializeForEditorMode(this UIPopup popup, bool skipFindingTags = false)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot initialize for editor mode on popup {popup.name} that doesn't have a tag collector.");
            }

            tagCollector.InitializeForEditorMode(skipFindingTags);

            return tagCollector;
        }

        #endregion
#endif
        #region RuntimeOnly

        public static TagCollector SetLabel(this UIPopup popup, string name, string text)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set label with the name {name} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetLabel(name, text);

            return tagCollector;
        }

        public static TagCollector SetButtonText(this UIPopup popup, string name, string text)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set button text for button {name} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetButtonText(name, text);

            return tagCollector;
        }

        public static TagCollector SetButtonCallback(this UIPopup popup, string name, UnityAction action)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set button callback for button {name} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetButtonCallback(name, action);

            return tagCollector;
        }

        public static TagCollector SetProgressorValue(this UIPopup popup, string name, float value)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set progressor value for progressor {name} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetProgressorValue(name, value);

            return tagCollector;
        }

        public static TagCollector SetImageSprite(this UIPopup popup, string name, Sprite sprite)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set image sprite for image {name} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetImageSprite(name, sprite);

            return tagCollector;
        }

        public static TagCollector SetAnimatorBool(this UIPopup popup, string tagName, string boolName, bool value)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set bool value for animator {tagName} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetAnimatorBool(tagName, boolName, value);

            return tagCollector;
        }

        public static TagCollector SetAnimatorFloat(this UIPopup popup, string tagName, string floatName, float value)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set float value for animator {tagName} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetAnimatorFloat(tagName, floatName, value);

            return tagCollector;
        }

        public static TagCollector SetAnimatorInt(this UIPopup popup, string tagName, string intName, int value)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set int value for animator {tagName} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetAnimatorInt(tagName, intName, value);

            return tagCollector;
        }

        public static TagCollector SetAnimatorTrigger(this UIPopup popup, string tagName, string triggerName)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set trigger for animator {tagName} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetAnimatorTrigger(tagName, triggerName);

            return tagCollector;
        }

        /// <summary>
        /// Makes an action run.
        /// </summary>
        /// <param name="popup"></param>
        /// <param name="callback">The action to run when reaching this item in the queue.</param>
        /// <returns></returns>
        public static TagCollector SetCallback(this UIPopup popup, Action callback)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set callback on popup that doesn't have a tag collector.");
            }

            tagCollector.SetCallback(callback);

            return tagCollector;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="popup"></param>
        /// <param name="seconds">If adding multiple wait fors after each other it adds up the time total.</param>
        /// <param name="isRealtime">If several wait fors are added after each other the isRealtime overrides the previously set value.</param>
        /// <returns></returns>
        public static TagCollector WaitForSeconds(this UIPopup popup, float seconds, bool isRealtime = true)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot add wait time on popup that doesn't have a tag collector.");
            }

            tagCollector.WaitForSeconds(seconds, isRealtime);

            return tagCollector;
        }

        #endregion

        #region RuntimeAndEditor

        public static TagCollector SetLabelImmediate(this UIPopup popup, string name, string text)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set label with the name {name} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetLabelImmediate(name, text);

            return tagCollector;
        }

        public static TagCollector SetButtonTextImmediate(this UIPopup popup, string name, string text)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set button text for button {name} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetButtonTextImmediate(name, text);

            return tagCollector;
        }

        public static TagCollector SetButtonCallbackImmediate(this UIPopup popup, string name, UnityAction action)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set button callback for button {name} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetButtonCallbackImmediate(name, action);

            return tagCollector;
        }

        public static TagCollector SetProgressorValueImmediate(this UIPopup popup, string name, float value)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set progressor value for progressor {name} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetProgressorValue(name, value);

            return tagCollector;
        }

        public static TagCollector SetImageSpriteImmediate(this UIPopup popup, string name, Sprite sprite)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set image sprite for image {name} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetImageSprite(name, sprite);

            return tagCollector;
        }

        public static TagCollector SetAnimatorBoolImmediate(this UIPopup popup, string tagName, string boolName, bool value)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set bool value for animator {tagName} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetAnimatorBool(tagName, boolName, value);

            return tagCollector;
        }

        public static TagCollector SetAnimatorFloatImmediate(this UIPopup popup, string tagName, string floatName, float value)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set float value for animator {tagName} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetAnimatorFloat(tagName, floatName, value);

            return tagCollector;
        }

        public static TagCollector SetAnimatorIntImmediate(this UIPopup popup, string tagName, string intName, int value)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set int value for animator {tagName} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetAnimatorInt(tagName, intName, value);

            return tagCollector;
        }

        public static TagCollector SetAnimatorTriggerImmediate(this UIPopup popup, string tagName, string triggerName)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot set trigger for animator {tagName} on popup that doesn't have a tag collector.");
            }

            tagCollector.SetAnimatorTrigger(tagName, triggerName);

            return tagCollector;
        }

        public static Progressor GetProgressor(this UIPopup popup, string name)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot get progressor {name} from popup that doesn't have a tag collector.");
            }

            return tagCollector.GetProgressor(name);
        }

        public static Image GetImage(this UIPopup popup, string name)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot get image {name} from popup that doesn't have a tag collector.");
            }

            return tagCollector.GetImage(name);
        }

        public static Animator GetAnimator(UIPopup popup, string name)
        {
            var tagCollector = popup.GetTagCollector();
            if (tagCollector == null)
            {
                throw new NullReferenceException(
                    $"Cannot get animator {name} from popup that doesn't have a tag collector.");
            }

            return tagCollector.GetAnimator(name);
        }

        #endregion
    }
}
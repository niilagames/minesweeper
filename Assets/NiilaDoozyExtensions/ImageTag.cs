﻿using UnityEngine;
using UnityEngine.UI;

namespace Niila.Tools.DoozyExtensions
{
    public class ImageTag : ATag, ITag<Sprite>
    {
        public override string Name => _name;

        public Image Image => _image;

        [SerializeField] private string _name;

        [SerializeField] private Image _image;

        public void SetValue(Sprite sprite)
        {
            _image.sprite = sprite;
        }

        private void Reset()
        {
            if (string.IsNullOrEmpty(_name))
            {
                _name = gameObject.name;
            }

            _name = _name.Trim();

            if (_image == null)
            {
                _image = GetComponent<Image>();
            }

            RefreshTagCollector();
        }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Niila.Tools.DoozyExtensions
{
    public class LabelTag : ATag, ITag<string>
    {
        public override string Name => _name;

        [SerializeField] private string _name;

        [SerializeField] private Text _text;
        [SerializeField] private TextMeshProUGUI _textTmp;

        public void SetValue(string value)
        {
            if (_text != null)
            {
                _text.text = value;
            }
            else if (_textTmp != null)
            {
                _textTmp.text = value;
            }
        }

        private void Reset()
        {
            if (string.IsNullOrEmpty(_name))
            {
                _name = gameObject.name;
            }

            _name = _name.Trim();

            if (_text == null && _textTmp == null)
            {
                _text = GetComponent<Text>();
                _textTmp = GetComponent<TextMeshProUGUI>();
            }

            RefreshTagCollector();
        }

    }
}
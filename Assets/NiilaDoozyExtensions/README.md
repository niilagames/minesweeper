# Project README

An extension to Doozy UI, which improves on some workflows, especially from a code perspective.

Find documentation in the DOCUMENTATION file in the same folder as this README.

## Installation

This package is a .unitypackage, which can be opened up in Unity.
When opening the file it will display a window where you can select the items you'd like.
You need to select all items and it will place them in your asset folder.

## Dependencies

- Doozy UI
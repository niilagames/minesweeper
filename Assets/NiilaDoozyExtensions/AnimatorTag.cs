﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Niila.Tools.DoozyExtensions
{
    public class AnimatorTag : ATag, ITag<string, bool>, ITag<string, float>, ITag<string, int>, ITag<string>
    {
        public override string Name => _name;

        public Animator Animator => _animator;

        [SerializeField] private string _name;

        [SerializeField] private Animator _animator;

        /// <summary>
        /// Set bool value on animator.
        /// </summary>
        public void SetValue(string name, bool value)
        {
            if (_animator != null)
            {
                _animator.SetBool(name, value);
            }
        }

        /// <summary>
        /// Set float value on animator.
        /// </summary>
        public void SetValue(string name, float value)
        {
            if (_animator != null)
            {
                _animator.SetFloat(name, value);
            }
        }

        /// <summary>
        /// Set integer value on animator.
        /// </summary>
        public void SetValue(string name, int value)
        {
            if (_animator != null)
            {
                _animator.SetInteger(name, value);
            }
        }

        /// <summary>
        /// Set trigger on animator.
        /// </summary>
        public void SetValue(string name)
        {
            if (_animator != null)
            {
                _animator.SetTrigger(name);
            }
        }

        private void Reset()
        {
            if (string.IsNullOrEmpty(_name))
            {
                _name = gameObject.name;
            }

            _name = _name.Trim();

            if (_animator == null)
            {
                _animator = GetComponent<Animator>();
            }

            RefreshTagCollector();
        }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Niila.Tools.DoozyExtensions.Editor
{

    [CustomEditor(typeof(TagCollector))]
    public class TagCollectorEditor : UnityEditor.Editor
    {
        private SerializedProperty _labelTags, _buttonTags, _progressorTags, _imageTags, _animatorTags;

        private void OnEnable()
        {
            _labelTags = serializedObject.FindProperty("_serializableLabelTags");
            _buttonTags = serializedObject.FindProperty("_serializableButtonTags");
            _progressorTags = serializedObject.FindProperty("_serializableProgressorTags");
            _imageTags = serializedObject.FindProperty("_serializableImageTags");
            _animatorTags = serializedObject.FindProperty("_serializableAnimatorTags");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            // LABELS.
            EditorGUILayout.LabelField("Available labels:");
            for (int i = 0; i < _labelTags.arraySize; i++)
            {
                var tag = _labelTags.GetArrayElementAtIndex(i);
                var name = (tag.objectReferenceValue as ITag)?.Name;
                if (string.IsNullOrWhiteSpace(name))
                {
                    continue;
                }

                Rect labelRect = GUILayoutUtility.GetRect(new GUIContent(name), "label");
                EditorGUI.SelectableLabel(labelRect, name, EditorStyles.textField);
            }

            // BUTTONS.
            EditorGUILayout.LabelField("Available buttons:");
            for (int i = 0; i < _buttonTags.arraySize; i++)
            {
                var tag = _buttonTags.GetArrayElementAtIndex(i);
                var name = (tag.objectReferenceValue as ITag)?.Name;
                if (string.IsNullOrWhiteSpace(name))
                {
                    continue;
                }

                Rect labelRect = GUILayoutUtility.GetRect(new GUIContent(name), "label");
                EditorGUI.SelectableLabel(labelRect, name, EditorStyles.textField);
            }

            // PROGRESSORS.
            EditorGUILayout.LabelField("Available progressors:");
            for (int i = 0; i < _progressorTags.arraySize; i++)
            {
                var tag = _progressorTags.GetArrayElementAtIndex(i);
                var name = (tag.objectReferenceValue as ITag)?.Name;
                if (string.IsNullOrWhiteSpace(name))
                {
                    continue;
                }

                Rect labelRect = GUILayoutUtility.GetRect(new GUIContent(name), "label");
                EditorGUI.SelectableLabel(labelRect, name, EditorStyles.textField);
            }

            // IMAGES.
            EditorGUILayout.LabelField("Available images:");
            for (int i = 0; i < _imageTags.arraySize; i++)
            {
                var tag = _imageTags.GetArrayElementAtIndex(i);
                var name = (tag.objectReferenceValue as ITag)?.Name;
                if (string.IsNullOrWhiteSpace(name))
                {
                    continue;
                }

                Rect labelRect = GUILayoutUtility.GetRect(new GUIContent(name), "label");
                EditorGUI.SelectableLabel(labelRect, name, EditorStyles.textField);
            }

            // IMAGES.
            EditorGUILayout.LabelField("Available animators:");
            for (int i = 0; i < _animatorTags.arraySize; i++)
            {
                var tag = _animatorTags.GetArrayElementAtIndex(i);
                var name = (tag.objectReferenceValue as ITag)?.Name;
                if (string.IsNullOrWhiteSpace(name))
                {
                    continue;
                }

                Rect labelRect = GUILayoutUtility.GetRect(new GUIContent(name), "label");
                EditorGUI.SelectableLabel(labelRect, name, EditorStyles.textField);
            }

            serializedObject.ApplyModifiedProperties();

            EditorGUILayout.Space();

            if (GUILayout.Button("Find tags"))
            {
                ((TagCollector)target).FindTags();
            }

            //DrawDefaultInspector();
        }
    }
}
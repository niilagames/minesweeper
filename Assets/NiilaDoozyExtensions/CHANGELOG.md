# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),

## [Unreleased]
### Added

## [1.1.0] - 2020-03-24
### Added
- Added the animator tag. There are methods for setting a bool, int, float and trigger value and get the animator itself.
- Added SetCallback method to be able to run a function anywhere in the queue.
- Added support for nested TagCollectors. A tag collector doesn't include tags that are also contained in another tag collector.
- Added a way to use the tag system in editor mode. This is useful for making editor tools which use the tag system.
### Changed
- Changed the wait for time to use realtime by default. It can be set to use relative time.

## [1.0.0] - 2020-02-11
### Added
- A way to easily tag content in popups and update the tagged contents' values.
- Made it possible to add delay between popup content updates making it possible to have a staggering animation effect.
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Niila.Tools.DoozyExtensions
{
    public interface ITag
    {
        string Name { get; }
    }

    public interface ITag<T> : ITag
    {

        void SetValue(T value);

    }

    public interface ITag<T, K> : ITag
    {

        void SetValue(T value1, K value2);

    }
}
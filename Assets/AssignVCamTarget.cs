﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class AssignVCamTarget : MonoBehaviour
{
    #region Declarations
    [SerializeField] Cinemachine.CinemachineVirtualCamera vCam;
    #endregion

    #region Methods

    #endregion

    #region Event Handlers
    void AssignTarget()
    {
        if (GameManager.Instance.Player != null)
        {
            vCam.Follow = GameManager.Instance.Player.transform;
            vCam.LookAt = GameManager.Instance.Player.transform;
        }
    }

    void StopFollowPlayer()
    {
        vCam.Follow = null;
    }
    #endregion

    #region Callbacks
    private void OnEnable()
    {
        //EventManager.OnLevelLoaded += AssignTarget;
        EventManager.OnLevelLost += StopFollowPlayer;
    }
    private void OnDisable()
    {
        //EventManager.OnLevelLoaded -= AssignTarget;
        EventManager.OnLevelLost -= StopFollowPlayer;
    }
    void Start()
    {
        if (vCam == null)
        {
            vCam = GetComponent<Cinemachine.CinemachineVirtualCamera>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (vCam.Follow == null && GameManager.Instance.isPlaying)
        {
            AssignTarget();
        }
    }
    #endregion
}
